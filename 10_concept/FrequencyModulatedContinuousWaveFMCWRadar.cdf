(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[    160857,       3463]
NotebookOptionsPosition[    131378,       2803]
NotebookOutlinePosition[    157661,       3364]
CellTagsIndexPosition[    157548,       3358]
WindowTitle->Frequency-Modulated Continuous-Wave (FMCW) Radar
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 ButtonBox[
  GraphicsBox[RasterBox[CompressedData["
1:eJztvXmUXldxL8pazzeKUCRHkqVWz3O3utWTutWjWq1u9dz9SdbgSZ7AI1OY
59EGMxgMGI9AgACBJIwmcBNCwnBvgm2mvPduCATWevfmvgD5864L5P3Neaf2
WFW79vlOt2RJtrfXqnW+s3ftGn5Vu2r7bEtuvfXlp2/9P57znOd05fS133vO
cy7Ln0/Mdf6vROebOhIlSpToKaaLXecSJUqUKFGiRBeIfmfoYtuRKNHTjX73
RNo/iRKVod8V0MW2LVGii0VF+2Ize2Wj8hIleibQed2X//dCZ5bowtP/lehZ
QRc7zxI9/fZ5efldF92/RIkSJUqUKFGiRIkSJUqUKFGiRIkSJXqq6b+v7k90
3qm7Oq0kSoSoTM4kelrR/3MOBHXkf5Si7kSJEiVKlChRokSJEiVKlChRokSJ
EiV6RtP/XO3+X4kSJbr49K+JnjH0PwT67yvFhHn/FT1jdLHzNVGiRIkSJUp0
weh3hi62HYkSPd3od/8z7Z9EicrQ7wroYtuWKNHFoqJ9sZm9slF5iRI9E+i8
7svfXjueFdF/IPot+x3QNYgicsrwRuevidEY4f0Pgar5dj5JxnKsgIpjsPF1
fK5Id5HcsQ3q36yeajI2iudm9Z+LndViWH6fPTtpomBcz/1/ZGwSzV0sKlcD
fnONp1/ntUoizOPXTlTNjXPNu0JSdTWk/8ht/A+hBv+HIcVn90VERqJEiRIl
SpQoUaJEiRIlSpQoUaJEiRKVpYHWjpzas/782Z8/B1o89TNSfNK4mQNZg0pe
hxvra2nLicvJ+dpynrYOp1/pa7Xy7LgmZVdOg3adWQPvQ+2d+ViXIsXbAtTm
qK8Vnq1ZX3O7ov7mDiUf+PoUtetncxv63W5+txs+64eVp2Uqf5oRGd5+w9OH
7AC92rZ280Rygb/Vyx4AwnrUnMamr7UD4dkWkpFl3weUnW1Er/W93/nncbDr
Qf+Ak2vwaDY8bq3FBGxsNfHz9hFb4beKpVnb2k5s7oMYtWI79XPA+dlqCGEP
fBZ7I1fpA6ybje5mn1fq6XLMY03tMDa2ej/7Wk0ssZ2t7QH1tbS7ddp3u97k
hOWzv838AIqL09nSYXJV+9DX0mH2hd9Dfr3XO9DsZeI95eLd6vMa70ntN35H
a9z+bvd7kuWTrR9eVoeJuZGF4tNnawmpJ23GZpQfsMebWrOBnPraOrOe1s6s
t7lTxb+3tSE70NqYHWhrVtTX1qpjY0jnI8SrRVOrnTO/Wz2PptbsgJrT8/i3
loHlm3clB8vW7wdatc4DLV7WAXg2NyvqRc9exaefvW4NsqGFPrUs4NXvfl2r
0WlssjqRTLe+hY5rm1udTCv3AFmjZfbmYx3tjVl3S2PO02zmmlWuduXUmcep
N6f+Rohbi6kReN+2+jrFahatOW2+VqA89LUF5xSu+TquA1i+zQlbB6E2NHf4
mu1qeIep076eBzU+RoQX2d3cLo+3oHesNzZP+OjeJWudng5qW2Ar9xX700HX
OV86sgAnQlhnRHegs0OwLyKXjwcyMU7CWoJHh8zX3BHOcz1FPsfsxX4HceC2
cX6GO8ZMwGNvTUO2Z29ONY3qCe9qjDwbydhey7e3Uf/eS8f3GJnUFpZzzcym
Zo4jyl0pD4hfHQw7LkeIvYQjty+Qw/0pkhfLhw5mhxQvwQ++17keHFcxj9uz
ECtmO9m7PN8ErMUawuqOVK9idROfF4vsD/Kc7SmOO6+7ol1CzvF9zOsQwV94
kjM890GIL9/nQV3voHJ5DyL285wR/A4w43VCqkOxePB1PHcku7m9fB9JOdAR
kcfjjtdJfEKOi/uJ68J5zH7z2sb/PU6SFeSOhDmX0R7KlOqqVIN4TZT6cLDf
NX9bQ6uidkNtDW1Ze2OrG7dzbY1tlKcBvxu+RjPeqH97uS2eGvUz2Eu5bdBr
XN+yvSjoWw1h30I9jvQy1LdsX9tj+xtei8YUv+mNzp69drzB91Ono5HqqQlp
j/HD8e6V5pBfe708i4PGpNHZsRfxUX2Ngc0coz17G0NMCTb03ODisLfR21Qj
6OZnjb0hLt4ndgaJ+ELjgnBBeUJ9oHHfw+Rh392Y09+IbGP4CfnH7d5j7OU2
ybnUyOQ3hj7ifWDjI9nAYoDPfVYOwVTcT2bNXuqzlZH6Vupbl1LfEvWJ519u
D7eR+UrkS7km2VAQW3wW5TnH5/leCGyO5TLXX4JaGF48j/g43//8/B/UByFf
uZ+8VgQxY5gH+Aj5EeSjZK+wV2L7XopJrDZwuXw8kMnyRYyBkOvE9o5wnusR
bNPnNTjX+bNdW2OrObu1krOdf7axcx2mNrSuLZxvbDXnPgGPFuYnj0U0Vh67
y3bty/7Trprssp3wzGlnjXrHBHOXwW87Z3ndGPq9087798uUjBo0ptd73Ugf
WkPsQLIvszbsZON4jKyp8fZjwrKJbXbOy7xMydhHbcVrEJ+Xvc/PYZt3eUwc
Ng7/mB/7GIYhTpcFviDZ6Onx20f4Ltu1j2Jgbdm1j+piuHsZNq4UH/4uxoE/
XS5S22luorGdnv8yZpfPWR5zn6eXIX8szjRvPQaXobWXReQRP5h/OIccvru8
DThXsf7L3HqbU/uIveEe2udyjNgu5pPfV56Hx2yfk+t928cwxXbsYz4weW49
9R/HCq/1+clyk+cW9oPkiBCHYL/6d4pXJO/RPqF738fX+7BX0669euwPYV6/
w7i19zKEz2XCPg32ALZT2ktYRrB/6B4I/EX4XYbyPYZLaAeyPagvFuN9gZ1h
HfW/L8OxjeafnBeXYX94rUXj3keWP7ymWV5sC+5/LKeCceuTe/K9wHKO1zIp
7iz2l+3ka/aFfgh16j+x/cb1kHq0C8fS+LET1x+cx7xO4ncpHgyvCB+Nj7xn
vM1UNjlTsJwn9RHn1S62H3GOobjiHA9qHMklCQ/WD3Av5nUN28kx2kX1pHMf
50/nvnTuozHC8tO5L537gnwsfe5Dsney2O30efVsP/elvsXkBTER4pz6FrE3
9S0qP/Wt1LeCfEx9i9UbtidQPtL6IvctuY763+l7RSznuQ60D8iafaEf6XsF
8nFf4OPT6XuF36t2L/HzmYThPqLv93bX5rQv25LT7+3ST/UbxnfVmnHNY3k1
fy15wlrFZ9cQuXT9Fml8l7fBrXNraqm+XbWOj89bmW79Ls+DdXn/+Npaxp/L
2oXX7SO6uZ1+jMrAGHvckN9Y7i60dpe302PHfHR83qYt2M/dzH7mw+8xmTY2
W/Bah7u3i+gnPqKcsDhb25yM2sBuh4eYAzSWzl62lvuJY7GF2LiP+cRyBOne
wtZTHjlnCL/dGyjnwvwSfMX5tmufEE8ao2DfRWhLsCcQbrt8HfB7WsrLfSaW
sRwI7SV7Ldg7PGZ8r1CMtmB72d73dUXKnXAv/h6zy+0jJH8LqSthPpI6tguv
zWss1Fn1jp477TgmxLO7xsveReuFsw35QfOc44VjGub6FsbnxpEOtweEXkDy
AuMQ2CDHItxT2A5e5zBvrbC/UV3AvgZ+s3zZ5d+37KJ+hLWI1VZTs7bwMVRb
pVzfEmAR2fsk31g8RRJsdBiw34F8oa5hfMjewv0ijFuAM89N3m9jtmA8CB+v
P8hW0l94PcByad/Yspva5PYxz9lI/4z1ELx+Czlnof0m5TnKS44b7/NbmE20
Ltp+QTFI5z4hD62v6dyH9pS3K537aNzDdXzv4ljy/BJ8Tec+kptBTJ4W5z4p
X/gcjwHfd8+2cx/HCu8dinnqWzymvK7g/YvzndqR+paQn6lvpb61O/Wt0B5O
tR5zsu+ebX2L4pO+Vwg5I+FbZKPDgP0O5At1LX2vCHM20j9jPQSvf6q/V7h9
HmDDc1qIhdH1+1fUadrDnpxgfE8tmq8tXLeVr3XP2nAcj11RhXcP1lGrbULr
tlh5AX+trCdCWyV7YtgE/gh47EEyyZy2a4vzRaA98bmtnE9aj/DYavRv5b4Q
HPn6feypdW3dg36L2NY6PRhP/7veYRPEG9ticm9rgY9b91DbojhI2EXiujUa
TxaTPYJM4X0rtyu6dyIxlX7jp+g3wxzp9DmJ1+4TZKBxw7cF+8xjs4fjK8UL
8ezheSzsNSZjq2RbmT0u1ZGSe1vjVcvWc33M192GrI3576270XiM7NpqubyH
4VyQA1Vr0R5MOg5bRcxqKX8E12o1MpBN+ksJWdVqNHrfSmperVzf8VwkvlvN
fLAejcl1no2XwYlhFa9HLE8LbCN1Xqo7ezxWkpxoPZHyAssJ9rMgS/JBWLeV
68djG80ZbntOWyLxidpk5zlmEh+uuc5m0yeDHhzBKCI3GqciTNK5L24/zuWi
dZs+9xm7C7EvYWeJ+JY793GqZU8mA/dDtu78nfuKc3hrQT2rGuPzcu6T82Zr
TB6WWbh3In5J/bbA7+rnPh7rSA4Yvi3s3BSeD2oL8SV5IdXLgj4R4lx+Xz/1
576Izah2FvXTarkh2iXoKMz7WC3ag+kSOPdxns3glPpW6lupbyGbUt8Kcjv1
rbjNqW+FuAf5m75XxHxJ3yvoXPpesTE8ZX5ak7ai9+furc+pwTztb03byHs9
eq8X1knvEmFZIXGdso6Y3nDNNvaU14dz2wQ/t4k6/Tuf34awfO7eHOs9+fie
ugLKdaDf8Nxm6LlsnPLXF8vei3gcHjb2dZREXfVVxuqoHU5uvZcp6XI8BqM9
1jaE2V4sn/q6LWpXvcHG84W80to6Yvc2h1Odeec4USx47Krp24bW0xjLcYjn
AdeD/ce60LvzI4YlliPFmT5l++M4b2P2b6sqp5rvPOdxDsap2p6u/hvnBPaX
+r4V0R/dPJnTVNbZ26b+fea5pi9sZTEq2tfb9oSxiOWQGzc5XVzfimu65dnm
6ltMBiZprMHsr7j+4tjwca7H+7ktylcvrMF2cayK+pusn2MWzz+qn/sQsyNc
Xy22/l3uwXTtNlGXbHfxXsM+UJu2BZiFvDQ28XEf7zCuNG9pr+fx3sZkUxnV
crIIg5jtZdeH4+ViIdWvdO7jc+f/3Fdub/AYhjkR5kyxTznV8JwLa0txnlXL
GaneV8sVAffg3BfL7XiMqI94X8fqfDyPw3pQjE+8HhTVNSl3yuyZ2N7m/vMa
Ktsq48PrHK8B1eyP48x74Laqcqr5Xm2NTOd+7ov5G9uTsRrVwKhIBq4L1XNI
OsNc2ue+ItxkDOXaFvNr8/mU+pasP/Wt1LfK7ZnY3ub+Y118L0u2xHKFx5k+
U9/i/larv7xGFdV62e5nbt8qE5sYng1MRvpeEcYlfa/gfeSZ9b1C0hfuixBr
v/YPahqzP9jXmG3fZ57mXVENeq8R3vex3zUNfoyM+7Ht+FnD5OwL+USKrBHn
aqjO7ZwH2xDzzWIkyOb4Ub4GTTWM8hhsZ+/w3GYIj29j9AcQwxo0htYHT0Pb
9/rndm6L468P1lt7iA1ct+BH/F3r2I70KHs4Tua3tXW7lbWX2SbZEMGU8Fa1
k9poaTvi8zjWk/Xb9vqnw6tIlzAX5Ab3xflTvzm5eym/xXcb0sPz0f1mcYjJ
Btv42m0YJ44LztW9DaL/nuqNDibHxonjofKr3tho96edQ3XNjsdqDOjN57cb
CuxEv7W/9SZGvg8D/fm9lewfPnF19q9fuzmbn+3X96xqb9Cnw49jLlKYC9sR
ERvtfov6yWq0UEur1WPST3idl+qoVI95D4rUdaqzAb03BPzbS9vQIPA2oD7S
kHFfRful3mZ8wv2oyC7ek/2zweSjYIegU+zX1eJbE85tr4mMS3qFuWhe1aAY
ur3LfbZ8DR7rWL5Wyc+orxyzGtnmELOGUDfLOfF3ydyJxjd6bmkI1vFzVzr3
FeEnYBPBT+QTbOS1gsdmu5XDaDveCzG7I/m7neslmDVE1/P9FY1bUS21+VEj
7KdI3ou5IuTddkZR2aXtDPOe11+y1yM1rGwNkvzhmMg1SupNJeRKODM9Qa0o
ygVRdlhzgt4r6ZJizPOZYy5hFcw3ONlBDatpLHnuw32/IKdqQv6NxAdjEMv9
cB83iPIKzzpVxi7auc/5nPpWYb4V1X2MUayec2yL7JPiXxPGJvWt1LfIvovE
LPWtCFbBfOpbT5u+xXVGbJTzKH2viOkU87NafIVanb5XyP5fct8rBF8K44D8
uLy2OduR0+V1TdmOOnjq9x21Tf43jNfqOftU82Zuh3uCDL9OyaxF8+bdzdei
Zx16Wl6sF81r24weN250K3lNRh9da9fYdTuQHZcjvZfXItnIFrxmB5K3o45i
o/Rz2teUbbfPHHs93pCTjs/lMGZitcPs9e3mucPsgx2OB/2uxb8bjbxQtyP1
rvVrfjvu123fp+e07AZ3RgUCW3bYd7u/jF6rYzvx2cxzGwBj8355Pq8xa1Rj
l9f68R213jaNm6/L3l+DibE5GDeYKl9qGxxmNg7bjf9Wl8ekyWPiYkfHNZYN
zk+rj8TP2qB4mhxZHDBezl8nr9G973Cyfcx3IJ4daA2OzXbsB/EZ+4Zzz2Bl
412D9Nl47/M2EvtqG6nPTi4eRzpqqR/OZjfW5Ox3+Dq8cRytfRjbRp1bZv7y
fSa/bG7ZXHO8NudYLtR6vssxL4mXtQ+Nob2zw+4f5D/cZX30bSv5eL0er0EY
231m+H3+NHrf7b6ytqJ9vB3FGu8954f5fbmt2eapa3QTrZG4/uF6jOqlr8vN
pn76+r0Dk+0BtsfU+vp9udFteXa4et3k63fQc1DPqvO1n/cBbreT43Q1o37S
lO3g9hv7rL2XIzttPyQ9DNvnMEZrsd2s35C+hnHDvQ7h42TF+hXr434MY0Ux
wNi7OLE+h3OD9sGmyDg+F7D+XtuE/Pd54GMf+nA5savJYyngu8ONMT0ohtQv
li+47+P+j3LeY8bk1DLbhRz2c+gM5faXnBdu/9VRu3A+O78w8VgQ/9K575zO
fWzfER9rUT0z8v4Q711UR7F/l+PfOO583wo1jsa8ya9lscIx97WY5jzhqWX+
1zKcSFzo/OUBjnSP+3g1IczRXiaxbCKxCveN7UXeHpzjWBfNtch+I/sBx7uJ
2ujqA8s7Vh9I/gh9dkcdwwmvZ3WB1DorC+9bYQ2tAz7udj/x+JI4orz1e7sp
sM/zoDMUqSkoZ2pR7rEa4/FhvvD8cn3Dn238PuD53oRiifZfLfMR41/r9fD6
hs8CtFeEMd+B8Q0wQ3aT/OFrmohf+BzG9eNef0me+3BN4fWrFuPmsU99K/Wt
1LdwrtG9nvoWzefUt5qJPalvoRijvNpQ33I+NAW+hT2H1qP0vYLFq47iResz
xopikL5XsHxB9Yn0rUvxe4WNrdB7SL3j+wDl2K6G1mxnfUu2s8GQ+g1jrfrZ
gN7tWL0eV2sJtfjfTiZdE/K2UBn1wtqcdrlxQRdZS+d3xfxh63bVMznEFjzW
Qp9OJuCBMMzpDxU162edPvuppyX+nsclfDayJ+dv8nJq6fxOQ4GuekxN4W8i
qwnpaUTvjcxWq0+Q5343UTzM750Yp+DZ4m0KMGuqjklAdq7J2Nzk5CqsnI1N
FKc65APzW8I+jHMTfa9FscF6q/nFcahtZvY0UYrFAOeBtZ344OO8k8gssLG2
GfFirBkWHI+oPwWYcj3muTPIb5NDdT6ndrqY+vxSezZfu8v+JqTHfT7q/PV5
i/CtxXh6O+2dEaa/eO9xRfau7XJ3l4bxbqR5ymrFTikX65pYznp/dxosoC6q
2ghjDc2oluPaK9TooI6jsXqhthI+qSdIeqV1Qq+pR73BvbcwedyfiJ31kt5W
hkukfwR9r8gv2ycMbz23NdJzne1CX471qsDeiB6MJ7J/F9dN9MbkFOmV47wr
tg7Fk/BwjIO4x/IBYyXkQ+CvPUNQ27Atu8jZycxh/np0BonlN8eHxJ37x3ns
OaxaPFBOOzvTue/8nfsimPBYBvszkrsF+yWoBWw/7MI5Wl9NpxQ/plfEv4Xp
i8WoRZCF95Hku5y38n6JYCLFg9QTHx/ZDp6/vJ7I2MtraZ5G612RX9F48njx
Gs9jKdgeiW/Y+yI21rf6f/chWDMsomcH7k8BpmKd4zUp1kN4jnj/Y3s4GK+P
1Nto/xXeybpIbHieBuMClkQOxWAXeu5CPJfWuS/1rdS3BJ9iMUt9qzgvU98q
tjH1rdS3ovFqzcr3LWmdgJ1Zn75XtBbkXUEN5nOsx6bvFS0CDk+H7xUx+SVi
YnJyd2NbdkVTe07+uRuRfs/HG9vpswmv0zyWH3j0E8kxevRaT1iWW+9ktpGx
3U5Pe2ifW298aLR2tBvdns/aJfntdDVS/6xPnqc9uwLZ4/SA7AZYn+dDTrtz
vHebXNqtvhdrgm/H9vu4/Vau3t23dERqrCWbW60osvP2W7SXp2sOvGu9+JkT
xLvB2qRpF6LdJsd2GxlKR/48de312anrbiB27bL6kU9Wr7arFcmlOjUeZrxR
26SwUpi1uXHbu3a7f4dpNhh6m70tdFzb0OyefYPd2cr8way5s8PYjW23NrY6
//Fv/8zH670PPJ7qt4tjS65vKOvP9XrbMEbNQRw8LtQ2Xbd0fEEm+EFyxsa8
HsehmdhL49xKfTI5stPYNTXZp/S0dBis6hDGxhZnn80B5qO7/6lrQZho2Ydz
crnr1rcw25sZNlTnTqOruasjW1kYyg5P9ZFYuRg1tpjcavP5ZXLN5p6ea/N8
Cp82t2Y3WdNCfztc/b8XWf1gF8Sf7pkm/axryj733uOKYGynGm9yOMP7YRMH
h1Md3WsWK7C3f2h/ru9g1prnd2yP+/3W5qnR1kpUyxp9Ld3N6yquiayWy7U4
/92I17SRmkx7AB3bLc5bG2N1mMm3/qEeckVA3n5iVyNds9uMEd+EniD7IPSQ
xjbWm7gM60vYn1w/bMSyeGxYvFivt/30Ct7rXA+kMsjT6UXxcHkUkUcwbs84
Xhh/3p89fmHvd364fMM9nPqOzyOux9tzSmM71Yl4dpMYeDkeE2oDz0Nqrz2j
hOcLGluUbwRvFl/sCzpH7Q7s4rmdzn3n7dzHc4T4Le9Z4k8j9X23ZAvZFyhm
jWGdDn0Q7AiwFGoK8Veqc0hGo7UlVgNoX/DxoHVI573Jucb2jMfb5S/pAcLe
Y7kT4BbNR17vuK7Qd88n1G+EB96nvJ6EexTXNrYHcG40SnZIcW6nPgmxJ/nS
SP0g+xTtdVpzhF5H7GNxEuqeZJeVSbHANU/qfaxO4vmgXto6GubQFYHtcl2K
zYf9gPJc0cRtYfucxIGu4XVWrqNWjid/1kP74yKf+1LfSn0r9a3Ut8I4p75F
62fqW5dS3+J60/eKcK+KNTp9r6Bxe5Z+r8C2455H90hYG/Bzb0tHTp3uWaN+
d6KxDjcWzDV3mKeds/wdwVqqB1NsLr6mRpQh8XHdnC+2xuqg62uiOs3vHI89
ze05mWeTpbbszNmbsiz7XfaOd9+bx7HFUwNQa3Zs9biaf+CRR+mdT32rWvO/
f/2/1TwQ/NZyWo2cVkUw9v/+278pnjNnbzTjbUhvmx5ralXzmu8m9U72T86z
J6cHHn3U6bT0wCMfVs+3G/17jB9//93vKrK2WCJ6VO62Olz+/ruPqzUWI6C9
7nf+bPTj8P8Skuhz7zuRveq2I1lrV4fDdI/FJMdveqov++6fXEPWwPu1V456
PHJaWxxWc+uLBw1GHgvQAWTxtM+1nBfWrKk1Wndrd0f2gdctyvog1vm6PYpa
1RzYvqexjdjyqttm1JyN7XX5WtDPff/Y3avG7zYjV6+/4+yk81nbxvxQmOo8
wP7Aun/63PUM3+PZXz98Wq9rQDmbP6cP92Vfz+e0HzNOlvXL5UHO/6pbjxDZ
8NvztJgYHDQxGA6w9jHQ8YW1PK4g8+4/OmZyrM3l1V7ze68ba89efdtRtWav
yzlPe9EaTK82ccGy7dPmS2t3Zx7/pTD+J0bp/Vr+BGyBrmhoVmTnb79uMvtv
QRxOKLy1/35/QV5xHD5295qy4woTY7CvbX9X9rG3r2lZ951Q9Qn8tjVrb06u
5jV3CvWzQ6iDaK45rIVyLS/qAbjuSjXY66e9qBONS7WZ2oh1eDtDG2qkdc1a
dwwbuZeGPYu8N3eg/snxlX2xGFBbOtF6jNNm+m3MP459KKtGHKO2aZ9p3ih7
Ue4FedaMzyESRliH7FONO7NUO4dIZ4fY2aMoXgU4N/Nc53upWD7HoQZh4/ND
ym8BF+F8lM59fD/G8kLOHblG8D3fyeKNsS2RQyzu8bwpwr9anZJrsM8zwb8g
r4rwYjnJ8Khp8fm8l2PWLNkq5UssH4Wa0SxhUZTLMdx9rH2c8J6X8jK+J0mO
Nku2hzWBxitWG4v2B/rdzPVUo1BWvDbLdUnKzRrBNylW1Gdqm3SGCM4PzdX2
npBXwXlCim9EX5RXii19j9Wa8CzRoXpgWLc7L4FzX+pb5fclxST1LQnzEL/U
t+I+S7H2cfI8qW8V5EBBbqa+Fb4/M/pWJ/sd5k76XpG+V0h7qbi2PHu+V8Sw
cbWsWbATjdW2dXtq78qfXXTMjrd3mznz21I+X+fGhLVt3WitHevK12AZSG6b
l+vG27qFp9XNeNu7Qz3q2WX86KI62mW5IS7cNqO7Hels7cz2AbWZZ6uvVffc
+151n/PffvzjbG9Tax6DNve9fG9jW3bPu9F8ox6raWx14/e8571ZV/9BRfe8
5143ButrcrrqBn1P9tCjH8muzn939Q/r79Q5Kd5cP3yr3gffq3O6xvADb435
hq33pf6mDbLtOtAJ9i6uHVf2Yd3aj3Zzj/W4eq9paXd0taTH1IS/f/xxRfB7
X2tO8MzzUhFgZ/HL+eH7+wffsJRVlkeyytJItp7T2SvHsg++fsndYQwP95pv
89qug/k7jMNdy8x0v8L94Eiv+p4P4+093Qo7WLO+OKLkVJaGtZ2K2tTz8/ed
UITHwHewwd672Hha2XeenVLvwyM92cfN/cHM9KDDCwjG4D5lr8FcU1v26tv1
fQm8t+/vVvaDfqsHbADfv/7IaTVn72LgOTM9oLHKcQH7Onu6nD/Wj30tKA7N
Ok5gm113UOHYqp4fNHcysE7lrc3Z3AaFbW4DYDc8fEDbb/3KfbBxsP7APVD7
/i5Fduw1tx0xMWhzMYCn88ngYm2HMfARdL/9pcfUbxgDnO68/rAbtzlWo/Yh
yiuTY6+5Xd9j1bTq+rrPPN27GXN52YrWAH7NPo/1U2Ps4p/bUpPbBTlp749m
Dg+4vb3X3ivCnZK5N95j7ixVHHLcYW2NwmVY4az8t/dPCC+I2fDIAaX/7Mlx
pf8FoN/YBnv+429fV+NnT41ns0eGXK2qbe3Kn13qvRbXZEN1pi7W4VqI+0S7
qa2u/tI6T/pFW+QZ/O4Sekak9uP6G+tfUTm4D1k/8zGhD9WRum94kb0LlZOK
xo7OZd0HR53ssZlj2Znrb8q6h0adjIXKlQ4bGAceLAfbA/8dwP6hUacbZIMO
i5XiN/Z0Dx1S/73A2NFjxFbQp3Rg+QgvWGN5wJ5aZ+dJZ5te53GDNYrXxAp0
gn7ba7Vdmnf/QWvXnMKgrl3Pdx885OzQmGhMQefYzBzBdzx/Vzz2zNHerfSD
XJj3+g1GDlMtf7/hHVNyTjL89O8wFlcaXEeR7i5nr8XZ7g14937zeHbnPhzT
PiPddfzM1E5/W9keC3amCvZPl5HZRdYH+Z7OfZHawHEJ4xLUnQg+ZJ7UtRhW
tL64GoxrsWBHneGpE+Lt6zjHrZvoiNqD4yTOdwW6CmPbztcw2VKeMx/CXOgq
8AXbgfGgOsk7t4fnIelTsu66dpR7PJdE6vJ71+0vipuLr8OC4Sj0bbIX2pk/
QZ6yXJX2MPO7jmOEc0Baa2uUs0vIoyLsi3I20CXFFGGF9RJ/fF6SPSXVA26j
4ed7NbCD7yuSO1hnDAuBGG9dINufY4K9xOJ2Mc59qW+lvsXrSOpbHI/Ut1Lf
Qjypb4n5k75XYL1dLj5hbnvbeezqSOyEekdiwPcty2e+h4OcwrZG5lkcw/3G
YyX1SlYHXa/F2KFcae8m79yOOo65w5L2kTpio1SrqN11kr28vwSxEPzE5wi+
h4J6xGtxGLun4nsFxZbncFHd8PIaOno0dQLt108yhsbVc3/I2ylQbFxax/VL
/NF3PF40x+3b7/V2lNETk+2pPpdZ39Gd1dve7WpmZ/YPjz/u/kxT9+CwukvY
Z+6V9jW1Z1/7q7/28wPDbg7+fBXMqfcWT5/58z9Xc/q7fEf2znvfp9bWtnYE
BOPvfO/70Fhnds2Nz1Pj8FTfsNV3bPPM5f2DuWOy8rX+tmx8ZlbLu/e9yB59
JwVrsA6t52at54abc7lIj8EEqK4VcrYTnWu7POV8MA/f6197x6y3VcnSuuC7
PMzf//pl9b6vWdt8/xuW1Lf7zt7uAL/O3v16fYuWcWJF3wnA0+kwWHz+visV
WV67zq45vjSi9A2P9Om7mduPOlysLY//yTW5PWBfu5HRbnw6GsQLxmDOvnfl
tvo4dDjZ158aV3yzM0N+7e10LY6F80PFoEPHweBodTodCC+w3d5/WTuOLxvf
8yfwW10w5zHQsh7/5DXqHkWvbXe5D3iAbC5Tx6Cd+KBtP+HGLSbcz3e8bF7F
vM70EppL/gm5BLpU7rWZPIOne++iMvAaMm+wzHWPHOoPYmp9AwwgH3EOunvF
Zl8LHNYkXztUvlpZNv9IPNG+6OrrcXsMbKvLx+3+sf5AjarPe4GuWZrK1rmg
Lkr1M+ghG6nXTJ7YK6TeFNEt2sH6kCQ36gPV+bFPfjr7xje/pZ6/+c1vs6vz
mgpzr3zdm9T4Bx58OHvy+z/IekfGlDz4bWUB7wcefMi9q7mcp3d4LPvJv/ws
+8ADeq2SmY9ffYPAnz/vftd7si98+TE1Bzpf+fo3qfHbXvQS9X51Xoe5bqBv
/J22G2z85a9+ZWyHdX+k5qwvet1+5YOy60Fr181qHN6tjbJdDxO7QOfyiVNq
7cTsQgb/KHzyOeAHsviCDGsnyNBx0TaBXOBx+h2mDzsfsc02Rhw/ha1btx/F
Yn+2fPyUWofXKJz/zvqzP7fx3dRGkx8Yb23jze7swWMBpDHpUZhADpTK4Vhe
S+c6cY+kc9/G9ZaJRQQPrL9aDKLxKGNTzL4CW7m9GCOFncW7hA0dRTJj+CMb
o9hFancR1tzmwt5kfJV4i/RLeR7k+AZiF9Mr5nVRvGP2ST17k/ZxndXqQ7U6
EYxttu5E8qSoNpZ532gsNxTnKrxSjGLnl9JYXSLnvtS35PHUt4ptTX3L+yrx
pr5VTmfqW+cQ5yq8z+S+9TT6XlGMS0+op9p+6eiR7XLjJXEjOJeoo9H44XrP
bdtgPQ34Ir25Wg50FOT1ZkjsrzE9HM8YvpEa1lGUWxGbSu9V3t/L9Ge2h/Pf
zd0Hzhs1ieO9m14vy7tQVM1uNt/VmzUp6skaO4FM/OGusL1L3eX857/+unre
+ZKXZnWtHZ5aOsz8X7t5+FY9eXRO4O9U9IJ8DOYmjx7L6ts6s3e9V99jwXf1
evVNXj+BYBzm69u6FIE91930fDV+3c3PV++atK3AA7b+0z//2MmoN3rrzL2Y
ktfqddg7Ka3DjoMefV8G+uqJnu6c/4nsH554Ims050zArNHkMoxhgu/wr7tz
DtnapeRZnz70hmV1f4Ht/ZtHzqhxi1k9JmQjPK9c0X8OBp5qDMn+wvuvVGTf
LV25csisOaRkvOjGafW+v68nf+9AOjqze14+r+4yvE5/N8flvs7cl3AZnG48
PaH4jh0dcn74tV3OD4uX9QPH2sbbrutWttN4f+G+KxVh7LjvmFSswK+cd9Tc
7dx4asKtt7G44ZS2f3S0X5RZ1ybFAOui+IAv97x8QeVBQ0d3kEOa9Pjr81wC
XcBnee1vTHYN/HZrTP5S6spe7OLfi+Jp4v8yE3+UgxZXu7e7D+g8f+ENh8l+
l3IWdAAv6Gxg+4rbBU/gff2ds85/2GdNeZ0CajS1ayO1ejP1uSm6rleQWcaW
3sjvjcqJ2Siv5fb/9F9+5n5/8bGvZNfmtQ7Wfu8HPxTX4/Frct77H3rEybVz
8Pe2vvoNb1a/Dx9byv72m99Sv0H2/Q8+Isqy1D864cZBtrbH8zdFbNe8zzO/
Hw7Wxe3qFfUU2YUJZAJuWq7GDfNxfFdPngl0YP3XIkzt+yc+9aciBliGX9cb
8aE3u+MlL1N2VsuBajaG8rXOT3zq00o+YAvzgBnMbWyv8bztFfP2fNGz+txX
Go9zwbL8uvODXczHMtj0luQN7Y3Zvtlafu6x3oysjft97nZtNGdD/gux58rm
aHVb4n3/fGD61OXb+fKlDD+Xe/5i/tSd+1LfOj95tDm/Ut8qH//Ut86HXalv
nc9Yp751rvEpG/en9/eKjcdEllUaz65zi125mlO9VmzU12LejdXNc6UyGJTL
0/NFof84hzei73zY1tbbn7XnBM9WQ229A+rd0wB7RqinYM6sbQ3G+wK57SXk
tgvrYjrjtsnrW3skOwYK/WvdD+v68ueBrGV/b9YC8e3qyWl/dv3zblV3OS98
6cuyf/vFv2Wf/YvPZQ3tnYrgm/RZc6e0euJk9utf/zqf/ws1d9bcAZ01d02N
ljq61Jidg/d3v+8+9a6/U1OCcZiH79dNnZquf94tahyeTeabdnMnntfy/+rr
X1c6MHl5Xe5+4LtPPJ7TE0pfU2e3ktGcP60ceFrZTV2agB+oubtH5X5rTi15
zQPsWmwPAwzzefgO/4YXzOm1Srb9Fq99fPFNRxRPb/8B7Xe7vid64I0r2cnV
0ZzGslNro/r3mqbx8QGDSXf+Pqb4T62POfubjPwvvv9KRfbd6rRr4NmI7jkc
9u0+BnRO343ou4U5J7PJ2EJ42z0/6LnpzKSafyfc13z++uwbj5whscZrm2AM
xfSL7z+pSMegx2EJdGDggJL3N4+eyXVMOYxOroyq+0DwX8Xb5B/Mad9HtR5E
/s4E8eW46/WdJm+6EH6j2r9ViifGysYA64FYAy/E/g253xBrWP/OVy6anNL3
9DafWlzN7clz6Zji1XvUUKehHBf3jub9mv2ELI6QnzCPcyeIi4qn9t/7pHEF
jCx2Omdxvo6pp81vyFOfr3RfNTH7gPT+Oeb2msIjr1Wt+/t03VK1K1ZTB8Tx
sJbjWlutNpehgbwGDwiyq9fwcn0q3tdaC+Rzv7//wx863g89/Gh2Nq/38P7L
X/27egf60lf+MnvBS1/O+PsVL8xruQNuzsnpQTp6Bgh/m+MfyGYWl5UOeP+7
b307q5y+mspxtv5IybR9Hmz6k09/RvHBWs07EKyDOxUsz2LwfTP+QD5+veXv
0fYCz9HFFdEujOVPf/Yz9Vtj9Irsnnvfp2yy+q0ObhfGEY8TjHr8OksQFxtX
KRbtFtsf/BDZOhDEBZ8PYrbAeLtgexuKNSfA6bVveovCCrAZGp/K5PNHbK8K
cz1h3qdzH9vXmzj3wZ6kPlGcid9ITqutqZLsoD5Vt704NnL9tvLaC/XhuYGC
94I4VqH2AEeEQU/IizFtZe98X+oeIuVBMY54b/AeFORyD4uj9Jv0xD5qJ/c9
4A9zsb2nP56XPQW9uWzN4LLFvVhCtvO/L863gT0qyu+R4hKrZUUy+P4t8rUI
Y4Enlh/IPlnegCyjNMX3fXGu2bELfe5LfasoR8QcS32rCqapb4m5nPpWXHbq
W/GcYPalvuX1Ph2+V5yTLURufN+ItbFUX5ZrR9jnBwpj19pDZfBcbO/Fdczj
FMSvKpZCPyzVlzC/lNPl64D9vtDWE+MfEPpLnNrJ+pJnKQmXDX2v6C/sC5iP
5roe7+wbzGkI0WBI/UOMR+ItMx6Rb8f77RONkTUxGZyXyTPvHZK8fkkHGwM5
lkRbBrKOA7n8A7qO6V59QN1ptXYfyO697wPqLmfg0Fj2Z5/7nLrLajL3DECP
fvSj6v6qqaNL3evo+S53B3TDzbeoexu4F7J0g7mHgid8p773vverd8XX1ZO1
5GOaetQ4zLeo8V71Z8dueP5tev3zb81t7FF3SGCrfvYo3he//BXZ47k99u87
xKTl7TcyexQfkH23BPKtHnhvNfdVQI8/+aQifVY4oHDT2PUp/Bzt139f3xtf
eIzYqO3sVXLPrOu/Y+/M+pjBSt99FdEbX3DMYeLXjzuZinIdX/zASUV+3Ooc
Q2v2K3nw7v33+IRzPcgGc69g5HLe/sH+7BuPntH/H7DPX69s+eQ9FcUHc1gm
X2txAvpSvg6IxFvlqPZp6dghJRd0cKzgvqXZ3HVCvp02eJ3OMWgh+YbuTHJe
j9GYmnN3RZ371Vo8h3mbWR75GOh3sNXa+cQnr1VzD75pNXvp846qfFHUg+mA
f+Y+Qy7BWp1PB1Be9aK9APj0OnrjC+fZGsrrZfa4PRSLC/hn7xUtJhaPIrL5
eWZd/1m2M5UJZMuB0HZ4z/3V+2de+Q4YtPf4Xt4BlNeuDqHGdgV1saAO9+M5
qX6zmm35cX/p92s7iIxYP5B0cPvQez/WPyT0hCJbZb3/8rOfu/cv/+VXsxtv
uU29f/Pb33E8n/zTz5rxIXWXNDJ1RMl80cteoeasjVYW/HnX17/5ber37NKq
kgvzIMPydzjdQ9mDj3xYyeJ+w7i1x8vneOrfnteuu93JApu1Xffldr01sMvr
H8xGJqcdvx5/ZTR2IEPh1K/xAppdWlO6tZ8hvrDG2WTiAnpuANv7MUY8VkPE
F/sb7IU5FYvPfNbJVHr76XrwRftPc8zjOmhsXAtsB5s8RkeI/XbtiauuNTYd
UXIs1vKZB59zhLOMeLaR3tO5b1PnvsBerq+objEb+ofo7/6Y3yy+yLYOETNP
HWXjR2pdEeZFsY2tYz5H/eTjMRyknC6aL7N2kO3LAv5IDhTajn3ul/iq5E21
/VQG//6IbUVY91dby/OozL7R1EX2TlG8ByP6i+rURmrTRvOlTDxwbZD2T1xn
l6Crq5+vYTKD3I3EgeBJxy/OuY/Li+mtgnvqW6lvVd2LsdjG1jGfU9+qYvMG
9mthLLjdRfUlglfqW5uMB64N0v6J63x29a0YRpIdJfIvyDmv93x8ryiXM2zP
9Usyi9aGPsl7ZKP5Xi3Pq/m8kb1SYGeQO2WwpPHscDks2Vsyh/q9zK6gxpXF
rWxOsnysinNEXvC9okRekjWhf/sHh3M6qJ8DiAZjdJD+Hjho1hwM10myBiQ5
gg6Qh+1y4+iZU7f53T0gyTwY2juI7Cz0M/S1e5DZ4eTkcwNDijTGA1knfBfu
1f8d1uNPPpH9809+krV192Zveuvb1L3OzPyi++7/43/+5+yvvv43+e/u7N77
7nPzx0+dUb9vvOUW/W28qyeX0aPk3HSLvh86cebq/P1A9t7367sy9Z0aqNtQ
/hvGYR5+t/f05nQgu/kWfY8Fz/b9fflYn7a3p0/Nu+d+TW37ezXlur28Xqfv
8SeeVPdYeq1Zl+u6+VajJ3+2GbkdPfrb+RNPPqmoE+5Tc7wUdgc8fpY6Dui/
m+5NL5o3dlnb7LM3u6oyqXiuOj5pbOpV9xtveuExZXOrsR3o8NSw4r3l2mm1
ti238+oT+l4Ans7//frew97/WF3t5i7kquPmLiF/wvubzD0H9t/iAbb/+PM3
EHycT/stv+f1cnqzh968lv1TvnZlYVT7YWOM1rQpXb3UBuOD9+OU86PD3F9A
fno+b0cbirn1X+FnxqzvV1UmHK4275RfcGeS/16ZHyUYaZ5eg5+OGfDgd3g6
nHo0js4Gg+sTn7o2+9sPX5UNDg+4nFB5daAvzyeaPy6f1Hi/yqc3G4w1JqeM
fP0E26cPj7g90ZE/O3N684sWDLY+h+0TcPRxQ3vH/IY8dPE3GHuf9N3T4amD
xv9xt9f0HdoBdT8Hc8rf/H16asTk8BFnY4e1t7fPUL97B17wGXzvVKTPfEDd
/UOmfqF6N8Bq78BBP0bm9bN74GDYPwZo3aXyI/0Eyw5qNB8X+gOv0a4PCf0p
1qMkGhD6lRn79Gf/LPvWt7+Tffozf5b99re/zW6+7Q6l/yWveFX2re98R89/
57/oHpXTD370j9ljX/1a9tCHP5L97Oc/V7+B57G//KqSAT1tdHpGzQEP8N98
6x1qLciGcc3/NcUP4yevPqv4gP+xr35V8c+vrhtdXzU2DSv7YC3QTfkY8MNv
qwd45lcrzkZYB33Pzo1OH6V2Gbn2/aFH9RN8J3Y9+hFkh8+lN771ruzd73u/
+w2ybbzU75wX5r/17f+i7IR7LhuvmL3gO8Zo9PCMWg82KN8Bs0EvQ47F1xAf
zXuLM8gEm2Fc2ZjHGNaoWBsff4Pw/sGPfqTyhGDE9hLwAcbgwxuUbHTm4ecb
t/fCfdId5GrRfiuzD9K5Lzj3Id5uwe5uws98M9QtxpbjwH4jP7qDnED2DeCa
y9azuAU4YruxHyIf7wsHkQ/IxiAnYnW+SmxITxJ40Hh3LL8BG4I7taWb24X3
mvEtwN7xF+T4APsd2ycOOxsf5BOOG48Pk+1zkPVvbkNRn5V84jJ538d7QIp3
2X0Y2FWAubS/uf1oL3UTX4X1OW8P9y9WQ0itYj5H685BH0t2num2dQfvLzFf
sD6p7g3TeZZfsZ5yUc59qW8Jfqa+RdanvpX6VnTvp75leVPfuoB962n2vSKK
P6vp4b9XSjFEeznWlyW/GNZ0L8h1uXuQ2hucFQzFxrkvgX+BzTj/CuoF6SUH
yXpS/4rswjUS9zqpLsTyS8orvo+lmAzaenuQ1habIzwX2V7ye5/HUN6jcl4h
PYX9Vsqf4ezAwUOahg/536VpJPKbyutV76PheqazN6pHWLthu0eE9xEmZwTJ
G0XPkUI9vUMjinpyPHsstvBtGP4bhL4BdY/z0Y9/XH1nHhmfVO/ve/8Hsvbu
3uzowpK7F4I7C3u/9Oa33aW+1//6N7/OPvqxfG3vAf+tOieQB3Od6rv9gex9
H/igWqfuhHr7HME3bKUvn1dj8A0753nebXeo8bfcdXfWdWBAU58h87uzz/Bb
MjK1vPu9rgP6TuoXv/iFHuvrd3rectfbjZ63K1uxnl/88hfZ3/zt3+ZYDSq8
fC7nv/sNhooG1Hf4t7x4IV/X7+zVOgaUnmtO6DuQa05MGUz6soffsqbuOjoQ
HkAvf77+O95mjhzSMnKZ11w5pcZefssc87lf3ZkAeQz0uJWztjiudMIT3m+7
9ojjsQR2fOpdJ8ydipYNvGCjvsfzBGNw52Hx/fIHTymyvgaE8LZ3LVZWl7nL
gSfIeOJT16EYDJKYOxusPIPjl/J1X1L6+9yYx3vSY2JsUHcmL5534+DLva9e
DuyGMe9nv4qdisHz55wsKxfwV/j1Ih25r50oH7rVncyAyR1DNqcgv/rNnU3+
fMtLFlxOwW94vtkQ6IK5V+S54LHqVzwwfjTPG4Vdv5nr1/rXl/Td3m3Xzfg9
ZTB18T9g7mzzp49rn4s16NZY9RG8ISdUDvb2K5kgG2L58FvXvR2c0Ljy9SWL
yne1v+CsO3jQ1Sxbw8QaOMxrJ6/HQm3F9RL/HorU92Grp0StH8Z6RwUebo8g
h9dzaUzqIQGf1nXmuutzOps98pGPZs+/4wVufmn9uJrDctR7LuP5d9yZTRyd
02Nnrw/4gEDWxMycwwl+L+YylypYrrYB5oAfdFqfiD3DWrfls37AmOMx8dHr
XhjYHNg1rHs76JyYOab4ltaO+77J7Rqm+WXtgffx/OlsN5hQHM+SeP3wR//o
3rG9E0YOxUjbLMaC+DNiYnmDnL/INmxrNNZnz3q8TaxIjA6ycw/ZN1XyuCjP
ic2jkfEyOV9N77P33Be3aTSCcwQrXiuHuX0xXZavSt0U5URyR9Rv3odwrBA2
QzEdVXTZ8WiuIh1FPUjCsQgHm5PDjAqxi+FVsBcIX5X4cBoqEVsJN77/RAp5
Azyq5k9krmrNsvwlfCqyg+eew8mQ0KODOEblx2I6QmVLOgqp2rkpgqN0Zoqc
QwpjZXEX5y+Rc1/qW3FsU98ScCnSn/pW6lvV8InFKcaf+lbqW0//7xXxOBbU
V4xTqf6ife0tm3PVerykcyjUV8iP66jlH7K/BbuGhfwyFNSpol4l4lXC3mrk
cr3grLQR2cHeCTF5Sr5XbMZexDMwMqb+vjtF5ne//W3eBw+FPME69z5OKR/v
V/PjaN04Wzvux0bQGJY5Iui0tuW8g8SO8bi9WMYhbDN/9/YPYhucLd7e/mHw
cTTry/HsM+fN3sER9d9E3HrnC9Q9zstf9Zqsu09/1/7JT36Sff0b31B/xgPu
kWB+fsl8u+7R90Sf+/wXct6+7I8//nH1fsvtd2T71bf6AfX7N7/5dfbHn/hE
tr9fj73/gx9SfPvhW76iAUcw/v4P3q9+7wfqH8pl3KnG4S5pcW096+mH/4Zj
KLvrHe/Ifw9l49Mz2ZPf+1525tqz7g4A6P33az10fEjxaj0fcrxjh49kP/np
T6meXAfgctc77lHjd93zzuzA4HDWp84/w56GhtWZqDd/9g7qP6fy1j9aNOu9
XmvD2ZPTigeeai7387qTh933e8CpJx+vrExmT37quuzL9592du4f8Othbvbo
qJab4/jKW4+5v9PtrUoOyB7KDo0NZ3/34asVP8bHjjkZ+Risg/V3nD2qdGka
cnKdzQNg34S623n0LRWlH2wGv2EMbLf6QTbIBTuUPDXudWHfIJ6A2ZfvP+X8
sDieWJnKjudy4fcjb13PXnnbMS9P4WoxXFAY2nyz2MJT55Rfg7EC3e99zYr3
s0/bCb/Bp/e9ZtXZTmIwM6r9zMnGAJ427hC/v/vI1dno+IjLqbGJ0extL13W
Z2nIn4P62Tdk8knd1+h8Akw9Tvn6vkHiw6fffULZdwjkG/ze+kdLas2fvvvK
XO8hNQb677j+qLYh5wObwP652TFlE5DVhflAB8QDyGJicwX0upzICfLGYWp8
BTmvvE3/+S+IWc+gvpsCex7LsbExtaT3z1LWm/P0Dtl9ltervPf05f2gP+/t
UMOCuulq4zirw+Okbg6idYOSDFvjR2htJXX/EJeb13XSg8apbl7rg77Ce9OY
kxddE/QGRiPjmdwv9O9HP/qx7NYXvDDSG6VeymWxnhfwcnvHZbkjuo+/9e53
ZFefvYnJGvdYjEiYmnXX3+h41W+xR/KeH+I4iMYHAxlCDsTyguH1o3/8P008
x9V/p/GKV78291mwQczlSL+P4R+sk84sFN9i4vKlvKLjgzbOI2x9kT58VnEx
lnIrnfs2eu6jv1Esgr3G83vcrfU65L3j9mnEl0FnK5Lh5vgaIadHIpiNhPLC
GjSeiT5HcWdrwW4xd6vkc0GtCfVxjKVaw9bwnONYCfk2aOptyFd2n1s5Fhcc
e5x7wtpY3yrAZlDMff97sLQ8KUe4/BBT3BPCmjIe8VUYI3w8twpibP2M9nxW
h2K9SsilQVRH+vmaWP0cQfVXlCv5qam/ar+RcrBajvCeIdlxvs99Qh4QG1Lf
ErFKfYv6lvqWjFXqWywWIU/qW6lvbbxvhXF5unyvkPsuzneOcSxmUm9mutH6
2PcKsfaU+F7BeUX7eG5EavFT9b3C723Pm75XSGNC3Ynllhk/ODZpaII+x+3v
iXB+fFJYh9/9uiHy7mmYvQ8RPcLvcT82HMxLNCmQ4AvhxbIlf8DO0Mah0XFF
g6qOjebY6u/CcKf1wQceUPc1E9NHs94BfS/xsU98Qo3Bnx35/Be/qO549vf1
6/uAnOBOCMaAd/zwjLsjgjshey/05Pe/l00cOZodGDyovst/4EP6fql3YCh/
13SbuUPDBOtgze13vlC9f+FLX1LP733/+9kvf/lL9fvue96ZTc3MqjF4/+m/
/DRf9311dwbvH/+TT2YHhg6a+4IR9RvkwnpL8A68sObqszdQPb/Ser745S8r
nAaARhjZb+vwPHhIfYe/S91RDDud4Iel608fUTw35E89NqyerzLf+p/89HXq
+z78huf45KiS0zek79BuOHPEzdknrIHfj76tkl25OqXuF7AceL9ybQphMZy/
T7t1X0Ey7nvtmtLTZ+5V4DeMw70HyIEnkB0bnzxkfB3OJibHlCw7Z/VrTJaM
/oNk3NINZ2ac3sfM3Y+6a8nt+sqHzjhsYP59r10lWFnb4ffYxCGXa28z9zmY
YExjrv/OxrvUu44NYP0Yst/6aePQp+wfVrErioHGQ/OeJDifcTIBy5uuOopy
6ZAmm0vD+q4Zcgn4cQ65u6789/ys/rN1r759wdyF+TUffttxpQf0whPGFuYm
FYYn1w4XxH/Y00EdDyCXzyanwVeOFcTmAF6vZIxk971uzcUMx1PHfVg9eayA
T+MyqmtW3gdU/RrlNZrVwPFYnQ3r8bBYX4vG/NxQVL5Qz8eldVJvKu5T8X4h
9SbJLm0L3M9fd+PNVWyojkGR7cOiXNyXY7jx2MSwCW38s7/4nOl9ct8sjnNx
T/X44R6r/RzC68apPLDJ/p6ZX8i+81//Psf/nYJu7rOUZ7JNPPbymiLsEN84
twHHM5YDm8FZOrOUybvJdO6L5Kh07ovaXE1WgGPR2mo1pFrMi3QVYVMNS0ln
lVwMckuWPxToqbY36PuwqKM47+WeE8OqzL4rWROiOeyfcv+pjkMs14tt2RjP
EJFdvC84L93P0jOCX1BHq/ldBb9x2fYh0Z7QL7oG8Uhyx6V9EounvM/oXuF5
MhH8jtnqeamOi3Xu45T6VgznSUG2nCupb8VskfZKRF7qWxEfYj6Ftan6Gsnm
WMzK4VnEk/oW9xHxpL4ViYeM5dP5e0Xct1jOFNU12faL8b3C53RRLYmtL5PH
Exv+XhH3NcztZ+f3iqJ857lL+Q9NHM4OTSLi74qmQh5MeEzx5jQxZd6n3fwI
5uG/ka4RQQ/8v9idzAlu5xS1fWJK9oXb7WzE/OHvESYf7BvBtuVyhidMfOFb
sLvXGsu+9wN9b9N/0Hx7hjukF+g7JLhLgnueL3zxS+hb+sHsgx/Sd18rlePq
e3f/0Eh27fU3Zvc/+EB2/wMPZtfecKO55zmkngMHD+Xjeg38+Yp+oIMj2dTR
ueyOF77I3RnBb5AzkM9Nz85ld77oRfnzWLZ24srsHe96V/ahBx/Kzt50s7oz
Hjw0qujsjTdlH3roITUHPOvHT6q7OnVfZ3jg99kbb8zO3nBTNjg8mr3mdW9Q
/PCcnjum76pyHlgLMh4weobgHtVg5facxe9QPma/sY/o/8fS3S9b0f6O2Luv
Eef/a+7Qf+Zl8diUubcY0ZTPLS5MZa+5c1Gtv/mq2dzeQ4rAfv0tfzQ7fHhC
3X8cPjyeLeX8b895gf9UZVrrMgQ8MPeim+azqXyNlqUxsFjA8+arZ7O7X76S
vTbXu7Q45eeRTrAFbAPdwGflDuY+AY5KNpJ5+vgRxaP5jql1isfYdqpyJPvI
XccVDjflsoHAxgFj3+l8HngA05uvnsventv3ulyvlqN1gO/WFu3/EY2xwn1E
YQsY3/3SFROT5ezGHJPF+SkXl5vsO/gwPOrwAf3Wfvjt/DRPHYNZFYNFEwPN
O61lDB8iuTdgcH77y1ZzX1azm6+ZUzmDa7ir4zA+OqZyDvwE38B+yA/tn94z
joYPkZwD+wAb0AeYq1jkOu/OdS8vHDb/7cG4izHYAna99s4ltc7nh/3vPHQ8
nW8joy6WIlZoPRD4oSj3aXnxsIoj2PPim+ezw9MT2p6cD36DzeDLR+46oew6
c+Ko2mfDQOP6XD4yntfdvIaNmPo5Molr7xSt2VLtRTTCay+vt7hn4N4wKckX
ZEk9g8uc9Dp9P/H1fiSQN0VrP/GV67P24nVTbPyw02Expfbl71OYF+E4McX4
pyhmga9TAkbM5wlkg/J/iq51/RnHE81JseFy0TyNJ+/PDHcbE9Jz0RjSSezA
/Vo6U+DzgHhWYVhL78oGfE6Y8nw8ZpM+5u7cIJ6TQoyD/GV8o/jMEZzPfE77
vRee2Uawf+ncx9Zv/NyHYxzuHZQjZD97v0ei+0LAlu8BkiMUuxHuf5DXdM/J
tVOohVw/33dIjsMK28niNIL3jK3VrhYJ+1KMFcPHYR7B0e4T7A/by36foDxE
+nGNl+rzCMFmiumxPKwmkdyI5DGLndgLJryN1EfMJ+UlzymOG8+DKdrngz3G
8Cf7dcrrmaB6Rgr5D6MaNxXV53iwv+IzzF+rn+gQ6iGtxdMeU3E/4xof1oGg
vwvnjbD2sdgE+rlMQQ4/D10S577Ut6J2p77l85XnDrFdiDfWL+x7nGupb6W+
FcSJYJv6Vpi/3D4ui+P6TOtbUt7EcEnfK+R9xjBP3ysiuR/B3tmA6zHatzxm
uN5MXhrfK3D9D9aTOkRzZvzwEU3TR/zvGE0jqsa7GYrJfar0bdQOgcamph1B
7EZNXsH/B2t2flHdIX35sceyoUOj6j5Gf6s+5P5sEjxf87rXq+/o+nv+oezO
F71YjcOdD6xR9z2WRi2NE3rgoYfUmoOIV3/rHlXjcHfk5KB1w0Bj4/pbtvqe
bWhMpmFDsfGQxj2NYx2T2SEg9+8nnPQ8fGcfGdP/L6Z3vHyV+W2wyH366N0n
FA/HZZj4OE5sq2r/OLOfrY9hoX2cLMSxGLNN4pz7CRgBDoHP49YfGoOYnVgm
wdDg+rxr9P8bDJ4B1iyvzhuNC5ggP1TOVMmnESPD4kT3iqfpaZ9zsRwi+2Uc
xZyTxFsqByaRrkkmYzJCsnzw5Z5XrJF9B7Vq1PScscO6hl20mnteaObcavzF
6G3n3Oc24POm+C9Rmr4QvpSQfzHz5ULpSee+RBcY1zH1xPvvwtStsXPOg0us
vkb37Tn2ygvtw8XG8YLG/ALmPcf2ksd6A3amvpXoAuOa+tZ5xD71raeYUt96
2uCfvlc8vSl9rzgvNBYZnzpyVNPMUf8bv894mjzCxsz75IwgQ/o9E9HDx5je
QF/M5jJUzUYsP7YWYzI9k/PPqOdEjueEwhW+CR/O3vDmN6s7pHe95z3um/KI
+eb/zW9+0/1df7PH5tV9zDC6D1D3X195LDs0PqG/OxsanchpHJH6Hj2ZPfTw
I2qN59ffs+EJ4w8+/HB2aEzPkfWTU4rA3rHJ3G74po1o1D5hfsq/e/JrRtm8
/p3LnjBkeMbzJ2A0PmUIcJs22MF+sOOTh41dU+47PNxHjExMGj81XXNS/3mT
P347/H+L9J2F8mvCk7MD+8RsFX2cmKJkx6dCjMamGEWwdHiy36PCmgBXrhfZ
Bn6+85XrCgvnt8XAypjycsaRTEm+k8uwBLr1Ov33NcLT5eFkyKtwx3o4NlPI
F+6/s2varR1lvCqXwBe4S85zasLUNb8XUT7Z+AJOr9B/Hx++73H3P/nzHjO/
vjyD9pnHNMgDwXbym/OxMZd/kwJGQs5ov/0duo3nqMAL9oIvkBtjZh3QhNl3
qoZB/copWpuFOmxr5aTtAbHaat4ni+p1rP/E6jHXNYN4S/QYbAvuX5NIDvmt
eGdFO8R+hO3h+th7ke/iu9SDBYwm+W+hT8d6XTRWPI4zrP/zuVifnRF0R+RW
7cdFMSF2zIrrA5wkDHi+SGeBmL0xvGdCOyaZTMknnkuxs1HAH5Er2prOfYWY
RLGL1MhYbklzUbxK+hTU4phPji+sa5NMVojNLNEXrTUFuYL5gnoj1PTJQvxn
Zb+F+HJb4z4WYx7siwjusZoyGfNdwrVMngp6nQyh1kR7V9GYNF4tX2eEnGR2
8LWu3854H2K9RKq71fYTyQXek6rEMMYr9nWeqzOhPpIHku6If/yMEt1HZfM4
ZncEv6fq3Jf6VhW7qsWvAJModtX2SCQXU9+K50fqWxG9VSj1rbh9qW8V+3Yx
+1ZhnkfwTd8r5LH0vQKez67vFYWY4T0j7A/4++WOHJ3Tz5ymj5qnGbe/7Tx+
V7xH/Rr8e5qtmz5KZRwJZM7mz9lQz1FPWA62geuI2oB8xfZzn6YRL8bkyCzF
Ss/NZodzms5xPWzzw9xp/elnP6PukE5ddbX5pjwV3Dv9y89+pt7HJibdd3+4
C/hZPv6rf/+V/j6d0wTc+6jv9PYbdE5TmmD84UcfVfLGDT++T/nWt7+d/fa3
v8ne+Ja3ZM+75VYnB2gSnuYOCe7jpo7M0Oc0fcc0Ze7v7D3eVC7HkpU5YX5P
gvzpI2j9EbUe8Dpsnwa/w0fo3SDYCN/hP/aOk9lt1y8ouv3sQvayW5bysSvV
3D9/4cbs+Oos8W3C3GtMonsy7gO2f9LYPul8mHb2Wzl2fnKaYTCtf08hzPj8
5DTVO8XGptC4JYK9fTd2Tbp7CG/j7TcsZe9+dUWPHT4S8E3ma7E+HtcpYrOP
H9UznZ1Ym8ve9apKdmJ9juZSTl978OrsTS9ZI3ZNTjMi/mDfTQ5JNkmEcD+M
6tthUnMRP2CR75l3vUrf992W5xHQrWfn1fOlKqdOqrkPvemE3k/EP2O/ygW2
X6ZDLEVbzfOBN1+pCOedJZ+HLFcRuTw5wvKaxyy39V15TtyR54a16zDC6/CM
qWFHZ4N66WviLK27Qm0N1gn1+gir5a5uH2W1t8rT1/nZQL6ty2IfiNR7seeQ
Oi/0i6PCk/lwhM+xHhX4zynmO+qVQe+265DN3H7sg8KQ9TiOBe+vhbGO9Ohq
a6WeT+IgnS+OynqDuLGeH66fpeM2r47KeX9EwJLgLGEi4HKE28T3C889fNbi
TzbGzzgEE7yW4Z/OfSXOfRJWJBdnqVwhpyXsY3uC7+/AHx4jjlOslkTGOX8s
rgSjCJY0dqjHHJ0VdeJ8l/Y1xjeUX7w+VquoDbOET6r5vLaQWs/mef3jNuOY
HjkqYO/eZwlOQY1me4Xub9rbpX0Sy0Weg5xHysUo7sw+4juzK5rTTM8RPibo
xb0Q12nApWgP8vqGa3CwF7g9zK6g5khxiNRwUg+dDVXOZEIeiPtU2l9u/MKd
+1LfCvFMfYvhIeEo4JX6lhBjjIGQI3w+9a14Tqa+JdgpYPps6FtBrglPj2f6
XkFqB483y/P0vSKyp55B3yvkPkn9F/s7sv/osXmRZo8toPcF9WeG4rzxObu+
aN1shDccl+XEZFa3q5ysQj/mjimaAVL4HtPnnJx+/vOfZ//+77/S39WP+Dse
uEt4/q23q3unRx79sPo+ru4X0Hfsz3z2s2r+qmuvc9+cLU2j3/b90Y98RPG7
u5Qj+vs2PM9cc2327e98R83/8Ec/CmToXJl1T0VztH8ov2b5GO6Rs46m2W9M
R0z/BZpRdMw8JZlzyk4guFeQ6GsPXa3ubRYXj7q7DOvf9Iyg29pqfFJ6kb3Y
fr5Wnx1mA38D2xle4jy3p4iwjKiNpoZh/x3fnJ9nco8I/oexM3XSvrPc4/kI
cXnPa47T2M9G4lCVwvwitrM8Vbk0R3NqZm7O+wM2Hj2q7CvKqVfctuLu07xv
swhXbtNcAa6Sr3PZf37oGkXTUb/D/cL34QzG4CjPjzkaA7YHHVaGoI65uj9f
vsbPxn7Pb6zWzqIa6+Q4GfHaf251fkHsFdSnBWZj9b4m2TQrrPFj1XvbhvpS
gKnXIfnKe7Gkw58JJDwEHOeLYhPaLOET2hLDaUGIGT+3LBD/JF83kktFOUPs
FXDgvsZ8L5vvxftzI7mVzn0b2V84dvKe4TLLx2Jz+Ub1FK9ZCPMuqNlF9oY+
l8G/ONd1neH9gPsU0xGXXS6vY3jpXsZrTBz32LzPB6kPFNtTbd+EazfWV0Rf
5uO2lNonG8onqWdFeCNni8Lzx/xm60OYg+X8kfagsOeCmEk+CfkyL+upVu/l
vLw0zn3VsIjLSX0r9a3qvsfyI/Wt1LcIpb6V+lbpeCM56XtFCX/T94r0vaLc
muI897+PLSxugJayOfN7rgqvND8n/MbPamu4LdV4NmOjxFPENze/4Gh23u8X
/W3YfGuGuw5+94C+/x8hdw7ofsTckeDv8UcJHTM0R/lmqV5JnpVhv2HbfcKJ
144YT9F8lAxmsxhDLNfeERpf5O/94X3SDMKmrA+4hpb1KzZfbVyyp8yacN7b
OuNywd+ruvcCDMrgQmoMl43vcYXxsH8U699sLuF9OIfyKvDD3TfP0fsisw8V
oVyj+yWOqdRryu6huIyF0jGL5g/Lh9j+s1SmD6h6uBjW8DK1s0yNLytXWlck
Pya3bE87lx6yEXm8T27Gvmp9K2bfxnpwOZ4i+zerb3N+L20Kk7LjsbhtxrdY
TKTzUqn4LJ4LvuncV5ZXqk1FMat6vtyAvRvNr3PVu9l9Gqvf5zteG9NRPa82
m3dFsS6zJpyP2yrVoHOp6XKdWQp4iuSX8ftcan6ZPAh/L0XXbw6TjedfXIbc
pzYTx9gaTmWxfSrPfaKs1Lc2htcG45n6VnlKfWtja8L51Lc2mgfh79S3LrW+
VVZuWZylWrsRfRvFdTM8MR9iObZROWXtO5caUW3ufOXy5v1+9n2vKGOvxLOw
tIxoST8Xl9h4AS3y9UuRcb4W61pi/Hx9EcVsLfJhyT8DX5fCtYuSPP0+n6+f
z2ukfmrS2KLv6vDNeA59P56bJ3dRs8cwoW/z7vs8fy4o+Za0DkzHDM0jmceM
vHnFM2vWaBk0J7wvi5oWzPvCEvHT+n0Mjy1gDDAtsLWyLL7W3295P2YJnsco
Xsc0TsSv+YVQz0KBD8ZvYv+8xYavD30PfVokcrW+xYznTfC+wOdxjvG4LRA7
yRj5vYBiwWMtxWEhIpPm4DGWk0X6i+Mv2USxC3GPreF7Eu3LOZo7R9nd56x5
+j3I98qC3ytSLsRsxfYgXKmM4rjIeYXlFuPv9zi1Ua6Hkbop1ezC2i3U1cjc
fLRfcBlLEdlL7Aky5fFyfcP4ivyal3gXuQ2CrFJ9zds5X7UPlcW+hI+BjDLn
gCJMy+Ad0nxgB8eTx30zZwCJZ6P80nrJTuRPNP6y7vlCPr5nC2wrPI+lc99G
z31V/ZP8WeR6yuZvgU+LiKJ7xPps8qkqxiw2Ab85+5aKW7WcKjvP86tErIJ6
HK6fD3TF4ifldzE+Un+aF+NdZHc1vKrtO6lHFvdKnC9xmWwfR22slsNFNkVi
UhTfahTkyUawZ/ylem2VfCkVt6K8kmwvKwfHumTdlGp2VTyKYl+lhqa+RfFP
fasKTqlvifpS36KyUt+qEp8q+ZL6lntP3yu8nel7hYTnRvtmGfs3wy+tj9XX
5YvwvSIWVyk2VO7SympOK9ly/lzOn0vquWrG7e8VRKtkDX73YytofT62zHkp
uTXLbG7Z2rQS2LaM1i0TH7A9q8SeZSJnBdlmiftBfcFzywiTxeXlbCnHcymX
tbgE7ysoFuYbsflmvGAIf2+GOU9LZo3nXwQSYrioyMwtLInr6R2A1mFlLaL8
UT4sr2gf0BPGlR42vmTmPJ8ZXzJrgnksI1wXyAY5DsNF941+nt/H2HsR820e
cHD+OZ+WnV6sa2lJto36T+0N55dDuUwv1iGtW0J4Of4lPLdM7JBk23jofND1
YonPLy0j+1hMl7xNNGYhHjh3lJ2Lyyg/l9x7aCfFgdq0QjFeCvMwnp907dIy
j5+x29qJ9prKm+BezuaS4UP1ktti4yP7gPEs4ENjPt5SLPweW+IYkDjT3FW/
F5cJH9ah+IL6v0LqMK7BtlZjfl6Lad3kY2jdcji2JNR6LlPqS76OU5lF/Yn2
HCNnWeht4K/1eTnscU7W8krQc1SsmJ1h/+P44R5D55e5XufLCpMj+L7M9UpY
c/krTvbySgz7cD3lp/bz2HJfPc7eN7nfr2Q87ssMv2Xmg8+VVeQXi8myPxtI
5wieM9L5gOqXcJVwW2H7Ap+FwpzB5yMec+ksRm1O575zOfctYR+5DwxzYuMy
s4X4gDBc5rhiWTZPZJ+x3TxGUp5IeUj3C9+vzM9laqeEY5ijguzlMJZh7qIa
b21cpjlL85r6sbwcl8/rH6+1dJ8yuUxvmI90ndinSD1aIXZIsnH9DOuNr3G0
JlG8w/oR1nUeV4sjwXx5NWInz11sE6tly2EexvOTrl1e4fHD9QTvt3gf8/tO
qvE8DjEf5Loi9W4uT46F32NSrZN7+4pbQ+sH2z9BbvOz0YU893FZ0t7HcUh9
K/UtqS6kvpX6VpgrGMfUt1LfOn99K6z1QWyEmPF/b43V7tA3lpvpewWTv5Kl
7xU8LuG+5D0nrMNh3KR6RvcFrV3hurBW4LhJNVPCisdiZW09W11dy1bXLK2r
54qhVYkM/4p95ms8P12/YviJPPu+aoj95vx4zOrD9qxwu9awfeuUb5X6tcLs
x36srFIfVo2N3L7l1dV8fFU9MS0tr5C42ae9p1iy90fmPoDmt+dfMfKtDv17
zemB3/xMsLTM9wuWuWrw9raurBhaXTXz+h37huO0GsQEEbKX2rxKY7rG8gjz
OVv0/vT3NPbbu38uWvwMH44BxWnV5xjWTfJk3ftgaxiyh+RXsDfWxRzzvOsk
f/A+WEW48lzENvMcc7SC5lws1wj+y0JcloVxu1ZatwwyV2RenEvLK1SP4llB
7/lvWnfoHrV4aN/XI/kSwcvwL1t7kX2knwX3Tcvkjsf9e2NOdn8sE7/RGN4H
1v6VEGeC90rxGPaT109avzD+a4ZCW2N2cCy5bFL3V4V4ReKi98N6IBPXUYxZ
UEtQfwnsWQ3jTmv8OrHZ1y60B9k+DHxbDeW7cd6jVteZzSGOK6sCDkwm/x3G
3/aqdXkdwpvXDz4e8KyiWDnM1mlcV2kd8+NhD+XY8pjyOirlOO41q0xekDuo
jsfOLitMVrCXVhl+OJcwBgiXwF6+X9coHz0v+ZrF7Qzn16n9axIWjC/IDySP
5Hs69+EejX2InfuCPYRyhs87vHj/F+qphL8fo5gHPkXk+RixOPN9HGBO4xSd
L7Q5xIL2iTDH/G+5rhKbWD2mdZLmfrCP2bnP+Sf0BMkHsnY1nMM5dS7nPqm2
4nqF5fEaKNkkxmyV4kDjtx6PDcqloMetrdPYsZob1DGLPetlpO7E8CL2rAf2
BbWK137s8yolXmukWrCySv2TddO10hi2L7Apmg/+rCXVRdEOhmW0BgkxK4rL
Zs99qW95eyQ53s7Ut1LfSn1LwjuMX+pbJD9T34rGJX2vkP2hdlKZ/HcY//S9
AseK5I5Q86L1RchnXEep7KfX9wrJ/iDfWDxXGc5r65VsfX09p0r+e90RHlt3
4/a94sbUs4LnsIwKkbcWzGGdFaSvQvR5ezx5mRX39GvM74psC/cz8LFSQXo4
PhXCu17Rc7YOakz9GWON1RNfE1cd2fsV9W7WrCFZa072uhvH5901p4vqw3JW
14HWkU3reszqqYS4kPhV1on/6xxrEquKw9Fi5GRX4rFYMziuEb9szUI4rVEM
+RzF3ttkYxXGlecOHVsPcFlnuWpzphLwrwW+hnnq10m8FWJjFDvk9yrKF2Vr
xftK92Ql0EX3JtNZobm4ynNzfd3Pofxcr4T+hTr4vse1JoyJj6mfWw9km98m
1/HeWUH70ObPqskznFe0rltMWc6z+udyo4L4KhRruj/DWqvrKscH5WOF78Ww
vq6T/W5rwBqypYIwxPoqDvd1xCfnsucL4mdxqWAseO55/WR9xfvp9geSsW5r
UgXVImR/2Ae8T3wv2v0R7k+af+vCWFj/cD+qOPvDmkn3ttTfwt5G6yiPFY6F
3D+5bl4bpHoY1nm7jvTtShyX8PwQ9k9bP3m+4bG1YI7nk5SvtC6E9T/MZx83
X2NCv0Ks1tdxLGyc+NoifMI8prFdR3264n7T2kHzD9cTnn/p3Hfu5z5eR2Ix
lfY53+O8d9A4ytiEe5710oqv675OIjmXyLkvGhOCeYXZEeHjOfsMPvfJfUuq
7TxOvOYVn/toT2O5GfRdmzuhfxfy3If7Eo+zVK9iZ6noHtzEuY/yVohuX1c5
PigfS577iI8VXAdQzbjEzn2pb8m2SPs99a0Ql9S3whimvoXrUupbfu+EfqS+
5etM+l4R9t1YP5B6QpCL6XuFsHelfKV1Iaz/YT77uPkaE/oVYrW+jmNh48TX
FuET5jGN7Trq0xX3Wz7vSbka5vea3UdE3npWOV7J6Tih43ascjx8VhAv/23e
j6tnJeThvBXQFY7BWpBxPMcn0Kl4C2Rz/TGqFMxXzHwRj5F/nGHj95rPp4r9
jQnt1UqF5p/nQU8rl6xn/BW2FuV1pbIu8K1HY3s8hh/hgecJeR7FEuMlxq1C
11H/cd1e8/tnjZ6JOB6AeWH8Y/ZK+Ub4KjTmxB9h31RQjhDc8O9KdD7AjdtI
9hy3tyLYGMbmeLCuxH5xcVpXczZm9Ldgl1RHYL9HYhOMV4S5wLdQXrinUE9Y
w3VZyCeUh3JuC/Epk3tSDhbkXaVSKeApsc8K9txxFOPjZfaFzTspZqQ+V9uH
IYZSLhx3NTmSq7Ga4myga48H9S4iI5qzVbAQ9ii24fhxhBP7HbWB5TjBpyje
FTQf1O8qeSLFC9st+cp7p1QzeWyL+gjHuay9Yn5E9lCQwxFZONaVKmclglUF
EcKsStzL1U/kV7Q38binc190X23i3BfYFu19nu94UX8+zuTi9dVqUQXXGckX
ppfUhIt37qN8FWpvLPfEHCxB1fpbwJfOfdzn0B/Pc6mc+/h60mdj+chqUgw/
cV2Z3JNysCDvni3nvmqYpL5VFNPUtwr32PHUt1Lf0jJS3xLWlck9KQcL8u7Z
0rfS94oCv1COE3yK4l1B80H9rpInUrzS94pL/ntF1Zou7mt/pjl96lR2KqfT
iPi7Gzsdjms67flOnxZ5JJmnC2WGfKJdp+NzgY4CXTH/TmHbT0f4sc+n/fjJ
UyezUycNnTKE3k+eLJg/ecq9x7A6hXRJMk5iWUwXiV2JGFB8ThM/8VxxnnAs
5VxxvOCD8eckx6sKxXKP5AnyoShHivJO8uGU5D8ey/Mluseq7Zdqec5z1MYr
sAHlbiSPq8WyeL8Vx1aMd1HeFGBq/RTtRe8n2d47eZLvuRjJtgQx2gBWsViD
7FNV8SuBrYrraSpfyvEYriT+p5HMTeTDKW8H8a1wn5wO4ivhJa2PxqcoZnYN
XxvLJxGH0K+g3rDY0TyQ8S3V24rGeH+K8Qq+nWLxi8mJ90ih1hkcXH4hvSLu
RFe8lp0qxF3IE+ZfWYrVqWjOnBLqqmCfs+W0vC6699z6SG5BDIRzWTr3PTXn
vlM8N9l79AwSxF+2/5Qgs7Dvi3Iu3XMfxvBUtRhXs13Kw3Tue1ad+3DunYrx
i3RatCWd+8rSZs59qW+lvpX6lpiHqW+lvpX6VjSP0veKgnwScUjfK8Iemb5X
XOjvFdE6ejrso6dOYRw8TUzPJEqU6FlIk5eADYkSJUqUKFGiRImeekrnvkSJ
EiVK9HSi1LcSJUqU6JlPG631W3bXXhi6ojb7fUV1m6c9nraKVL9JMnKRrufW
NKnnZn0tot8voC3oGaULFDMcL643jCX3MRbH4jwIcRLmCuwoWi/bUeBL1Fe+
Pp6zYcyr5XkJnoJcf+5eoAZCW/cyPsKD9gCRXyfsi03k6gXM10SJEiV6ttNz
tu9+yuhi+5YoUaJEiZ559FT2rUSJEm2eLnZtSJQoUaJEiST6/wFtJUc4
    "], {{0, 0}, {1714, 38}}, {0, 255},
    ColorFunction->RGBColor],
   ImageSize->{1714, 38},
   PlotRange->{{0, 1714}, {0, 38}}],
  Alignment->Left,
  BaseStyle->{"Hyperlink", "DemonstrationHeader"},
  ButtonData->{
    URL["http://demonstrations.wolfram.com"], None},
  ButtonNote->"http://demonstrations.wolfram.com"]], "DemonstrationHeader"],

Cell["Frequency-Modulated Continuous-Wave (FMCW) Radar", "DemoTitle"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`BMHz$$ = 4, $CellContext`GdB$$ = 
    45, $CellContext`L1$$ = 30, $CellContext`L2$$ = 30, $CellContext`NF$$ = 
    9, $CellContext`Pt$$ = 8, $CellContext`r$$ = 2000, $CellContext`T1ns$$ = 
    0.1, $CellContext`T2ns$$ = 0.3, $CellContext`\[Sigma]dB$$ = 0, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`r$$], 2000, "r (m)"}, {2000, 500, 1000, 1500, 2500, 
      3000, 3500, 4000, 4500, 5000}}, {{
       Hold[$CellContext`\[Sigma]dB$$], 0, "\[Sigma] (dB)"}, {
      0, -20, -15, -10, -5, 5, 10}}, {{
       Hold[$CellContext`Pt$$], 8, 
       "\!\(\*SubscriptBox[\(P\), \(t\)]\) (W)"}, {8, 1, 2, 3, 4, 5, 10}}, {{
       Hold[$CellContext`GdB$$], 45, "G (dB)"}, {
      45, 30, 35, 37.5, 42.5, 40, 47.5, 50}}, {{
       Hold[$CellContext`BMHz$$], 4, "B (MHz)"}, {4, 1, 2, 3, 5, 6, 8, 10, 16,
       24, 32}}, {{
       Hold[$CellContext`NF$$], 9, "NF (dB)"}, {9, 6, 12, 15, 18, 21, 24}}, {{
       Hold[$CellContext`L1$$], 30, 
       "\!\(\*SubscriptBox[\(L\), \(1\)]\) (dB)"}, {30, 15, 20, 25, 30, 
      35}}, {{
       Hold[$CellContext`T1ns$$], 0.1, 
       "\!\(\*SubscriptBox[\(T\), \(1\)]\) (ns)"}, {
      0.1, 0.2, 0.3, 0.4, 0.5, 1}}, {{
       Hold[$CellContext`L2$$], 30, 
       "\!\(\*SubscriptBox[\(L\), \(2\)]\) (dB)"}, {30, 15, 20, 25, 30, 
      35}}, {{
       Hold[$CellContext`T2ns$$], 0.3, 
       "\!\(\*SubscriptBox[\(T\), \(2\)]\) (ns)"}, {
      0.3, 0.1, 0.2, 0.4, 0.5, 1}}}, Typeset`size$$ = {
    458., {174.84375, 180.15625}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`r$5762$$ = 
    0, $CellContext`\[Sigma]dB$5763$$ = 0, $CellContext`Pt$5764$$ = 
    0, $CellContext`GdB$5765$$ = 0, $CellContext`BMHz$5766$$ = 
    0, $CellContext`NF$5767$$ = 0, $CellContext`L1$5768$$ = 
    0, $CellContext`T1ns$5769$$ = 0, $CellContext`L2$5770$$ = 
    0, $CellContext`T2ns$5771$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`BMHz$$ = 4, $CellContext`GdB$$ = 
        45, $CellContext`L1$$ = 30, $CellContext`L2$$ = 30, $CellContext`NF$$ = 
        9, $CellContext`Pt$$ = 8, $CellContext`r$$ = 
        2000, $CellContext`T1ns$$ = 0.1, $CellContext`T2ns$$ = 
        0.3, $CellContext`\[Sigma]dB$$ = 0}, "ControllerVariables" :> {
        Hold[$CellContext`r$$, $CellContext`r$5762$$, 0], 
        Hold[$CellContext`\[Sigma]dB$$, $CellContext`\[Sigma]dB$5763$$, 0], 
        Hold[$CellContext`Pt$$, $CellContext`Pt$5764$$, 0], 
        Hold[$CellContext`GdB$$, $CellContext`GdB$5765$$, 0], 
        Hold[$CellContext`BMHz$$, $CellContext`BMHz$5766$$, 0], 
        Hold[$CellContext`NF$$, $CellContext`NF$5767$$, 0], 
        Hold[$CellContext`L1$$, $CellContext`L1$5768$$, 0], 
        Hold[$CellContext`T1ns$$, $CellContext`T1ns$5769$$, 0], 
        Hold[$CellContext`L2$$, $CellContext`L2$5770$$, 0], 
        Hold[$CellContext`T2ns$$, $CellContext`T2ns$5771$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, 
      "Body" :> $CellContext`DemonstrationPlots[$CellContext`RadarDiagram, \
$CellContext`r$$, 10^($CellContext`\[Sigma]dB$$/10), $CellContext`Pt$$, 
        10^($CellContext`GdB$$/
         10), $CellContext`L1$$, $CellContext`T1ns$$, $CellContext`L2$$, \
$CellContext`T2ns$$, $CellContext`NF$$, $CellContext`BMHz$$ 10^6], 
      "Specifications" :> {{{$CellContext`r$$, 2000, "r (m)"}, {2000, 500, 
         1000, 1500, 2500, 3000, 3500, 4000, 4500, 
         5000}}, {{$CellContext`\[Sigma]dB$$, 0, "\[Sigma] (dB)"}, {
         0, -20, -15, -10, -5, 5, 
         10}}, {{$CellContext`Pt$$, 8, 
          "\!\(\*SubscriptBox[\(P\), \(t\)]\) (W)"}, {8, 1, 2, 3, 4, 5, 
         10}}, {{$CellContext`GdB$$, 45, "G (dB)"}, {
         45, 30, 35, 37.5, 42.5, 40, 47.5, 50}}, {{$CellContext`BMHz$$, 4, 
          "B (MHz)"}, {4, 1, 2, 3, 5, 6, 8, 10, 16, 24, 
         32}}, {{$CellContext`NF$$, 9, "NF (dB)"}, {9, 6, 12, 15, 18, 21, 
         24}}, {{$CellContext`L1$$, 30, 
          "\!\(\*SubscriptBox[\(L\), \(1\)]\) (dB)"}, {30, 15, 20, 25, 30, 
         35}}, {{$CellContext`T1ns$$, 0.1, 
          "\!\(\*SubscriptBox[\(T\), \(1\)]\) (ns)"}, {
         0.1, 0.2, 0.3, 0.4, 0.5, 1}}, {{$CellContext`L2$$, 30, 
          "\!\(\*SubscriptBox[\(L\), \(2\)]\) (dB)"}, {30, 15, 20, 25, 30, 
         35}}, {{$CellContext`T2ns$$, 0.3, 
          "\!\(\*SubscriptBox[\(T\), \(2\)]\) (ns)"}, {
         0.3, 0.1, 0.2, 0.4, 0.5, 1}}}, "Options" :> {}, 
      "DefaultOptions" :> {ControllerLinking -> True}],
     ImageSizeCache->{599., {204., 209.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>(({$CellContext`DemonstrationPlots[
          Pattern[$CellContext`RadarDiagram, 
           Blank[]], 
          Pattern[$CellContext`r0, 
           Blank[]], 
          Pattern[$CellContext`\[Sigma], 
           Blank[]], 
          Pattern[$CellContext`Pt, 
           Blank[]], 
          Pattern[$CellContext`G, 
           Blank[]], 
          Pattern[$CellContext`L1Loss, 
           Blank[]], 
          Pattern[$CellContext`tr1ns, 
           Blank[]], 
          Pattern[$CellContext`L2Loss, 
           Blank[]], 
          Pattern[$CellContext`tr2ns, 
           Blank[]], 
          Pattern[$CellContext`NoiseFigure, 
           Blank[]], 
          Pattern[$CellContext`B, 
           Blank[]]] := 
        Module[{$CellContext`gleft, $CellContext`gbottom, \
$CellContext`gright}, $CellContext`gleft = 
           Show[$CellContext`RadarDiagram, 
             ImageSize -> {300, 
              150}]; $CellContext`gbottom = \
$CellContext`DesignComputationPlot[$CellContext`r0, $CellContext`\[Sigma], \
$CellContext`Pt, $CellContext`G, $CellContext`L1Loss, $CellContext`tr1ns/
             10^9, $CellContext`L2Loss, $CellContext`tr2ns/
             10^9, $CellContext`NoiseFigure, $CellContext`B]; \
$CellContext`gright = $CellContext`SweepPlot[$CellContext`r0, $CellContext`B]; 
          Grid[{{$CellContext`gleft, $CellContext`gright}, \
{$CellContext`gbottom, SpanFromAbove}}, 
            Alignment -> {Center, Center}]], $CellContext`RadarDiagram = 
        Graphics[{{
            GrayLevel[0.5], 
            EdgeForm[
             Thickness[Large]], 
            Disk[{0, 1}, 0.2]}, 
           Line[CompressedData["
1:eJw1VQs0Veke984kt0wPXCkaTUJlomg8fq6G6aEY1cqQm4rRjEtnoqYyV5Qy
EXmVysjxqNDgHOd4P75jKqmUV6N0i0rjynntjXsNueYe39p9a+21117f2v/v
//+9PvP9h3xDNNTU1M6qnpm3AV3NxFuq7u2bqsC3W+72JrRLSGbkuxcu/Qo0
nz6eP50sIX9t7jadslfCuHFV2BEvCRFn+5lsTVXi+/++tpPNlpA3wkrnVqUS
GXttJu6mExJt4fbbpCWDjV2ZiaK5hKywSbDk+zMY/eJ/Jnnnm4ha3i17zfMM
CqpDSlM+aiL996LXezcw2GH12PXHs41EeUBgXT7MQDNnfce3mo1kcNncxK8W
sRDPzd23O7aB3GrYOBXmyiL41CxVyXqiN202H/tZLPjPofi1J+rJ77s6e43j
WNwJfbbQbLyOdN4ucgq8yiLqudtN/ag60rryvtXJUhYW24od3zO1JHVbzg/6
dSyeEIMHQ+G1pEu6Y4uAsFAVC+gZriESw3FbmyYW9tffyG6H1hCvxT9UHahk
8dZwa4zwbTVp1Zk65nedxcy0ufuqSXiAdo11CguPmXH7qojrHvddS3gsaHsB
VcSvO8MjdSsLWu5ZJTEs8jz8pxmLXXRVkkwrN30dloF2a+Oge5eY3Nb6Z6Cf
Cp+qzz89ZustJnXaL/V+Pc3gGxV6S9pEZOiYaHLZJgaLzMav6m0WkcH6IXHY
bAYt6TOEVBDDv9vPsstS4igtWEEk/nOOBhsrMVPtCRGSqdM++SdzFOgZvvyq
2VlI3g/7F2QtV+BsoPrh8loBMX5ekFApkmOGjZz1AuIyGr+6fbMcbWcKVy4M
LSeavHj/pncyrFapgwkqI2Oalp86XpQhxcfz0EP/UjLmqaF1d5sMinWG6Td2
/kLma1/h1S6QYbvJkDhu+y2ivl5xfI1UijK12qd7NpWQ46EyZ/UuKf4yeG7S
wb2YlEXcj7pzX4qIh/6m852LyNUpod091X6HwNpNse4mydV80qMvl8L20tT+
+2tukABrF3dfIxlSox+pOrxO8k8YhWntlIEJulZ08pNCcmAdsfXKlYG2Z1pA
ChYPx8yZkIGWM8wnbS0Opf/eK8c86o884pjtM9ujSw4e1TufaPC2R+/ergCf
6vcaKRvv+Hh5l4LTYzbZfTAs5kKAktPXFeI+b9QicEjJ6SWLLEja+425E4NC
yv9FkiUwaj8axyCY8plBHMZOlOi0MLCg/KSRtRHFjuWzWLRSvC+QzKbdpv4e
LHwofklEJzptrXoMy+GRQEoVFYtEApab7zTxK+QFZb1guX5jiEn4vpARtRHw
6flHyKp/RMZZmo7AjdYLI+ZPptsltiOIpfu7iHtU4+ZQpxGoDcx8eyD3lMRF
4TwCCd0PAUmKXeK9bgRB9LxI4Mcvc0ssVN+0XjTadMPMN+mNwJbOEwd+kU5U
5jsWAvr/GQg/mydbKmFhRvE6hzfRwpfZFz70mwLnoMpTT/1Yjo80OMtj9zQs
/jBfBrT6jizb8oLh+L4IeUupd/plhsMjC13G4mCRL4N5VE9X8JGu42i3HsPh
l43s47YLnRKVSKUFczAq2bLh0J8KdFC8c/FWovM2/rgCP1E/8HFH/HVi4aQc
lnSePJzZk2n/W7wcrdRv+ejcdqNz1FiOg5TPAiy6W7LzVr0MutTPhRhYepaE
h8k4/q8jOemxY4SVDDQe+26gOma5588TUk4vNxHze4Erv1fK5VERvjCw0Ox9
IOX0VYyvT6zQCngk5fKuBMYa7CXmlZTT4y2ki7wummjLoEXztBQOZXEuRhtU
51N8y9AjWO3+Olp1Ps3rcux7zRuzb5PhMNW7AM9WtoZnW8rxK70PBAhKfng1
/IIcH1M+hDBUm9Z8OS3H/hl7ZQrx3SKNNVNRClRQP1Ugwm0h755SAY0Ze1+t
wJch1hqrw5XwpfyJ8P6Rq5OJVAl+t87n4XwRFN0OvcSO4fwqhl3hGsNVPAZu
NEDE8M0eMH5V/IHvSoxv9po12s/glW7IWNUvlbAyiDLOMGC5PKjC5E+uUk3V
fRH7SWbnvyqqMFv8x4784A9+qUbJZxEtg/Eq/bneVkVQNdLUhVah11jwaN7U
YKDP1+aZyk8Sv5Gk5aQGB+9FTug3sJyeanGqQ/T9lOq+CIo0V12htehialuq
VPsCmmd1cHVIS4+oYKFGA7EOjaPJ3x3OY7n8q0eKv3DcOpEFha+zHk2KtFzT
cJbLywYExfEf9G9hQdt72oBBq1U9phYsl6+N4Nm0N6dPMOh7ObMaIUhuLtJu
Y7g8boLP9X1Gnj8ziPljTr3G2yZUbdy4yzKMwWOqbwLLADd+3AYGS+Y7X14x
TKB7+9x5C10GDjTvJWj8aqmirlyJ/kuVqhtIgj3PXQP7fJVIoH6QYEAiqFz6
XoE1VJASBFn+LbCtWIH/A+StStE=
            "]], 
           Line[{{0.2, 1}, {2, 1}}], {
            GrayLevel[0.5], 
            EdgeForm[
             Thickness[Large]], 
            Disk[{2, 0}, 0.2]}, {
            Arrowheads[Small], 
            Arrow[CompressedData["
1:eJxdlnlcjdkfx89DixJFIRmytIgQNU1N6nsmlcRkTyhLkbXBKMQgugkhW4tc
7SlUbtoXnXtlS8RVlMw0mmGEmKIQnueZ2+/X+efcf+7rvJ7Xufc83/NZ3qP8
Ns1b3QshpMsh1P1d2TnWJ+oaD6jn0+HYWpSuWo//O2e/jtoqSCTbq1q618ed
Slx6B0Fj3PFI70oe+vs8yf7Qaw+MvLG3s0O1PpVif80USWBG9cCvV6/zkN4a
sdaZPwj9c6VH8m7woPHy65SNn49Cu/WnmEc3eXg480FD4o4TEDFd0tvkNg9m
y08rsz6cgi8Nk4oTqnjoM9Jrz/jAGEip3xLrWs3D0ouyHa+fx8GJ/XNCDe/x
YPTL2N3FS85Cwxc/zyH3eRiY3GvTqppz4G9U9+QnJQ8HH+nkNBknwaio1Z9i
a3n4arwIz1iUDJ4/bt0x9DEPJDA+ffHRFJDUfhx3s4GH4Au6sk+KVPC7H/nk
3FMe/A2Vnn0702Ddoa0zk5p4aLpVGvWb2Xmokp1xudfMQ9pp92Rrrwwo+RQX
bvKCB/78ljpDSSZoq8ur01t4CHXal2AguwARGYOVc1p5OJ7ZGq/beBHkHv6z
TNt40HM0O/qey4K6LKv20R2q80wN9Dtvng2VdbWbZnzmwSIw3tpgZg7Uuv8a
HPeNB3PPV26mGy/DSUmfLF1OgOK/mg6udJVBS/bm0hx1Abz7W5/bUCuDawfX
uQRrC3AstvWu6YpcKMo899pPV4A3c4tHhL/JBetnFz22GwhgfWh2/p7gKxDS
aiHmDhUg/9Ocak3hCrzzLUwcaCxAWtSQikHheWBY/lEeZyJAp0aUWoJ2Pkif
p3VMGydAg4HG9zHH8iElLaZK30qAsjmCWodeAfwbebNa01aA1SldM7JPFEDv
Lh+J8VQBZli7finSK4TDviUhS5wFMFMqJmgfKwS/0l2Tit0F8LlyYUS8VhHs
m3fMzG62AGePzB+3LKwI3p6a9MeThQJEvnuvnPW1CPrbfquR+giQdG9hmc+W
YpAc6QoK9RfAyj4xV/KiGMr7JblK1guATcT0skUlEKjzT37GFgGu/7BG9vlW
CZileti83iHAyMSpeta2pTBE+WzKzFABxik0JvillILtrSXD70So5lVvFLtb
pwwaVw1aFhAlQONilz67g8rg3V9jfI1jBVhmvn6Vd2MZVGctDe1IEKB6k1Wp
plM5NK5eMf7FeQH6/hQ7LSyxHDqMZKQtR7W/IDqgUiiHKwmXpYOLBPixMLdA
vvQqnNAtMvYiAux0Ot22tfAqaESMSMu5JcCIUfFqj/tXQHibR5jxAwGC7Vb2
e7aqAgI1ywwyGwS4/23uwsjiCmjeaZAzvVmA6CBTL6UWgQjbyan8KwFebl1T
csmbgNF9S7eq9wKEjOr0NkgnUDfg15cXvgrwrHA7qP9LIKrg9qMENRECSgIX
TB0mh1hL/2UZ/UR4uXjlvnxHORjFtN69NlgE3zM7t7Usk4NhTcyGNmMR0MUX
0kd75FAePzZskoUIdyo9toeck4POx75L904RQVYvJitL5XD7Vx/7Px1EsHO2
q336WA5Lyzr9PF1FKAmZ8jS6XQ4Ki/3D7nqKUJTvn8ZrK8Bx+6H0Rd4ifJAk
Ww8Yo4D8/Xud21aK0Ow/I7PGXgEnLWunRG8Q4fiEgKk2sxWApvMFrsEidB6o
dcH+ClhwJkqb2yvCK8vlk9uDFfBb57Rfbh4UwdGi5YhrhALcl/UdEX1ShG1v
dvEOsQpY5Djca6NUhHfOW1xq0xUQmqExb+Z5Eazt9jlp5ykgLKdk1mSZCFWV
f/z9Z4UClBXS0OGlqv0r3NznVylgSp94c93rIhzdbnTW/6ECzNsm7lKvEaEw
fZ25XqMCyo7syhfrRVCr5xZ4P1NAmtk8Hb5ZhIn2u1IdXihgs8Kt7NsbETR6
O+eXtCjgWGZFF98pwkq9BPzwtYLmMv6/fxVQcmlkb04dYfrcPdEnhdNGmO7P
aX0cwukiTH8/tTuWDRCm//+2O5YNEabn+18sD0eYnj8uKRfaRyNM369ibOri
l+YI0/fHWd+8Gi0RpvNZuLK9rmoywnR+v8/5srnAVrW/Z77O6k23pQ4I0/l7
WRfv3IsRpvcT6sOZ+roiTO+vaF1MwPceCNP7Pe8kb9KcjTC9fz8Xhwl18xGm
+jB+4/T+jDfCVD/zR4fXePsiTPUlvTnXX88PYaq/L2o758kDEKb6TLmywWnt
BoSpfjvibO5obkaY6vtD/qEzCUEIU/0XrKuZYxmCMPWHffqwaNluhKl/Bn9y
0xm/H2Hqr61xr3zOHkCY+q981ZqJXKRqf48/o651FzDC1L8ev6V0XjqFMPV3
RcQSp7ZYhKn/d5e3ZY+VIkzzwW6HSduCJIRpfmRYZeYGpSFM8+XAe/OkiEyE
af7o35u9/mgWwjSf4i0eknAZwjS/DvX/Ze7mfIRpvp36ufPBrGLV/p78Wy2d
125UrtJjTz4GxUYFNBKEaX4G5jS+OFyJMM3XO11HTC1vIUzzd3TXpD+u3kGY
5nOFfdgNqEGY5nfX831xuUqEab5r2a4doP8IYZr/5HFxq38DwrQf7o92bkl9
ijDtD4e5YyofNiFM+2VPmNKxrRlh2j99Sr8z+vYcYdpPl7Nthn18iTDtrytH
s9WbXiNM+60jWZKY91alz57+01KayLa1IUz78brWBmT+AWHan/9EXVxyvVPl
j55+TR+fe9jzM/VvLiQ6VPve+IIw7WedC7dSxvII0/4uuvrVYoeIMO33mKdu
N/I4DtP+Txg02P1pbw5TPoi5sCbinTqHKT8kLalZ/FaTw5QvHLOCYuu1OEz5
YxdvbJTdl8OUT0wk1lcD+3GY8ovUPM93qC6HKd/MdHBrvKzHYco/sw1/1J88
kMOUj5xiJK+l+hym/PTE7+a0Dwaq8/Tw1Sa7n9VsBnOY8tf994P0lw/hMOWz
0/trvIMMOUz5raiinmwdymHKd5MMptv4GnGY8l+Yfkuq1TAOUz50/N1ReKta
U37UDJDYRH/HYcqXIRm59mbDOUz5c2n8tl7JqjXl083Dcbj6CA5Tfj2V90Pp
fNWa8q3VSJezkao15V+NZi2T7O51Dx8bh3ffLIcpPzPPCbOfML9PmP8nzPkI
c37CvB9h3p8w8yHM/AgzX8LMnzD3Q5j7I8z9Eub+CaMPwuiHMPoijP4Io0/C
6Jcw+iaM/gnjD8L4hzD+Ioz/CONPwviXMP4mjP8Jkw+EyQ/C5Ath8ocw+USY
/CJMvhEm/wiTj4TJT8LkK2HylzD5TJj8Jky+Eyb/CdMPhOkPwvQLYfqHMP1E
mP4iTL8Rpv8I04+E6U/C9Cth+pcw/UyY/iZMvxOm/wnDB4ThB8LwBWH4gzB8
Qhh+IQzfEIZ/CMNHhOEnwvAVYfiLMHxGGH4jDN8Rhv8Iw4eE4UfC8CVh+JMw
fEoYfiUM3xKGfwnDx4ThZ/IfDPMDdg==
             "]]}, 
           Arrow[{{2, 1}, {2, 0.2}}], 
           Line[{{2, -0.2}, {2, -1}}], {
            GrayLevel[0.5], 
            EdgeForm[
             Thickness[Large]], 
            Disk[{0, -1}, 0.2]}, 
           Style[
            Inset["\[Times]", {0, -1}], FontSize -> 30], 
           Arrow[{{2, -1}, {0.2, -1}}], {
            Arrowheads[Small], 
            Arrow[{{2.2, 0}, {3., 0}}]}, 
           Line[CompressedData["
1:eJwVVXk01fkbVr6f7w3XUkrF/DIyKUrXqITweUtR2SdSiJR0ayxlimqyZ83S
4CdZQhtKRbJMmj4ylpv14l77cqVkaXKzTpJ+9/fHe97znPOc9zzvOc/zvion
vH85tVRMTCxYVP/vZfl1ieevMeDSzsfa580H8eBo0fdt5xkwcWPIIUZtEA/U
ywSqODPgwRxStfkuwOsMUuJ2mTEghwWple0CfC8iMSdUlwEXy7aNDeULcMNU
Pv66gQGBD8sOlwUL8LPUXs5deQYYRaUbGtsJcMQfBwoDljBAWdZFIXKjACs6
ap6MnaBhTNg1tPLzAK7PLd3b0kdD8f3C6bQbA3itx2SpWQMNqx6rvs/XHMCL
+g8UF1/Q0HH7Zw8rTj8GCVwzmkeD4U7389eP9+Nm3UJZuVs0wPWdzDOzfdhe
3MXUJ5IG7aBTocKoPiyJF8uWX6LBNtHWXEOpD5OggEcTp2mwX3STVH7Yi+0y
Jo5LH6FBsM8krkOnF5+LYSt47KchApvynV73YM8nn8Sl9WhQyrizrOxAD472
O+b0WZ2GBUnO1cnmbrwhxNJCUYmGlcLsglW23XhMYun3SCYNGX0d5pvbu3BL
i2mS0SKC7r+kVQ0Pd+F7vmwNAyGCEK6yph2vE0+yZLtC3iIwueYY42/diSWW
bH8mz0eADIsjK+o6cIUjXTlRi8BK4Wv8JuMOXKzTKbOmHIHLM2pHxZ/tOCdF
4lbsEwS5GwRbE7a2Y+/UaQebOwhCZRfs87L52HSwa79bMoJMoeVjOXk+5iQ0
WtdGI1jD/d2iLYSHt1Xdc/g9EIEjTPstCNuwk/uQafAFBGv5q2NuHmvDFU8f
TnWdQfBGyWZZAacVS9G52uEuCOI2aUXZaLdiHzbVFm6HQPtNCCcitQWrONmm
95oh+O3c2XTnJS1YxinSKnIPgtZx79lBdy7+pbHvQbSeSM9H9epdyc3Y7DLX
7p0WAveLtUxDpyacGMeUSN6EwKDgXyxQbsRf1V3ds35EcGcOJI0G63Egu4hJ
rUXgZHJlr3dWHTYRn0x8vRyBm/b7yZhjb/BtX4nYLkkEwWE7jAvWcPDw885g
cwqBbWpa9GRLDdZ3V5tRXKRA/X6dsWdUNW4qTj6x718K5mUXmPq4Chc8zjZq
mKQALxfe95mqxM9Vomef/kOB3/3xf7QevMbWGnZC4QgFl+WfD8bZV+BvrMro
+HcUVJvKbbOQfYWrIoz/EyOg4NvprXE/eZbj+Oy5ueFeCkpvfXTv5JThKs6d
k1ldFGQ0qV3JWF+CM/3yssvaKbghZfko90oR5hhKfWTxKFBL3S/GainAQfrO
F+lWCiJWZurXm+djjkY924BLQWF+YrD4zAPcKCU51tJEgfT8wYDzadm418Zl
VW0jBfIx5fPfg1Kw/PNb35VEuDhEJ+10wnUsFitXwm2g4MzVcYe7IWx8iHvH
6L0Il10ICP7xbgDJ+m/JzaMi/t1M3Woz1wTiqqtXpimaDwUlAcfXZBCmdFa8
azMF/e2B2inDd4nNo1E0I9JjvqOy3wfnEcvGzK8fRHq18/hhdslPSM7fXQf0
+RTExB3jnJ0pJAYBuHK8g4KPocvM995+TuzUpVkL3RT01LB0QvaVks/Oqw/9
2k9Bynqt+ImxP8m78eZR3bcU6PhnhdrHvSTum8xy3IYpUHI/xw5jEVIwtE/3
0xgFkSTYZf2HCrLu7eY9/AkKZkvupWvtqCRe/6w7uXaGgvClo85twX8TX9/F
n1/MU/Aw91OxsKGKNNV6G5WKIdC7XdTspVBDijjDcnIMBA56N+e3OdeSBKun
GlXSCFatc8xRu8chbuJzqG2lyG9XVX7VHHlDNtR3rDL4AYH6JfYSlkY9Sao7
UbOoKspT4lLLFWcbyMnxR1cVtyCQnpJyeZXTSN595ifHbRfl90ivl+JQE7H7
QdnDwRCBRtv8JC3DJS/NPpn7myCQOe23FfK4xLlYEDJrhaDGX7N5i3ELKVDq
tK4+iqDd8zCvokeEYyWmRk4i6B+XPCL0aSX+j5iZx70QFMVr3Xi6rI3Me42H
br6MwGfrqebJ9DZy0E+7bv81BDD3IewJi0da+UuSSuMRpEDQno4KHvF3XPfN
Nw3BtynXIldrPhlyE1cKz0HgFy6bs6efT+ZUZxaGihBIeehJe59tJ+SUfH58
BYK306lzPdPt5Fmrx+bIRgQtIbzUAP8Oop274lJDN4LDtLGVOdVJkka3RDmO
iO7LM94f+lGdRIsha719FkEAr5vCzC7S/MW8zI6iRX7oPGcV20X6udycVyto
2PjWqtqV2U3yCoWLnio0WJyJXOsR1U3m9oWlnNCiwf2o/WM3qofop4gdTMM0
BMoNju727yEpjmFDClY05JYfOTE/1UOGMxZZPGcarvq/5F1j95KBwZ0zLV40
HDSK0+js7iXRmTNTMoE0PFkddXn8YB9Zri4zEhVPg4HFIaqkrI98+RwSuTtL
9B+qtuxi/dRPuDqpEdsLaeB9uvXSMrafWIp/SXCtpEFttdRh5nQ/eZ/016Ha
NhpSGUHU8SMD5OHqVC+39zQc/bJxZveLARJePhG6c060z+DiHj5TQG4qj7Gw
BANMlLRUD+wSkPjlaZJXlBiQbtf29RJbQOZuK+QOazLgS0OniWuSgKj4xqeE
AgPYOeU3F18JSGTSb2yrQwwwsKxJsxgRkAsQlbXfnQHZVbaKZnKDJMaMWeN5
mQFDMytnpnQGyf8AWA7WDQ==
            "]], 
           Line[CompressedData["
1:eJwVlHs41Nkfx20ubTvM92RD2trIbpFstS6/CeN8Sn4lkVvItZRcS7XLkHKb
lbbxaxVK6xI7SVorycavllOYzNCsuzKEuZiZeqoVq6SW3/f3x3k+z/t5zvM5
57zfr88xDY/3jlikoaERSa//V2aBY2/GJSa4jWontGo9x+ZTB0a21jHB5/zR
joqbaqyrJ7IoFjPh0xrz34s81FhdHll4Xs2E2Ja88OpJFTaY28000qRgxQrl
12O5Kvx6tDPZehUFNy/+zHHYqMK1Kn6P1I6CuYThv4WdSvzN6vUGJp4U2Ke8
CP3psBKvnz+2+2UUBUffZf18aWECr2+IP7s9g4JsV7dbyksTuKvbXL72CgWh
5w3bszdM4GJGd9qFWgq0qsKsMh8o8Jbcv85ltlNgU53vMeqtwB4Hc+xnn1FQ
d/t4wzW5HK9duevPyWkKujb4LRs9IcdXXz3a8bsWgkDB5U15GnKMcLd6M4Ug
Ttg78zhHhhU66qQjxghGHFmx+UYyfPpboWWkGQKLqqH6qTIpps64239pheBl
4ZT+G3MpfmuWLbtgh0BvcqO08NY49qsWRD7ECNasmCmYthnH/dNV+tU7ESBX
J67BiTFs7Wm92csLwRs+74tuzigWJIsNa/chEMfNJK1PeYbLNUJXCg8geNSv
u4d1agRzfL3KCqNpLdqRppcyjM1CK3VMjyPYafi9UTNHgoerFhr3JyGoZKzz
DD0xhK1MK9b6pyFIG7TgLYp9ionPlTbNMwjyimVe5MATrOtxVe9wDgIh955P
ld8gtvvHJfP0RQT/aUk/NOA6gL9cPXXDtRCB81DHyQCHfryoWaXXUYJg3MC7
ZodlH1bZ+TjP/4KgIMPDrtG4F3/a4SWdqESgH9gVSXR68J3691np1QiajqyJ
mlB04Td9BwbEtQhyZ/nuXf8V411dN5LF9QjCTrqIInM6Mavzw5q0RgQhgfHe
kmARrvilOFt2H4HNuTiet2U7pipVB981IzBI1HT6+K4NC5UBJ5seIiiRMgI0
W1uw1vvcPJs2BI3Tn6jzch7gLXv2pQY/QhARb2VdX/UHzmfcoGyFCFJ/O3VI
i9WITYP5a5pFCNpLk7443nYHJwZeK3zXgWC6qSHmemkNLojfsk/aiaDL3z14
cV0FVjmYe6c8RlD02b3DR7Ov4NWa8RwBrcOXNHds5adiZZFLK6H145COnsdx
PxAP+Sq7GFprKzkRPcuKyYs7/ME2ul+U1WZl1ux1Urn/j9td9HkWBYE/PlXc
InvvZEnO0fdpCdnK9o+sJyjeN+ZtO72fLVbclDSSHL+6dH36PcPndQ8ydzeR
1LtBzmOtCC4obLn5zx4Qz+8dR4NpPxZHJH/d83kr8SqP4/Jov4peF0zm7hSQ
Wxc3xMbSfmru5UoqU9rJ2dTxvo8NCA5B0NzzahGJLmUsAjoPdrOAZznSSXKf
7AJbOq+S2fApWPInSX7uMTbyK4K7qsLQBeNuom02aPsvOu+Z7Pf9K7/pIZLS
irLtNA8R3O2+cuglUYszM7VpXki+RTzbu4/Yy9dtP3mZnhdRS79BeD8xiXH2
5V9AkMRw8vY/NkDE8sh1GTwEt91+6H95apDU5+qwlmUhcHccjX6Y/YTUrorQ
CUml/Y2+5Pwg9ykJ5uctD+YgYBQXNvRfHiKPNofPLT1G8xjE3PaiWELCnu9l
p0UhOJsye+bl1WHyvrwk6dp+mqezAp/2shFCTHRcMwMQlM0pyg5ffUY0Rl95
Gnsi+HGpf97dolEyFMRixexAMLhpSyq/YIxcZ26tTnBCgH3XTeX8e5y4PxWH
OdrS8yxrcdR8OE5ijD5hEEsEWZmO+pb2UpLo5hY7b4rgiM3bhbnbUqKxJ9j5
gxHN+x6zmmhzGbGyWBZ0Vw/BlA53/EiJjJRxvkvbpInARLicM4/kBCkMtQcm
KdgZLjuqz5UTq6XDiR0SCrpf8WRl03KykW2r69xGASvclFEeriArBfIz8BsF
QnETfNatIDbnnOraCuj/1ZT1oc9hgpiYfH6w5zQFrzLEvTMVE8TFKZ0TFUGB
u4EoPZKpJMs53YO83RT4sSzGTRKUJIrle5xtTYG34bbXxhIlSTBygDRjCrZd
DOncxVaRj7FxLj4LTHBKDLj/a6mKfMsO491XMKF3xHGt7byKDHPffHVPxIQl
wqHCsSA1ud7mZOVVw4TTEn8Bv0FN/gfgiw3k
            "]], 
           Arrow[{{0, 0.8}, {0, -0.8}}], {
            GrayLevel[0.5], 
            EdgeForm[
             Thickness[Large]], 
            Polygon[{{-1., -0.7}, {-1.6, -1}, {-1., -1.3}}]}, 
           Arrow[{{-0.2, -1}, {-1., -1}}], 
           Line[{{-1.6, -1}, {-1.8, -1}}], {
            RGBColor[1, 0, 0], 
            Dashing[0.02], 
            Arrowheads[Small], 
            Arrow[CompressedData["
1:eJxdVFtIVFEUnYLKwkAikl6TfSQSKmjmR2WLDNLKLFOESiMMRYhIEsxUQiLL
FDRI6KGWNkNhgWhJOGKzVUSTxDQtR/MxNo4ISoRpzNucs8/5ccPlcjnn7rPW
2mudvWk3zqev1Wg0MSuP9z3wOeFW/rIHGaJm8MD/dGO0wYOOpaCU8g4rPp0y
NoVle6Atmkto1lpR/3zP24wQD/K21sdM5U0j8uVoimbWjWFdVtSmYQsqdmwJ
1OrcOCDKgtxx/Ji87Ea5aPgLvkd7qtN3uuHtVjQ3hQDdi8WvJhdWmnk7ouix
pxBPXNB72+nM6CrNcQ8lu7AsahI+ZVHaRn8XmgWgCVTNr8tZHHFCbM8ax+uI
9ItU7YQX/VLQGJqTF7rDrjphFu1HoT94+MS1/U48EzWCmsoPX0r+OiDgJJgQ
fqittt7owEYmBEOX5uRiqUPq8R1x+a7U3EsOyX8IBiTdSQ5xIFzwHcSrwurQ
ujUOyW8AQdE+7Q9Ndohuun4UxC4c8Xtvx0eBvw9Dx4tjEsvsEm8v1ndXbM68
bsdTga8HozcrQ9PO2uEj8HTjX3BqXWKEHbni/E4EZpt74nbZMSvOa0eJddv2
lA12qY8RZzyR8WVLNtnPgOWaiWO/Z2zy/yb8afaz3PtpQ4PYX4/7JbtDLwza
kCXW9Tj37u6+rD4brojvCvSKskEjqqZVrRfyOqn/+VtPqn8b9yd1Pq83UbzE
V8P4SOHn/UZS/OaZHyn+BcyflD6+rA8p/apYP1L6BrO+pPRvZf1JzYf91k9q
fnzeAKn5RvB8Sc2fzx+iFumPTvYHKf8wnmGqlf5KYn+R8h/jG6EW6c9p9ie9
kf5lvGO0yt+0yv+k8sHzMNOq/JDKVyzni1T+mN8U3Zb5fMT5JJVf5mshlW8T
55saZP6Z/zQZ5f0QwPcDFcv7g/Ww0jd5v2Ty/UL/AUVxWZM=
             "]]}, {
            RGBColor[0, 0, 1], 
            Arrowheads[Small], 
            Arrow[CompressedData["
1:eJxdlHtMU3cUx1FR23LbC/E1FGfRko1u8e1UfJyDRshE68D4SDE+itSJQStu
KLrJdHU+cFSJiQaTGjRqiptDCIKl5NAWbaGtgqJIhJbV1bgN1DE1RiO4Rfzn
3OSX+8cvv3t/5/v4xOq2pWUODAsLS/9v/f9+/0hQvcbkePGpF7YHPsnIS5ag
I/z1oeMqLxxO27KnvVCCf6T6E/SjvFATbAsfdk+COZ01/+4c4IVg29cNQ8ZK
UVdy/ogv6IHZVZ0XSzKkuLx83DFDjQdMcfXFPosUf7o8ISW7wANFKRPpm6dS
XKdfUnk7zQPXUy+k75suw/mT1LnlUR5Qvw096tglQxPaO5TuRshPPK741iZD
991p/um5jdBU3l47pU+GY8TicElMI2idSlUfRqDBK91+9VoDXKtbVePeF4Gu
3ibf2mUNIB4xanbbI/Dk56FF4zvc8H5eAUlVsFuV4YbFMy8tS5on4IjRCbsK
fnfBovzw2IQ8AW+Za0vzV7vg1MqxdneFgLnTips+c98ATc62rkCXgK2rqrtt
k2/AwcGmlmyVHIuTtPN0Rdfh5f7quA1aOWpt6ycs7a6H+phZgqVQjpd1D2Ou
JNaDZrll9gy7HJ/9ElvmNDmh7Knw14t/5BgX35xua3WAoqXi8QOlAtvzbh5o
iXbA84/enri/VIHx677YnLzS3q+PAgds7BqU9nMdWOZk7fCXKHD9o/ih+r0E
qVkVBp9bgYkHojauuF8L6mEjreZuRb+/NnB8+eBOUqSIC4zBX30GK0wtvPvE
OlnEzbVFW/Vl1RAK0N/PNCIGX5kDKY+vwr2ezLWBLBFzjP4FQnQlJBecOPa9
UcRuw9nhsxZWAI5oNjpPi+iQnfdv2HQFOs2tryxlIlpDpcPnCr/B4a3+TWqn
iAunjskcjJdA+YNZgDsitkXu/1Oz5SIMSVY+DHaKWDVX/+Ni0zn4OG5JaVSX
iNFWedVq7RnoiZ4fcj0Xsdn13aAVR09CL6xpH/hGxD19M6b0ZJtAa59krusV
MekrXWVbTn6/v5H4Idlsn9h5Yt8n9n9i9yN2f2LzEZufmD7E9COmLzH9iflD
zD9i/hLzn1g+iOWHWL6I5Y9YPonll1i+ieWfWD+I9YdYv4j1j1g/ifWXWL+J
9Z8YH4jxgxhfiPGHGJ+I8YsY34jxjxgfifGTGF+J8ZcYn+kdNsamnw==
             "]]}, {
            RGBColor[1, 0, 0], 
            Arrowheads[Small], 
            Arrow[CompressedData["
1:eJxFlHtQVHUUx3f3/jReKo3JJg/lqaJBq1DIGnoMoRIBQSZEsA0QMweRlIey
KYK6BCWIoiE4SAPmAwVR5CVwbHmICo57d1lgF3YvhTgJCULILNNQMk2/zsyd
+8d9/H6/cz7fj13U/uAYAY/HC39zzd4LLswWH0aCapecHFbg9PiCbFE1HwLy
F9TXjChwb0bLj00sHxK8Et4LHFPgs1ONT8SjfBjLsg3d/FqBy0NPb71oKoA9
d45HXJlRYGXFMm/NcgGYt6mLo41ZLL6a1j3sLYDSyZFHUgsWjYYPbWmXCMBs
fH3rsBOLgfWJr3ZLBbCIvxrLPVgse7zCGc8LoC7+rCv6sehmKY5trxTA2IsI
hUUUi7YDr/xSOgSwxK6oojyFxabnvQkdQwLwdyu3ST3HotD/88pqHgNnnHKa
Um+zOJd8MCKyZKCmNFN2RcGiu9tsMdD82d+NI+Ms1lYk1931Y2Ct9ADxsVBi
egdfcyuagZcG+bbL65To1WBqaSNlwF1YxAiilbjtgWTVRC4Dl9PsOgN+UOLT
wksvV11lIExW8eRojRJF/COJ9xsZaM+byDkxqMS7638j11kG0lsWzQ1eqMK6
DHPhr0Nv3p9nnd/jrcJP2op2Rk0z4KZKdjBOUmGxnbTZcR6BdyObeKprKjxX
8jjOYSmBQ61ZRs56FW6Kkcu3iwhwNzLypxZ1YXLlzkgEAsqTl/JdA7rw5el3
XIK3EpDtCjrWkNGFOvH+zWYSAs2TKyJy5F0Y8CLL7vdYAlGdocq8mS789NJ3
ksHDBMRBcS7169SYEzY0ZThJ4Hrymu0jh9XoECl3WpZLICvjzB/COjVWexjt
iykkkDtT6elsUKN/l6ywqpSA6b35G83E3bi8d/Sh+U0C/kcb2m5Iu3GvBCOS
qwi4qKuCXzd2I7iZzRmqJ/B9oudaFa8Hv7aGX3bcJyCa8b24elMPaubX9itb
CCz0tPjWkNGDkSZbJgPbCdy5e+2ORUcP8o41vep8RGBDawxJMO9FR8uFZv4d
BBY337L6K6QXOztmi8C/XP//3Co+Xeih66XfT+7rdEV7Df1/2pHMVOuvNHR9
u2nrGs8yDd3fG4zO80c1dP/XGg3TX6zR0vO9tfMo91Gilp7/eVVSrKxGS/vj
fMhxfJlBS/tX4p5mMBb30f5eLyv2sUrpo/3PusfT+tb10flMD7TMOTzVR+fH
xmglRe79dL7feJl0l8T30/mXPxm4nVTWT/mIvHm2gDzrp/yEyMoK1tnoKF8N
+66uNAnRUf6s2YQ1kkwd5ZMvubdgZaOO8qt1EhbuGNVRvm/H2bgMLNVT/mOL
NpSUB+hpPrQ3ju+qkuppfnIq3949+LOe5otnfe791U/1NH++nvH6U1N6ms+Z
8C+FyfM5mt/aPy+IHWw5mu8ND0LUrSKO5l8xN7g+DDjqh4lUuYkmgKP+MM18
tPHjCI76xd5xWJS9h6P+WT8QKKg/yFE/mS/mi+VHOOqvPNvp6hIZR/3WvsSh
Kjybo/4786Hrw+d5HPXjEIYO+xRw1J/2IcZxCUUc9SuXImEO/MRR/x7szG3w
KuHwPz+Xxs8OmMN/ACWkxWo=
             "]]}}, Epilog -> {{
             RGBColor[1, 0, 0], 
             Inset["\!\(\*SubscriptBox[\(L\), \(1\)]\)", {1.4, 0}]}, {
             RGBColor[1, 0, 0], 
             Inset["\!\(\*SubscriptBox[\(L\), \(2\)]\)", {2.5, -0.75}]}, {
             RGBColor[0, 0, 1], 
             Inset["\!\(\*SubscriptBox[\(L\), \(3\)]\)", {3.5, -0.4}]}, {
             GrayLevel[0], 
             Inset["amplifier", {-1.2, -0.45}]}, {
             GrayLevel[0], 
             Inset["generator", {0.5, 1.4}]}, {
             GrayLevel[0], 
             Inset["mixer", {0.5, -0.7}]}, {
             GrayLevel[0], 
             Inset["target", {3.75, 1}]}, {
             GrayLevel[0], 
             Inset["antenna", {3, 0.7}]}, {
             GrayLevel[0], 
             Inset["circ", {2.3, 0.3}]}, {
             GrayLevel[0], 
             Inset["antenna", {3, 0.7}]}}, 
          ImageSize -> {397.3333333333331, Automatic}, 
          PlotRange -> {{-2., 4.5}, {-1.5, 1.5}}], 
        Attributes[PlotRange] = {
         ReadProtected}, $CellContext`DesignComputationPlot[
          Pattern[$CellContext`r0, 
           Blank[]], 
          Pattern[$CellContext`\[Sigma], 
           BlankSequence[]], 
          Pattern[$CellContext`Pt, 
           Blank[]], 
          Pattern[$CellContext`G, 
           Blank[]], 
          Pattern[$CellContext`L1Loss, 
           Blank[]], 
          Pattern[$CellContext`tr1, 
           Blank[]], 
          Pattern[$CellContext`L2Loss, 
           Blank[]], 
          Pattern[$CellContext`tr2, 
           Blank[]], 
          Pattern[$CellContext`NoiseFigure, 
           Blank[]], 
          Pattern[$CellContext`B, 
           Blank[]]] := 
        Module[{$CellContext`dbc, $CellContext`zradar, $CellContext`Tm, \
$CellContext`fm, $CellContext`kBoltzmann, $CellContext`TempElectronics, \
$CellContext`c, $CellContext`\[Lambda], $CellContext`dr, $CellContext`Nfm, \
$CellContext`Tint, $CellContext`fbeat, $CellContext`Gt, $CellContext`Gr, \
$CellContext`ThermalNoise0, $CellContext`fleakage, \
$CellContext`ElectronicNoise0, $CellContext`NoiseL1, $CellContext`NoiseL2, \
$CellContext`NoiseThermal, $CellContext`EchoEnvelope, $CellContext`Echo1, \
$CellContext`Mask, $CellContext`SNRdB, $CellContext`fc, $CellContext`dfm}, \
$CellContext`dbc[
             Pattern[$CellContext`power, 
              Blank[]]] := 
           10 Log[10, $CellContext`power/10^(-3)]; $CellContext`zradar = 
           5000; $CellContext`Tm = 200/10^6; $CellContext`fm = 
           1/$CellContext`Tm; $CellContext`kBoltzmann = 
           1.38/10^23; $CellContext`TempElectronics = 290; $CellContext`c = 
           3. 10^8; $CellContext`fc = 
           94 10^9; $CellContext`\[Lambda] = $CellContext`c/$CellContext`fc; \
$CellContext`dr = $CellContext`c/(2 $CellContext`B); $CellContext`Nfm = 
           Ceiling[1.1 ($CellContext`zradar/$CellContext`dr)]; \
$CellContext`Tint = $CellContext`Tm; $CellContext`fbeat = \
($CellContext`r0/$CellContext`dr) $CellContext`fm; $CellContext`Gt = \
$CellContext`G; $CellContext`Gr = $CellContext`G; $CellContext`ThermalNoise0 = \
$CellContext`kBoltzmann $CellContext`TempElectronics; $CellContext`fleakage = 
           0.; $CellContext`dfm = $CellContext`fm/
            6; $CellContext`ElectronicNoise0 = 
           10^($CellContext`NoiseFigure/
              10) $CellContext`ThermalNoise0; $CellContext`NoiseL1 = 
           Table[(($CellContext`Pt/$CellContext`Tint) \
$CellContext`Sr[$CellContext`f, $CellContext`tr1])/
             10^($CellContext`L1Loss/
              10), {$CellContext`f, $CellContext`dfm, $CellContext`Nfm \
$CellContext`fm, $CellContext`dfm}]; $CellContext`NoiseL2 = 
           Table[(($CellContext`Pt/$CellContext`Tint) \
$CellContext`Sr[$CellContext`f, $CellContext`tr2])/
             10^($CellContext`L2Loss/
              10), {$CellContext`f, $CellContext`dfm, $CellContext`Nfm \
$CellContext`fm, $CellContext`dfm}]; $CellContext`NoiseThermal = 
           Table[(1/$CellContext`Tint) $CellContext`ElectronicNoise0, \
{$CellContext`f, $CellContext`dfm, $CellContext`Nfm $CellContext`fm, \
$CellContext`dfm}]; $CellContext`Mask = $CellContext`NoiseL1 + \
$CellContext`NoiseL2 + $CellContext`NoiseThermal; $CellContext`EchoEnvelope = 
           Table[$CellContext`Pt (((($CellContext`\[Sigma] $CellContext`Gt) \
$CellContext`Gr) $CellContext`\[Lambda]^2)/((4 
                Pi)^3 ($CellContext`dr ($CellContext`f/$CellContext`fm))^4)), \
{$CellContext`f, $CellContext`dfm, $CellContext`Nfm $CellContext`fm, \
$CellContext`dfm}]; $CellContext`Echo1 = 
           Table[($CellContext`Pt (((($CellContext`\[Sigma] $CellContext`Gt) \
$CellContext`Gr) $CellContext`\[Lambda]^2)/((4 
                 Pi)^3 $CellContext`r0^4))) \
$CellContext`gHamming[$CellContext`f, $CellContext`fbeat, \
$CellContext`Tint]^2, {$CellContext`f, $CellContext`dfm, $CellContext`Nfm \
$CellContext`fm, $CellContext`dfm}]; $CellContext`SNRdB = 
           10 Log[10, ($CellContext`Pt (((($CellContext`\[Sigma] \
$CellContext`Gt) $CellContext`Gr) $CellContext`\[Lambda]^2)/((4 
                  Pi)^3 $CellContext`r0^4)))/Part[$CellContext`Mask, 
               Floor[$CellContext`fbeat/$CellContext`dfm]]]; ListLinePlot[
            
            Map[$CellContext`dbc, {$CellContext`NoiseL1, \
$CellContext`NoiseL2, $CellContext`NoiseThermal, $CellContext`EchoEnvelope, \
$CellContext`Echo1}], Axes -> False, Frame -> True, 
            FrameLabel -> {"frequency (MHz)", "dB re 1mW"}, 
            DataRange -> {$CellContext`dfm/
              10^6, $CellContext`Nfm ($CellContext`fm/10^6)}, 
            Joined -> {True, True, True, True, True}, PlotStyle -> {{
               Dashing[0.025], Red}, Red, Gray, {
               Dashing[0.05], Blue}, Blue}, 
            PlotRange -> {Automatic, {-170, -10}}, AspectRatio -> 1, 
            PlotLabel -> StringJoin["SNR = ", 
              ToString[$CellContext`SNRdB], " dB"], 
            ImageSize -> {200, 200}]], $CellContext`Sr[
          Pattern[$CellContext`f, 
           Blank[]], 
          Pattern[$CellContext`Tp, 
           Blank[]]] := $CellContext`S[$CellContext`f] (2 - 2 
          Cos[((2 Pi) $CellContext`f) $CellContext`Tp]), $CellContext`S[
          Pattern[$CellContext`f, 
           Blank[]]] := 10^7.85/$CellContext`f^3.05, $CellContext`gHamming[
          Pattern[$CellContext`f, 
           Blank[]], 
          Pattern[$CellContext`f0, 
           Blank[]], 
          Pattern[$CellContext`T, 
           Blank[]]] := 
        Sinc[(($CellContext`f - $CellContext`f0) Pi) $CellContext`T] + (0.92/
           1.08) If[
            
            Or[$CellContext`f == $CellContext`f0 + 
              1/$CellContext`T, $CellContext`f == $CellContext`f0 - 
              1/$CellContext`T], 1/
            2, (((-$CellContext`f + $CellContext`f0) $CellContext`T) 
             Sin[(($CellContext`f - $CellContext`f0) Pi) $CellContext`T])/(
            Pi (-1 + ($CellContext`f - $CellContext`f0)^2 \
$CellContext`T^2))], $CellContext`SweepPlot[
          Pattern[$CellContext`r, 
           Blank[]], 
          Pattern[$CellContext`B, 
           Blank[]]] := 
        Module[{$CellContext`Tm, $CellContext`c, $CellContext`\[Tau], \
$CellContext`fbeat}, $CellContext`Tm = 200/10^6; $CellContext`c = 
           3. 10^8; $CellContext`\[Tau] = 
           2 ($CellContext`r/$CellContext`c); $CellContext`fbeat = (
             1/$CellContext`Tm) ($CellContext`r/($CellContext`c/(
             2 $CellContext`B))); ListLinePlot[{
             
             Table[($CellContext`B (
                Mod[$CellContext`t, $CellContext`Tm]/$CellContext`Tm))/
              10^6, {$CellContext`t, 0, 1.4 $CellContext`Tm, $CellContext`Tm/
               500}], 
             
             Table[($CellContext`B (
                Mod[$CellContext`t - $CellContext`\[Tau], \
$CellContext`Tm]/$CellContext`Tm))/
              10^6, {$CellContext`t, 0, 1.4 $CellContext`Tm, $CellContext`Tm/
               500}]}, Joined -> True, DataRange -> {0, 1.4}, Axes -> False, 
            Frame -> True, FrameLabel -> {
              Row[{"time: ", 
                Style["t", Italic], "/", 
                "\!\(\*SubscriptBox[\(T\), \(m\)]\)"}], 
              "(frequency-\!\(\*SubscriptBox[\(f\), \
\(c\)]\))/\!\(\*SuperscriptBox[\(10\), \(6\)]\)"}, 
            PlotRange -> {Automatic, (1.1 $CellContext`B)/10^6}, AspectRatio -> 
            1, PlotLabel -> 
            StringJoin["\!\(\*SubscriptBox[\(f\), \(beat\)]\) = ", 
              ToString[$CellContext`fbeat/10^6], " MHz"], 
            ImageSize -> {150, 150}]]}; Typeset`initDone$$ = True); 
     ReleaseHold[
       HoldComplete[{$CellContext`RadarDiagram = Graphics[{{
              GrayLevel[0.5], 
              EdgeForm[
               Thickness[Large]], 
              Disk[{0, 1}, 0.2]}, 
             Line[CompressedData["
1:eJw1VQs0Veke984kt0wPXCkaTUJlomg8fq6G6aEY1cqQm4rRjEtnoqYyV5Qy
EXmVysjxqNDgHOd4P75jKqmUV6N0i0rjynntjXsNueYe39p9a+21117f2v/v
//+9PvP9h3xDNNTU1M6qnpm3AV3NxFuq7u2bqsC3W+72JrRLSGbkuxcu/Qo0
nz6eP50sIX9t7jadslfCuHFV2BEvCRFn+5lsTVXi+/++tpPNlpA3wkrnVqUS
GXttJu6mExJt4fbbpCWDjV2ZiaK5hKywSbDk+zMY/eJ/Jnnnm4ha3i17zfMM
CqpDSlM+aiL996LXezcw2GH12PXHs41EeUBgXT7MQDNnfce3mo1kcNncxK8W
sRDPzd23O7aB3GrYOBXmyiL41CxVyXqiN202H/tZLPjPofi1J+rJ77s6e43j
WNwJfbbQbLyOdN4ucgq8yiLqudtN/ag60rryvtXJUhYW24od3zO1JHVbzg/6
dSyeEIMHQ+G1pEu6Y4uAsFAVC+gZriESw3FbmyYW9tffyG6H1hCvxT9UHahk
8dZwa4zwbTVp1Zk65nedxcy0ufuqSXiAdo11CguPmXH7qojrHvddS3gsaHsB
VcSvO8MjdSsLWu5ZJTEs8jz8pxmLXXRVkkwrN30dloF2a+Oge5eY3Nb6Z6Cf
Cp+qzz89ZustJnXaL/V+Pc3gGxV6S9pEZOiYaHLZJgaLzMav6m0WkcH6IXHY
bAYt6TOEVBDDv9vPsstS4igtWEEk/nOOBhsrMVPtCRGSqdM++SdzFOgZvvyq
2VlI3g/7F2QtV+BsoPrh8loBMX5ekFApkmOGjZz1AuIyGr+6fbMcbWcKVy4M
LSeavHj/pncyrFapgwkqI2Oalp86XpQhxcfz0EP/UjLmqaF1d5sMinWG6Td2
/kLma1/h1S6QYbvJkDhu+y2ivl5xfI1UijK12qd7NpWQ46EyZ/UuKf4yeG7S
wb2YlEXcj7pzX4qIh/6m852LyNUpod091X6HwNpNse4mydV80qMvl8L20tT+
+2tukABrF3dfIxlSox+pOrxO8k8YhWntlIEJulZ08pNCcmAdsfXKlYG2Z1pA
ChYPx8yZkIGWM8wnbS0Opf/eK8c86o884pjtM9ujSw4e1TufaPC2R+/ergCf
6vcaKRvv+Hh5l4LTYzbZfTAs5kKAktPXFeI+b9QicEjJ6SWLLEja+425E4NC
yv9FkiUwaj8axyCY8plBHMZOlOi0MLCg/KSRtRHFjuWzWLRSvC+QzKbdpv4e
LHwofklEJzptrXoMy+GRQEoVFYtEApab7zTxK+QFZb1guX5jiEn4vpARtRHw
6flHyKp/RMZZmo7AjdYLI+ZPptsltiOIpfu7iHtU4+ZQpxGoDcx8eyD3lMRF
4TwCCd0PAUmKXeK9bgRB9LxI4Mcvc0ssVN+0XjTadMPMN+mNwJbOEwd+kU5U
5jsWAvr/GQg/mydbKmFhRvE6hzfRwpfZFz70mwLnoMpTT/1Yjo80OMtj9zQs
/jBfBrT6jizb8oLh+L4IeUupd/plhsMjC13G4mCRL4N5VE9X8JGu42i3HsPh
l43s47YLnRKVSKUFczAq2bLh0J8KdFC8c/FWovM2/rgCP1E/8HFH/HVi4aQc
lnSePJzZk2n/W7wcrdRv+ejcdqNz1FiOg5TPAiy6W7LzVr0MutTPhRhYepaE
h8k4/q8jOemxY4SVDDQe+26gOma5588TUk4vNxHze4Erv1fK5VERvjCw0Ox9
IOX0VYyvT6zQCngk5fKuBMYa7CXmlZTT4y2ki7wummjLoEXztBQOZXEuRhtU
51N8y9AjWO3+Olp1Ps3rcux7zRuzb5PhMNW7AM9WtoZnW8rxK70PBAhKfng1
/IIcH1M+hDBUm9Z8OS3H/hl7ZQrx3SKNNVNRClRQP1Ugwm0h755SAY0Ze1+t
wJch1hqrw5XwpfyJ8P6Rq5OJVAl+t87n4XwRFN0OvcSO4fwqhl3hGsNVPAZu
NEDE8M0eMH5V/IHvSoxv9po12s/glW7IWNUvlbAyiDLOMGC5PKjC5E+uUk3V
fRH7SWbnvyqqMFv8x4784A9+qUbJZxEtg/Eq/bneVkVQNdLUhVah11jwaN7U
YKDP1+aZyk8Sv5Gk5aQGB+9FTug3sJyeanGqQ/T9lOq+CIo0V12htehialuq
VPsCmmd1cHVIS4+oYKFGA7EOjaPJ3x3OY7n8q0eKv3DcOpEFha+zHk2KtFzT
cJbLywYExfEf9G9hQdt72oBBq1U9phYsl6+N4Nm0N6dPMOh7ObMaIUhuLtJu
Y7g8boLP9X1Gnj8ziPljTr3G2yZUbdy4yzKMwWOqbwLLADd+3AYGS+Y7X14x
TKB7+9x5C10GDjTvJWj8aqmirlyJ/kuVqhtIgj3PXQP7fJVIoH6QYEAiqFz6
XoE1VJASBFn+LbCtWIH/A+StStE=
              "]], 
             Line[{{0.2, 1}, {2, 1}}], {
              GrayLevel[0.5], 
              EdgeForm[
               Thickness[Large]], 
              Disk[{2, 0}, 0.2]}, {
              Arrowheads[Small], 
              Arrow[CompressedData["
1:eJxdlnlcjdkfx89DixJFIRmytIgQNU1N6nsmlcRkTyhLkbXBKMQgugkhW4tc
7SlUbtoXnXtlS8RVlMw0mmGEmKIQnueZ2+/X+efcf+7rvJ7Xufc83/NZ3qP8
Ns1b3QshpMsh1P1d2TnWJ+oaD6jn0+HYWpSuWo//O2e/jtoqSCTbq1q618ed
Slx6B0Fj3PFI70oe+vs8yf7Qaw+MvLG3s0O1PpVif80USWBG9cCvV6/zkN4a
sdaZPwj9c6VH8m7woPHy65SNn49Cu/WnmEc3eXg480FD4o4TEDFd0tvkNg9m
y08rsz6cgi8Nk4oTqnjoM9Jrz/jAGEip3xLrWs3D0ouyHa+fx8GJ/XNCDe/x
YPTL2N3FS85Cwxc/zyH3eRiY3GvTqppz4G9U9+QnJQ8HH+nkNBknwaio1Z9i
a3n4arwIz1iUDJ4/bt0x9DEPJDA+ffHRFJDUfhx3s4GH4Au6sk+KVPC7H/nk
3FMe/A2Vnn0702Ddoa0zk5p4aLpVGvWb2Xmokp1xudfMQ9pp92Rrrwwo+RQX
bvKCB/78ljpDSSZoq8ur01t4CHXal2AguwARGYOVc1p5OJ7ZGq/beBHkHv6z
TNt40HM0O/qey4K6LKv20R2q80wN9Dtvng2VdbWbZnzmwSIw3tpgZg7Uuv8a
HPeNB3PPV26mGy/DSUmfLF1OgOK/mg6udJVBS/bm0hx1Abz7W5/bUCuDawfX
uQRrC3AstvWu6YpcKMo899pPV4A3c4tHhL/JBetnFz22GwhgfWh2/p7gKxDS
aiHmDhUg/9Ocak3hCrzzLUwcaCxAWtSQikHheWBY/lEeZyJAp0aUWoJ2Pkif
p3VMGydAg4HG9zHH8iElLaZK30qAsjmCWodeAfwbebNa01aA1SldM7JPFEDv
Lh+J8VQBZli7finSK4TDviUhS5wFMFMqJmgfKwS/0l2Tit0F8LlyYUS8VhHs
m3fMzG62AGePzB+3LKwI3p6a9MeThQJEvnuvnPW1CPrbfquR+giQdG9hmc+W
YpAc6QoK9RfAyj4xV/KiGMr7JblK1guATcT0skUlEKjzT37GFgGu/7BG9vlW
CZileti83iHAyMSpeta2pTBE+WzKzFABxik0JvillILtrSXD70So5lVvFLtb
pwwaVw1aFhAlQONilz67g8rg3V9jfI1jBVhmvn6Vd2MZVGctDe1IEKB6k1Wp
plM5NK5eMf7FeQH6/hQ7LSyxHDqMZKQtR7W/IDqgUiiHKwmXpYOLBPixMLdA
vvQqnNAtMvYiAux0Ot22tfAqaESMSMu5JcCIUfFqj/tXQHibR5jxAwGC7Vb2
e7aqAgI1ywwyGwS4/23uwsjiCmjeaZAzvVmA6CBTL6UWgQjbyan8KwFebl1T
csmbgNF9S7eq9wKEjOr0NkgnUDfg15cXvgrwrHA7qP9LIKrg9qMENRECSgIX
TB0mh1hL/2UZ/UR4uXjlvnxHORjFtN69NlgE3zM7t7Usk4NhTcyGNmMR0MUX
0kd75FAePzZskoUIdyo9toeck4POx75L904RQVYvJitL5XD7Vx/7Px1EsHO2
q336WA5Lyzr9PF1FKAmZ8jS6XQ4Ki/3D7nqKUJTvn8ZrK8Bx+6H0Rd4ifJAk
Ww8Yo4D8/Xud21aK0Ow/I7PGXgEnLWunRG8Q4fiEgKk2sxWApvMFrsEidB6o
dcH+ClhwJkqb2yvCK8vlk9uDFfBb57Rfbh4UwdGi5YhrhALcl/UdEX1ShG1v
dvEOsQpY5Djca6NUhHfOW1xq0xUQmqExb+Z5Eazt9jlp5ykgLKdk1mSZCFWV
f/z9Z4UClBXS0OGlqv0r3NznVylgSp94c93rIhzdbnTW/6ECzNsm7lKvEaEw
fZ25XqMCyo7syhfrRVCr5xZ4P1NAmtk8Hb5ZhIn2u1IdXihgs8Kt7NsbETR6
O+eXtCjgWGZFF98pwkq9BPzwtYLmMv6/fxVQcmlkb04dYfrcPdEnhdNGmO7P
aX0cwukiTH8/tTuWDRCm//+2O5YNEabn+18sD0eYnj8uKRfaRyNM369ibOri
l+YI0/fHWd+8Gi0RpvNZuLK9rmoywnR+v8/5srnAVrW/Z77O6k23pQ4I0/l7
WRfv3IsRpvcT6sOZ+roiTO+vaF1MwPceCNP7Pe8kb9KcjTC9fz8Xhwl18xGm
+jB+4/T+jDfCVD/zR4fXePsiTPUlvTnXX88PYaq/L2o758kDEKb6TLmywWnt
BoSpfjvibO5obkaY6vtD/qEzCUEIU/0XrKuZYxmCMPWHffqwaNluhKl/Bn9y
0xm/H2Hqr61xr3zOHkCY+q981ZqJXKRqf48/o651FzDC1L8ev6V0XjqFMPV3
RcQSp7ZYhKn/d5e3ZY+VIkzzwW6HSduCJIRpfmRYZeYGpSFM8+XAe/OkiEyE
af7o35u9/mgWwjSf4i0eknAZwjS/DvX/Ze7mfIRpvp36ufPBrGLV/p78Wy2d
125UrtJjTz4GxUYFNBKEaX4G5jS+OFyJMM3XO11HTC1vIUzzd3TXpD+u3kGY
5nOFfdgNqEGY5nfX831xuUqEab5r2a4doP8IYZr/5HFxq38DwrQf7o92bkl9
ijDtD4e5YyofNiFM+2VPmNKxrRlh2j99Sr8z+vYcYdpPl7Nthn18iTDtrytH
s9WbXiNM+60jWZKY91alz57+01KayLa1IUz78brWBmT+AWHan/9EXVxyvVPl
j55+TR+fe9jzM/VvLiQ6VPve+IIw7WedC7dSxvII0/4uuvrVYoeIMO33mKdu
N/I4DtP+Txg02P1pbw5TPoi5sCbinTqHKT8kLalZ/FaTw5QvHLOCYuu1OEz5
YxdvbJTdl8OUT0wk1lcD+3GY8ovUPM93qC6HKd/MdHBrvKzHYco/sw1/1J88
kMOUj5xiJK+l+hym/PTE7+a0Dwaq8/Tw1Sa7n9VsBnOY8tf994P0lw/hMOWz
0/trvIMMOUz5raiinmwdymHKd5MMptv4GnGY8l+Yfkuq1TAOUz50/N1ReKta
U37UDJDYRH/HYcqXIRm59mbDOUz5c2n8tl7JqjXl083Dcbj6CA5Tfj2V90Pp
fNWa8q3VSJezkao15V+NZi2T7O51Dx8bh3ffLIcpPzPPCbOfML9PmP8nzPkI
c37CvB9h3p8w8yHM/AgzX8LMnzD3Q5j7I8z9Eub+CaMPwuiHMPoijP4Io0/C
6Jcw+iaM/gnjD8L4hzD+Ioz/CONPwviXMP4mjP8Jkw+EyQ/C5Ath8ocw+USY
/CJMvhEm/wiTj4TJT8LkK2HylzD5TJj8Jky+Eyb/CdMPhOkPwvQLYfqHMP1E
mP4iTL8Rpv8I04+E6U/C9Cth+pcw/UyY/iZMvxOm/wnDB4ThB8LwBWH4gzB8
Qhh+IQzfEIZ/CMNHhOEnwvAVYfiLMHxGGH4jDN8Rhv8Iw4eE4UfC8CVh+JMw
fEoYfiUM3xKGfwnDx4ThZ/IfDPMDdg==
               "]]}, 
             Arrow[{{2, 1}, {2, 0.2}}], 
             Line[{{2, -0.2}, {2, -1}}], {
              GrayLevel[0.5], 
              EdgeForm[
               Thickness[Large]], 
              Disk[{0, -1}, 0.2]}, 
             Style[
              Inset["\[Times]", {0, -1}], FontSize -> 30], 
             Arrow[{{2, -1}, {0.2, -1}}], {
              Arrowheads[Small], 
              Arrow[{{2.2, 0}, {3., 0}}]}, 
             Line[CompressedData["
1:eJwVVXk01fkbVr6f7w3XUkrF/DIyKUrXqITweUtR2SdSiJR0ayxlimqyZ83S
4CdZQhtKRbJMmj4ylpv14l77cqVkaXKzTpJ+9/fHe97znPOc9zzvOc/zvion
vH85tVRMTCxYVP/vZfl1ieevMeDSzsfa580H8eBo0fdt5xkwcWPIIUZtEA/U
ywSqODPgwRxStfkuwOsMUuJ2mTEghwWple0CfC8iMSdUlwEXy7aNDeULcMNU
Pv66gQGBD8sOlwUL8LPUXs5deQYYRaUbGtsJcMQfBwoDljBAWdZFIXKjACs6
ap6MnaBhTNg1tPLzAK7PLd3b0kdD8f3C6bQbA3itx2SpWQMNqx6rvs/XHMCL
+g8UF1/Q0HH7Zw8rTj8GCVwzmkeD4U7389eP9+Nm3UJZuVs0wPWdzDOzfdhe
3MXUJ5IG7aBTocKoPiyJF8uWX6LBNtHWXEOpD5OggEcTp2mwX3STVH7Yi+0y
Jo5LH6FBsM8krkOnF5+LYSt47KchApvynV73YM8nn8Sl9WhQyrizrOxAD472
O+b0WZ2GBUnO1cnmbrwhxNJCUYmGlcLsglW23XhMYun3SCYNGX0d5pvbu3BL
i2mS0SKC7r+kVQ0Pd+F7vmwNAyGCEK6yph2vE0+yZLtC3iIwueYY42/diSWW
bH8mz0eADIsjK+o6cIUjXTlRi8BK4Wv8JuMOXKzTKbOmHIHLM2pHxZ/tOCdF
4lbsEwS5GwRbE7a2Y+/UaQebOwhCZRfs87L52HSwa79bMoJMoeVjOXk+5iQ0
WtdGI1jD/d2iLYSHt1Xdc/g9EIEjTPstCNuwk/uQafAFBGv5q2NuHmvDFU8f
TnWdQfBGyWZZAacVS9G52uEuCOI2aUXZaLdiHzbVFm6HQPtNCCcitQWrONmm
95oh+O3c2XTnJS1YxinSKnIPgtZx79lBdy7+pbHvQbSeSM9H9epdyc3Y7DLX
7p0WAveLtUxDpyacGMeUSN6EwKDgXyxQbsRf1V3ds35EcGcOJI0G63Egu4hJ
rUXgZHJlr3dWHTYRn0x8vRyBm/b7yZhjb/BtX4nYLkkEwWE7jAvWcPDw885g
cwqBbWpa9GRLDdZ3V5tRXKRA/X6dsWdUNW4qTj6x718K5mUXmPq4Chc8zjZq
mKQALxfe95mqxM9Vomef/kOB3/3xf7QevMbWGnZC4QgFl+WfD8bZV+BvrMro
+HcUVJvKbbOQfYWrIoz/EyOg4NvprXE/eZbj+Oy5ueFeCkpvfXTv5JThKs6d
k1ldFGQ0qV3JWF+CM/3yssvaKbghZfko90oR5hhKfWTxKFBL3S/GainAQfrO
F+lWCiJWZurXm+djjkY924BLQWF+YrD4zAPcKCU51tJEgfT8wYDzadm418Zl
VW0jBfIx5fPfg1Kw/PNb35VEuDhEJ+10wnUsFitXwm2g4MzVcYe7IWx8iHvH
6L0Il10ICP7xbgDJ+m/JzaMi/t1M3Woz1wTiqqtXpimaDwUlAcfXZBCmdFa8
azMF/e2B2inDd4nNo1E0I9JjvqOy3wfnEcvGzK8fRHq18/hhdslPSM7fXQf0
+RTExB3jnJ0pJAYBuHK8g4KPocvM995+TuzUpVkL3RT01LB0QvaVks/Oqw/9
2k9Bynqt+ImxP8m78eZR3bcU6PhnhdrHvSTum8xy3IYpUHI/xw5jEVIwtE/3
0xgFkSTYZf2HCrLu7eY9/AkKZkvupWvtqCRe/6w7uXaGgvClo85twX8TX9/F
n1/MU/Aw91OxsKGKNNV6G5WKIdC7XdTspVBDijjDcnIMBA56N+e3OdeSBKun
GlXSCFatc8xRu8chbuJzqG2lyG9XVX7VHHlDNtR3rDL4AYH6JfYSlkY9Sao7
UbOoKspT4lLLFWcbyMnxR1cVtyCQnpJyeZXTSN595ifHbRfl90ivl+JQE7H7
QdnDwRCBRtv8JC3DJS/NPpn7myCQOe23FfK4xLlYEDJrhaDGX7N5i3ELKVDq
tK4+iqDd8zCvokeEYyWmRk4i6B+XPCL0aSX+j5iZx70QFMVr3Xi6rI3Me42H
br6MwGfrqebJ9DZy0E+7bv81BDD3IewJi0da+UuSSuMRpEDQno4KHvF3XPfN
Nw3BtynXIldrPhlyE1cKz0HgFy6bs6efT+ZUZxaGihBIeehJe59tJ+SUfH58
BYK306lzPdPt5Fmrx+bIRgQtIbzUAP8Oop274lJDN4LDtLGVOdVJkka3RDmO
iO7LM94f+lGdRIsha719FkEAr5vCzC7S/MW8zI6iRX7oPGcV20X6udycVyto
2PjWqtqV2U3yCoWLnio0WJyJXOsR1U3m9oWlnNCiwf2o/WM3qofop4gdTMM0
BMoNju727yEpjmFDClY05JYfOTE/1UOGMxZZPGcarvq/5F1j95KBwZ0zLV40
HDSK0+js7iXRmTNTMoE0PFkddXn8YB9Zri4zEhVPg4HFIaqkrI98+RwSuTtL
9B+qtuxi/dRPuDqpEdsLaeB9uvXSMrafWIp/SXCtpEFttdRh5nQ/eZ/016Ha
NhpSGUHU8SMD5OHqVC+39zQc/bJxZveLARJePhG6c060z+DiHj5TQG4qj7Gw
BANMlLRUD+wSkPjlaZJXlBiQbtf29RJbQOZuK+QOazLgS0OniWuSgKj4xqeE
AgPYOeU3F18JSGTSb2yrQwwwsKxJsxgRkAsQlbXfnQHZVbaKZnKDJMaMWeN5
mQFDMytnpnQGyf8AWA7WDQ==
              "]], 
             Line[CompressedData["
1:eJwVlHs41Nkfx20ubTvM92RD2trIbpFstS6/CeN8Sn4lkVvItZRcS7XLkHKb
lbbxaxVK6xI7SVorycavllOYzNCsuzKEuZiZeqoVq6SW3/f3x3k+z/t5zvM5
57zfr88xDY/3jlikoaERSa//V2aBY2/GJSa4jWontGo9x+ZTB0a21jHB5/zR
joqbaqyrJ7IoFjPh0xrz34s81FhdHll4Xs2E2Ja88OpJFTaY28000qRgxQrl
12O5Kvx6tDPZehUFNy/+zHHYqMK1Kn6P1I6CuYThv4WdSvzN6vUGJp4U2Ke8
CP3psBKvnz+2+2UUBUffZf18aWECr2+IP7s9g4JsV7dbyksTuKvbXL72CgWh
5w3bszdM4GJGd9qFWgq0qsKsMh8o8Jbcv85ltlNgU53vMeqtwB4Hc+xnn1FQ
d/t4wzW5HK9duevPyWkKujb4LRs9IcdXXz3a8bsWgkDB5U15GnKMcLd6M4Ug
Ttg78zhHhhU66qQjxghGHFmx+UYyfPpboWWkGQKLqqH6qTIpps64239pheBl
4ZT+G3MpfmuWLbtgh0BvcqO08NY49qsWRD7ECNasmCmYthnH/dNV+tU7ESBX
J67BiTFs7Wm92csLwRs+74tuzigWJIsNa/chEMfNJK1PeYbLNUJXCg8geNSv
u4d1agRzfL3KCqNpLdqRppcyjM1CK3VMjyPYafi9UTNHgoerFhr3JyGoZKzz
DD0xhK1MK9b6pyFIG7TgLYp9ionPlTbNMwjyimVe5MATrOtxVe9wDgIh955P
ld8gtvvHJfP0RQT/aUk/NOA6gL9cPXXDtRCB81DHyQCHfryoWaXXUYJg3MC7
ZodlH1bZ+TjP/4KgIMPDrtG4F3/a4SWdqESgH9gVSXR68J3691np1QiajqyJ
mlB04Td9BwbEtQhyZ/nuXf8V411dN5LF9QjCTrqIInM6Mavzw5q0RgQhgfHe
kmARrvilOFt2H4HNuTiet2U7pipVB981IzBI1HT6+K4NC5UBJ5seIiiRMgI0
W1uw1vvcPJs2BI3Tn6jzch7gLXv2pQY/QhARb2VdX/UHzmfcoGyFCFJ/O3VI
i9WITYP5a5pFCNpLk7443nYHJwZeK3zXgWC6qSHmemkNLojfsk/aiaDL3z14
cV0FVjmYe6c8RlD02b3DR7Ov4NWa8RwBrcOXNHds5adiZZFLK6H145COnsdx
PxAP+Sq7GFprKzkRPcuKyYs7/ME2ul+U1WZl1ux1Urn/j9td9HkWBYE/PlXc
InvvZEnO0fdpCdnK9o+sJyjeN+ZtO72fLVbclDSSHL+6dH36PcPndQ8ydzeR
1LtBzmOtCC4obLn5zx4Qz+8dR4NpPxZHJH/d83kr8SqP4/Jov4peF0zm7hSQ
Wxc3xMbSfmru5UoqU9rJ2dTxvo8NCA5B0NzzahGJLmUsAjoPdrOAZznSSXKf
7AJbOq+S2fApWPInSX7uMTbyK4K7qsLQBeNuom02aPsvOu+Z7Pf9K7/pIZLS
irLtNA8R3O2+cuglUYszM7VpXki+RTzbu4/Yy9dtP3mZnhdRS79BeD8xiXH2
5V9AkMRw8vY/NkDE8sh1GTwEt91+6H95apDU5+qwlmUhcHccjX6Y/YTUrorQ
CUml/Y2+5Pwg9ykJ5uctD+YgYBQXNvRfHiKPNofPLT1G8xjE3PaiWELCnu9l
p0UhOJsye+bl1WHyvrwk6dp+mqezAp/2shFCTHRcMwMQlM0pyg5ffUY0Rl95
Gnsi+HGpf97dolEyFMRixexAMLhpSyq/YIxcZ26tTnBCgH3XTeX8e5y4PxWH
OdrS8yxrcdR8OE5ijD5hEEsEWZmO+pb2UpLo5hY7b4rgiM3bhbnbUqKxJ9j5
gxHN+x6zmmhzGbGyWBZ0Vw/BlA53/EiJjJRxvkvbpInARLicM4/kBCkMtQcm
KdgZLjuqz5UTq6XDiR0SCrpf8WRl03KykW2r69xGASvclFEeriArBfIz8BsF
QnETfNatIDbnnOraCuj/1ZT1oc9hgpiYfH6w5zQFrzLEvTMVE8TFKZ0TFUGB
u4EoPZKpJMs53YO83RT4sSzGTRKUJIrle5xtTYG34bbXxhIlSTBygDRjCrZd
DOncxVaRj7FxLj4LTHBKDLj/a6mKfMsO491XMKF3xHGt7byKDHPffHVPxIQl
wqHCsSA1ud7mZOVVw4TTEn8Bv0FN/gfgiw3k
              "]], 
             Arrow[{{0, 0.8}, {0, -0.8}}], {
              GrayLevel[0.5], 
              EdgeForm[
               Thickness[Large]], 
              Polygon[{{-1., -0.7}, {-1.6, -1}, {-1., -1.3}}]}, 
             Arrow[{{-0.2, -1}, {-1., -1}}], 
             Line[{{-1.6, -1}, {-1.8, -1}}], {
              RGBColor[1, 0, 0], 
              Dashing[0.02], 
              Arrowheads[Small], 
              Arrow[CompressedData["
1:eJxdVFtIVFEUnYLKwkAikl6TfSQSKmjmR2WLDNLKLFOESiMMRYhIEsxUQiLL
FDRI6KGWNkNhgWhJOGKzVUSTxDQtR/MxNo4ISoRpzNucs8/5ccPlcjnn7rPW
2mudvWk3zqev1Wg0MSuP9z3wOeFW/rIHGaJm8MD/dGO0wYOOpaCU8g4rPp0y
NoVle6Atmkto1lpR/3zP24wQD/K21sdM5U0j8uVoimbWjWFdVtSmYQsqdmwJ
1OrcOCDKgtxx/Ji87Ea5aPgLvkd7qtN3uuHtVjQ3hQDdi8WvJhdWmnk7ouix
pxBPXNB72+nM6CrNcQ8lu7AsahI+ZVHaRn8XmgWgCVTNr8tZHHFCbM8ax+uI
9ItU7YQX/VLQGJqTF7rDrjphFu1HoT94+MS1/U48EzWCmsoPX0r+OiDgJJgQ
fqittt7owEYmBEOX5uRiqUPq8R1x+a7U3EsOyX8IBiTdSQ5xIFzwHcSrwurQ
ujUOyW8AQdE+7Q9Ndohuun4UxC4c8Xtvx0eBvw9Dx4tjEsvsEm8v1ndXbM68
bsdTga8HozcrQ9PO2uEj8HTjX3BqXWKEHbni/E4EZpt74nbZMSvOa0eJddv2
lA12qY8RZzyR8WVLNtnPgOWaiWO/Z2zy/yb8afaz3PtpQ4PYX4/7JbtDLwza
kCXW9Tj37u6+rD4brojvCvSKskEjqqZVrRfyOqn/+VtPqn8b9yd1Pq83UbzE
V8P4SOHn/UZS/OaZHyn+BcyflD6+rA8p/apYP1L6BrO+pPRvZf1JzYf91k9q
fnzeAKn5RvB8Sc2fzx+iFumPTvYHKf8wnmGqlf5KYn+R8h/jG6EW6c9p9ie9
kf5lvGO0yt+0yv+k8sHzMNOq/JDKVyzni1T+mN8U3Zb5fMT5JJVf5mshlW8T
55saZP6Z/zQZ5f0QwPcDFcv7g/Ww0jd5v2Ty/UL/AUVxWZM=
               "]]}, {
              RGBColor[0, 0, 1], 
              Arrowheads[Small], 
              Arrow[CompressedData["
1:eJxdlHtMU3cUx1FR23LbC/E1FGfRko1u8e1UfJyDRshE68D4SDE+itSJQStu
KLrJdHU+cFSJiQaTGjRqiptDCIKl5NAWbaGtgqJIhJbV1bgN1DE1RiO4Rfzn
3OSX+8cvv3t/5/v4xOq2pWUODAsLS/9v/f9+/0hQvcbkePGpF7YHPsnIS5ag
I/z1oeMqLxxO27KnvVCCf6T6E/SjvFATbAsfdk+COZ01/+4c4IVg29cNQ8ZK
UVdy/ogv6IHZVZ0XSzKkuLx83DFDjQdMcfXFPosUf7o8ISW7wANFKRPpm6dS
XKdfUnk7zQPXUy+k75suw/mT1LnlUR5Qvw096tglQxPaO5TuRshPPK741iZD
991p/um5jdBU3l47pU+GY8TicElMI2idSlUfRqDBK91+9VoDXKtbVePeF4Gu
3ibf2mUNIB4xanbbI/Dk56FF4zvc8H5eAUlVsFuV4YbFMy8tS5on4IjRCbsK
fnfBovzw2IQ8AW+Za0vzV7vg1MqxdneFgLnTips+c98ATc62rkCXgK2rqrtt
k2/AwcGmlmyVHIuTtPN0Rdfh5f7quA1aOWpt6ycs7a6H+phZgqVQjpd1D2Ou
JNaDZrll9gy7HJ/9ElvmNDmh7Knw14t/5BgX35xua3WAoqXi8QOlAtvzbh5o
iXbA84/enri/VIHx677YnLzS3q+PAgds7BqU9nMdWOZk7fCXKHD9o/ih+r0E
qVkVBp9bgYkHojauuF8L6mEjreZuRb+/NnB8+eBOUqSIC4zBX30GK0wtvPvE
OlnEzbVFW/Vl1RAK0N/PNCIGX5kDKY+vwr2ezLWBLBFzjP4FQnQlJBecOPa9
UcRuw9nhsxZWAI5oNjpPi+iQnfdv2HQFOs2tryxlIlpDpcPnCr/B4a3+TWqn
iAunjskcjJdA+YNZgDsitkXu/1Oz5SIMSVY+DHaKWDVX/+Ni0zn4OG5JaVSX
iNFWedVq7RnoiZ4fcj0Xsdn13aAVR09CL6xpH/hGxD19M6b0ZJtAa59krusV
MekrXWVbTn6/v5H4Idlsn9h5Yt8n9n9i9yN2f2LzEZufmD7E9COmLzH9iflD
zD9i/hLzn1g+iOWHWL6I5Y9YPonll1i+ieWfWD+I9YdYv4j1j1g/ifWXWL+J
9Z8YH4jxgxhfiPGHGJ+I8YsY34jxjxgfifGTGF+J8ZcYn+kdNsamnw==
               "]]}, {
              RGBColor[1, 0, 0], 
              Arrowheads[Small], 
              Arrow[CompressedData["
1:eJxFlHtQVHUUx3f3/jReKo3JJg/lqaJBq1DIGnoMoRIBQSZEsA0QMweRlIey
KYK6BCWIoiE4SAPmAwVR5CVwbHmICo57d1lgF3YvhTgJCULILNNQMk2/zsyd
+8d9/H6/cz7fj13U/uAYAY/HC39zzd4LLswWH0aCapecHFbg9PiCbFE1HwLy
F9TXjChwb0bLj00sHxK8Et4LHFPgs1ONT8SjfBjLsg3d/FqBy0NPb71oKoA9
d45HXJlRYGXFMm/NcgGYt6mLo41ZLL6a1j3sLYDSyZFHUgsWjYYPbWmXCMBs
fH3rsBOLgfWJr3ZLBbCIvxrLPVgse7zCGc8LoC7+rCv6sehmKY5trxTA2IsI
hUUUi7YDr/xSOgSwxK6oojyFxabnvQkdQwLwdyu3ST3HotD/88pqHgNnnHKa
Um+zOJd8MCKyZKCmNFN2RcGiu9tsMdD82d+NI+Ms1lYk1931Y2Ct9ADxsVBi
egdfcyuagZcG+bbL65To1WBqaSNlwF1YxAiilbjtgWTVRC4Dl9PsOgN+UOLT
wksvV11lIExW8eRojRJF/COJ9xsZaM+byDkxqMS7638j11kG0lsWzQ1eqMK6
DHPhr0Nv3p9nnd/jrcJP2op2Rk0z4KZKdjBOUmGxnbTZcR6BdyObeKprKjxX
8jjOYSmBQ61ZRs56FW6Kkcu3iwhwNzLypxZ1YXLlzkgEAsqTl/JdA7rw5el3
XIK3EpDtCjrWkNGFOvH+zWYSAs2TKyJy5F0Y8CLL7vdYAlGdocq8mS789NJ3
ksHDBMRBcS7169SYEzY0ZThJ4Hrymu0jh9XoECl3WpZLICvjzB/COjVWexjt
iykkkDtT6elsUKN/l6ywqpSA6b35G83E3bi8d/Sh+U0C/kcb2m5Iu3GvBCOS
qwi4qKuCXzd2I7iZzRmqJ/B9oudaFa8Hv7aGX3bcJyCa8b24elMPaubX9itb
CCz0tPjWkNGDkSZbJgPbCdy5e+2ORUcP8o41vep8RGBDawxJMO9FR8uFZv4d
BBY337L6K6QXOztmi8C/XP//3Co+Xeih66XfT+7rdEV7Df1/2pHMVOuvNHR9
u2nrGs8yDd3fG4zO80c1dP/XGg3TX6zR0vO9tfMo91Gilp7/eVVSrKxGS/vj
fMhxfJlBS/tX4p5mMBb30f5eLyv2sUrpo/3PusfT+tb10flMD7TMOTzVR+fH
xmglRe79dL7feJl0l8T30/mXPxm4nVTWT/mIvHm2gDzrp/yEyMoK1tnoKF8N
+66uNAnRUf6s2YQ1kkwd5ZMvubdgZaOO8qt1EhbuGNVRvm/H2bgMLNVT/mOL
NpSUB+hpPrQ3ju+qkuppfnIq3949+LOe5otnfe791U/1NH++nvH6U1N6ms+Z
8C+FyfM5mt/aPy+IHWw5mu8ND0LUrSKO5l8xN7g+DDjqh4lUuYkmgKP+MM18
tPHjCI76xd5xWJS9h6P+WT8QKKg/yFE/mS/mi+VHOOqvPNvp6hIZR/3WvsSh
Kjybo/4786Hrw+d5HPXjEIYO+xRw1J/2IcZxCUUc9SuXImEO/MRR/x7szG3w
KuHwPz+Xxs8OmMN/ACWkxWo=
               "]]}}, Epilog -> {{
               RGBColor[1, 0, 0], 
               Inset["\!\(\*SubscriptBox[\(L\), \(1\)]\)", {1.4, 0}]}, {
               RGBColor[1, 0, 0], 
               Inset["\!\(\*SubscriptBox[\(L\), \(2\)]\)", {2.5, -0.75}]}, {
               RGBColor[0, 0, 1], 
               Inset["\!\(\*SubscriptBox[\(L\), \(3\)]\)", {3.5, -0.4}]}, {
               GrayLevel[0], 
               Inset["amplifier", {-1.2, -0.45}]}, {
               GrayLevel[0], 
               Inset["generator", {0.5, 1.4}]}, {
               GrayLevel[0], 
               Inset["mixer", {0.5, -0.7}]}, {
               GrayLevel[0], 
               Inset["target", {3.75, 1}]}, {
               GrayLevel[0], 
               Inset["antenna", {3, 0.7}]}, {
               GrayLevel[0], 
               Inset["circ", {2.3, 0.3}]}, {
               GrayLevel[0], 
               Inset["antenna", {3, 0.7}]}}, 
            ImageSize -> {397.3333333333331, Automatic}, 
            PlotRange -> {{-2., 4.5}, {-1.5, 1.5}}]; Null, $CellContext`S[
           Pattern[$CellContext`f, 
            Blank[]]] := 10^7.85 $CellContext`f^(-3.05), $CellContext`Sr[
           Pattern[$CellContext`f, 
            Blank[]], 
           Pattern[$CellContext`Tp, 
            Blank[]]] := $CellContext`S[$CellContext`f] (2 - 2 
           Cos[2 Pi $CellContext`f $CellContext`Tp]), $CellContext`gHamming[
           Pattern[$CellContext`f, 
            Blank[]], 
           Pattern[$CellContext`f0, 
            Blank[]], 
           Pattern[$CellContext`T, 
            Blank[]]] := 
         Sinc[($CellContext`f - $CellContext`f0) Pi $CellContext`T] + (0.92/
            1.08) If[
             
             Or[$CellContext`f == $CellContext`f0 + 
               1/$CellContext`T, $CellContext`f == $CellContext`f0 - 
               1/$CellContext`T], 1/
             2, (-$CellContext`f + $CellContext`f0) $CellContext`T 
             Sin[($CellContext`f - $CellContext`f0) Pi $CellContext`T]/(
             Pi (-1 + ($CellContext`f - $CellContext`f0)^2 \
$CellContext`T^2))], $CellContext`DesignComputationPlot[
           Pattern[$CellContext`r0, 
            Blank[]], 
           Pattern[$CellContext`\[Sigma], 
            BlankSequence[]], 
           Pattern[$CellContext`Pt, 
            Blank[]], 
           Pattern[$CellContext`G, 
            Blank[]], 
           Pattern[$CellContext`L1Loss, 
            Blank[]], 
           Pattern[$CellContext`tr1, 
            Blank[]], 
           Pattern[$CellContext`L2Loss, 
            Blank[]], 
           Pattern[$CellContext`tr2, 
            Blank[]], 
           Pattern[$CellContext`NoiseFigure, 
            Blank[]], 
           Pattern[$CellContext`B, 
            Blank[]]] := 
         Module[{$CellContext`dbc, $CellContext`zradar, $CellContext`Tm, \
$CellContext`fm, $CellContext`kBoltzmann, $CellContext`TempElectronics, \
$CellContext`c, $CellContext`\[Lambda], $CellContext`dr, $CellContext`Nfm, \
$CellContext`Tint, $CellContext`fbeat, $CellContext`Gt, $CellContext`Gr, \
$CellContext`ThermalNoise0, $CellContext`fleakage, \
$CellContext`ElectronicNoise0, $CellContext`NoiseL1, $CellContext`NoiseL2, \
$CellContext`NoiseThermal, $CellContext`EchoEnvelope, $CellContext`Echo1, \
$CellContext`Mask, $CellContext`SNRdB, $CellContext`fc, $CellContext`dfm}, \
$CellContext`dbc[
              Pattern[$CellContext`power, 
               Blank[]]] := 
            10 Log[10, $CellContext`power/10^(-3)]; $CellContext`zradar = 
            5000; $CellContext`Tm = 200 10^(-6); $CellContext`fm = 
            1/$CellContext`Tm; $CellContext`kBoltzmann = 
            1.38 10^(-23); $CellContext`TempElectronics = 290; $CellContext`c = 
            3. 10^8; $CellContext`fc = 
            94 10^9; $CellContext`\[Lambda] = $CellContext`c/$CellContext`fc; \
$CellContext`dr = $CellContext`c/(2 $CellContext`B); $CellContext`Nfm = 
            Ceiling[1.1 ($CellContext`zradar/$CellContext`dr)]; \
$CellContext`Tint = $CellContext`Tm; $CellContext`fbeat = \
($CellContext`r0/$CellContext`dr) $CellContext`fm; $CellContext`Gt = \
$CellContext`G; $CellContext`Gr = $CellContext`G; $CellContext`ThermalNoise0 = \
$CellContext`kBoltzmann $CellContext`TempElectronics; $CellContext`fleakage = 
            0.; $CellContext`dfm = $CellContext`fm/
             6; $CellContext`ElectronicNoise0 = 
            10^($CellContext`NoiseFigure/
               10) $CellContext`ThermalNoise0; $CellContext`NoiseL1 = 
            Table[($CellContext`Pt/$CellContext`Tint) 
              10^(-$CellContext`L1Loss/
                10) $CellContext`Sr[$CellContext`f, $CellContext`tr1], \
{$CellContext`f, $CellContext`dfm, $CellContext`Nfm $CellContext`fm, \
$CellContext`dfm}]; $CellContext`NoiseL2 = 
            Table[($CellContext`Pt/$CellContext`Tint) 
              10^(-$CellContext`L2Loss/
                10) $CellContext`Sr[$CellContext`f, $CellContext`tr2], \
{$CellContext`f, $CellContext`dfm, $CellContext`Nfm $CellContext`fm, \
$CellContext`dfm}]; $CellContext`NoiseThermal = 
            Table[(1/$CellContext`Tint) $CellContext`ElectronicNoise0, \
{$CellContext`f, $CellContext`dfm, $CellContext`Nfm $CellContext`fm, \
$CellContext`dfm}]; $CellContext`Mask = $CellContext`NoiseL1 + \
$CellContext`NoiseL2 + $CellContext`NoiseThermal; $CellContext`EchoEnvelope = 
            Table[$CellContext`Pt ($CellContext`\[Sigma] $CellContext`Gt \
$CellContext`Gr $CellContext`\[Lambda]^2/((4 
                 Pi)^3 ($CellContext`dr \
($CellContext`f/$CellContext`fm))^4)), {$CellContext`f, $CellContext`dfm, \
$CellContext`Nfm $CellContext`fm, $CellContext`dfm}]; $CellContext`Echo1 = 
            
            Table[$CellContext`Pt ($CellContext`\[Sigma] $CellContext`Gt \
$CellContext`Gr $CellContext`\[Lambda]^2/((4 
                 Pi)^3 $CellContext`r0^4)) \
$CellContext`gHamming[$CellContext`f, $CellContext`fbeat, \
$CellContext`Tint]^2, {$CellContext`f, $CellContext`dfm, $CellContext`Nfm \
$CellContext`fm, $CellContext`dfm}]; $CellContext`SNRdB = 
            10 Log[10, $CellContext`Pt ($CellContext`\[Sigma] $CellContext`Gt \
$CellContext`Gr $CellContext`\[Lambda]^2/((4 Pi)^3 $CellContext`r0^4))/
               Part[$CellContext`Mask, 
                Floor[$CellContext`fbeat/$CellContext`dfm]]]; ListLinePlot[
             
             Map[$CellContext`dbc, {$CellContext`NoiseL1, \
$CellContext`NoiseL2, $CellContext`NoiseThermal, $CellContext`EchoEnvelope, \
$CellContext`Echo1}], Axes -> False, Frame -> True, 
             FrameLabel -> {"frequency (MHz)", "dB re 1mW"}, 
             DataRange -> {$CellContext`dfm/
               10^6, $CellContext`Nfm ($CellContext`fm/10^6)}, 
             Joined -> {True, True, True, True, True}, PlotStyle -> {{
                Dashing[0.025], Red}, Red, Gray, {
                Dashing[0.05], Blue}, Blue}, 
             PlotRange -> {Automatic, {-170, -10}}, AspectRatio -> 1, 
             PlotLabel -> StringJoin["SNR = ", 
               ToString[$CellContext`SNRdB], " dB"], 
             ImageSize -> {200, 200}]], $CellContext`SweepPlot[
           Pattern[$CellContext`r, 
            Blank[]], 
           Pattern[$CellContext`B, 
            Blank[]]] := 
         Module[{$CellContext`Tm, $CellContext`c, $CellContext`\[Tau], \
$CellContext`fbeat}, $CellContext`Tm = 200 10^(-6); $CellContext`c = 
            3. 10^8; $CellContext`\[Tau] = 
            2 ($CellContext`r/$CellContext`c); $CellContext`fbeat = (
              1/$CellContext`Tm) ($CellContext`r/($CellContext`c/(
              2 $CellContext`B))); ListLinePlot[{
              Table[
              10^(-6) $CellContext`B (
                Mod[$CellContext`t, $CellContext`Tm]/$CellContext`Tm), \
{$CellContext`t, 0, 1.4 $CellContext`Tm, $CellContext`Tm/500}], 
              Table[
              10^(-6) $CellContext`B (
                
                Mod[$CellContext`t - $CellContext`\[Tau], \
$CellContext`Tm]/$CellContext`Tm), {$CellContext`t, 0, 
                1.4 $CellContext`Tm, $CellContext`Tm/500}]}, Joined -> True, 
             DataRange -> {0, 1.4}, Axes -> False, Frame -> True, FrameLabel -> {
               Row[{"time: ", 
                 Style["t", Italic], "/", 
                 "\!\(\*SubscriptBox[\(T\), \(m\)]\)"}], 
               "(frequency-\!\(\*SubscriptBox[\(f\), \
\(c\)]\))/\!\(\*SuperscriptBox[\(10\), \(6\)]\)"}, 
             PlotRange -> {Automatic, 1.1 $CellContext`B 10^(-6)}, 
             AspectRatio -> 1, PlotLabel -> 
             StringJoin["\!\(\*SubscriptBox[\(f\), \(beat\)]\) = ", 
               ToString[$CellContext`fbeat/10^6], " MHz"], 
             ImageSize -> {150, 150}]], $CellContext`DemonstrationPlots[
           Pattern[$CellContext`RadarDiagram, 
            Blank[]], 
           Pattern[$CellContext`r0, 
            Blank[]], 
           Pattern[$CellContext`\[Sigma], 
            Blank[]], 
           Pattern[$CellContext`Pt, 
            Blank[]], 
           Pattern[$CellContext`G, 
            Blank[]], 
           Pattern[$CellContext`L1Loss, 
            Blank[]], 
           Pattern[$CellContext`tr1ns, 
            Blank[]], 
           Pattern[$CellContext`L2Loss, 
            Blank[]], 
           Pattern[$CellContext`tr2ns, 
            Blank[]], 
           Pattern[$CellContext`NoiseFigure, 
            Blank[]], 
           Pattern[$CellContext`B, 
            Blank[]]] := 
         Module[{$CellContext`gleft, $CellContext`gbottom, \
$CellContext`gright}, $CellContext`gleft = 
            Show[$CellContext`RadarDiagram, 
              ImageSize -> {300, 
               150}]; $CellContext`gbottom = \
$CellContext`DesignComputationPlot[$CellContext`r0, $CellContext`\[Sigma], \
$CellContext`Pt, $CellContext`G, $CellContext`L1Loss, $CellContext`tr1ns 
              10^(-9), $CellContext`L2Loss, $CellContext`tr2ns 
              10^(-9), $CellContext`NoiseFigure, $CellContext`B]; \
$CellContext`gright = $CellContext`SweepPlot[$CellContext`r0, $CellContext`B]; 
           Grid[{{$CellContext`gleft, $CellContext`gright}, \
{$CellContext`gbottom, SpanFromAbove}}, Alignment -> {Center, Center}]]}]]; 
     Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellID->238456132],

Cell["\<\
Ordinary pulsed radar detects the range to a target by emitting a short pulse \
and observing the time of flight of the target echo. This requires the radar \
to have high instantaneous transmit power and often results in a radar with a \
large, expensive physical apparatus. Frequency-modulated continuous-wave \
(FMCW) radars achieve similar results using much smaller instantaneous \
transmit powers and physical size by continuously emitting periodic pulses \
whose frequency content varies with time. A very important type of FMCW radar \
pulse is the linear FM sweep. In this case, the range to the target is found \
by detecting the frequency difference between the received and emitted radar \
signals. The range to the target is proportional to this frequency \
difference, which is also referred to as the beat frequency. \
\>", "ManipulateCaption"],

Cell[TextData[{
 "This Demonstration investigates the performance of a W-band (94 GHz) FMCW \
radar emitting an FM sweep with a 5 kHz pulse repetition frequency. You can \
select the range to the target ",
 Cell[BoxData[
  FormBox["r", TraditionalForm]], "InlineMath"],
 ", the target radar cross section ",
 Cell[BoxData[
  FormBox["\[Sigma]", TraditionalForm]], "InlineMath"],
 ", the transmit power of the radar ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["P", "t"], TraditionalForm]], "InlineMath"],
 ", and the gain ",
 Cell[BoxData[
  FormBox["G", TraditionalForm]], "InlineMath"],
 " of the radar transmit-receive antenna. These four parameters, together \
with the radar wavelength ",
 Cell[BoxData[
  FormBox["\[Lambda]", TraditionalForm]], "InlineMath"],
 " (= 3.2 mm at 94 GHz), control the target echo power. "
}], "ManipulateCaption",
 CellID->410781500],

Cell[TextData[{
 "Like all types of radar, FMCW radar is limited by the effects of thermal \
noise in the radar electronic circuits. The radar noise factor NF controls \
the radar thermal noise floor relative to the theoretical lower limit set by \
black body radiation. Additionally, FMCW radars are limited by the effects of \
unwanted phase noise that propagates via parasitic paths within the radar \
electronics. Phase noise effects on radar performance in the Demonstration \
are controlled by the losses ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "1"], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "2"], TraditionalForm]], "InlineMath"],
 " on the two parasitic paths and the travel times ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["T", "1"], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["T", "2"], TraditionalForm]], "InlineMath"],
 " on these paths measured relative to the travel time of the reference \
signal from the signal generator to the radar mixer. The radar sweep width ",
 Cell[BoxData[
  FormBox["B", TraditionalForm]], "InlineMath"],
 " controls radar range resolution. Large sweep widths improve range \
resolution and increase the target beat frequency. This enables target \
detection at higher frequencies where phase noise is lower."
}], "ManipulateCaption",
 CellID->63149951],

Cell["THINGS TO TRY", "ManipulateCaption",
 CellFrame->{{0, 0}, {1, 0}},
 CellFrameColor->RGBColor[0.87, 0.87, 0.87],
 FontFamily->"Helvetica",
 FontSize->12,
 FontWeight->"Bold",
 FontColor->RGBColor[0.597406, 0, 0.0527047],
 CellTags->"ControlSuggestions"],

Cell[TextData[{
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Resize Images",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Resize Images",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   "\"Click inside an image to reveal its orange resize frame.\\nDrag any of \
the orange resize handles to resize the image.\"",
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Slider Zoom",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Slider Zoom",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   RowBox[{"\"Hold down the \"", 
     FrameBox[
     "Alt", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], 
     "\" key while moving a slider to make fine adjustments in the slider \
value.\\nHold \"", 
     FrameBox[
     "Ctrl", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" and/or \"", 
     FrameBox[
     "Shift", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" at the same time as \"", 
     FrameBox[
     "Alt", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" to make ever finer adjustments.\""}],
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Gamepad Controls",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Gamepad Controls",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   "\"Control this Demonstration with a gamepad or other\\nhuman interface \
device connected to your computer.\"",
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Automatic Animation",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox[
    "Automatic Animation",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   RowBox[{"\"Animate a slider in this Demonstration by clicking the\"", 
     AdjustmentBox[
      Cell[
       GraphicsData[
       "CompressedBitmap", 
        "eJzzTSzJSM1NLMlMTlRwL0osyMhMLlZwyy8CCjEzMjAwcIKwAgOI/R/IhBKc\n\
/4EAyGAG0f+nTZsGwgysIJIRKsWKLAXGIHFmEpUgLADxWUAkI24jZs+eTaEt\n\
IG+wQKRmzJgBlYf5lhEA30OqWA=="], "Graphics", ImageSize -> {9, 9}, ImageMargins -> 
       0, CellBaseline -> Baseline], BoxBaselineShift -> 0.1839080459770115, 
      BoxMargins -> {{0., 0.}, {-0.1839080459770115, 0.1839080459770115}}], 
     "\"button\\nnext to the slider, and then clicking the play button that \
appears.\\nAnimate all controls by selecting \"", 
     StyleBox["Autorun", FontWeight -> "Bold"], "\" from the\"", 
     AdjustmentBox[
      Cell[
       GraphicsData[
       "CompressedBitmap", 
        "eJyNULENwyAQfEySIlMwTVJlCGRFsosokeNtqBmDBagoaZjAI1C8/8GUUUC6\n\
57h7cQ8PvU7Pl17nUav7oj/TPH7V7b2QJAUAXBkKmCPRowxICy64bRvGGNF7\n\
X8CctGoDSN4xhIDGGDhzFXwUh3/ClBKrDQPmnGXtI6u0OOd+tZBVUqy1xSaH\n\
UqiK6pPe4XdEdAz6563tx/gejuORGMxJaz8mdpJn7hc="], "Graphics", 
       ImageSize -> {10, 10}, ImageMargins -> 0, CellBaseline -> Baseline], 
      BoxBaselineShift -> 0.1839080459770115, 
      BoxMargins -> {{0., 0.}, {-0.1839080459770115, 0.1839080459770115}}], 
     "\"menu.\""}],
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]]
}], "ManipulateCaption",
 CellMargins->{{Inherited, Inherited}, {0, 0}},
 Deployed->True,
 FontFamily->"Verdana",
 CellTags->"ControlSuggestions"],

Cell["DETAILS", "DetailsSection"],

Cell["\<\
In a frequency-modulated continuous-wave (FMCW) radar employing a linear \
upsweep in frequency, the radar signal generator produces a phase modulated \
signal of the form\
\>", "DetailNotes",
 CellID->1996861395],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["s", "1"], "(", "t", ")"}], "=", 
    RowBox[{
     SubscriptBox["a", "1"], "cos", " ", 
     RowBox[{"\[Phi]", "(", "t", ")"}]}]}], TraditionalForm]], "InlineMath"],
 ",",
 " "
}], "DetailNotes",
 CellID->375030449],

Cell["where", "DetailNotes",
 CellID->2064372561],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{"\[Phi]", "(", "t", ")"}], "=", 
   RowBox[{
    RowBox[{"2", "\[Pi]", " ", 
     SubscriptBox["f", "c"], "t"}], "+", 
    RowBox[{"\[Pi]", " ", 
     FractionBox["B", 
      SubscriptBox["T", "m"]], 
     SuperscriptBox["t", "2"]}]}]}], 
  TraditionalForm]], "InlineMath"]], "DetailNotes",
 CellID->1163831795],

Cell[TextData[{
 "is the signal phase at time ",
 Cell[BoxData[
  FormBox["t", TraditionalForm]], "InlineMath"],
 ", ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["f", "c"], TraditionalForm]], "InlineMath"],
 " is the radar carrier frequency, ",
 Cell[BoxData[
  FormBox["B", TraditionalForm]], "InlineMath"],
 " is the sweep width, and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["T", "m"], TraditionalForm]], "InlineMath"],
 " is the pulse repetition period. The instantaneous frequency of the \
transmitted signal is "
}], "DetailNotes",
 CellID->272252398],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"F", "(", "t", ")"}], "=", 
    RowBox[{
     RowBox[{
      FractionBox["1", 
       RowBox[{"2", "\[Pi]"}]], 
      FractionBox["d", 
       RowBox[{"d", " ", "t"}]], 
      RowBox[{"\[Phi]", "(", "t", ")"}]}], "=", 
     RowBox[{
      SubscriptBox["f", "c"], "+", 
      RowBox[{
       FractionBox["B", 
        SubscriptBox["T", "m"]], "t"}]}]}]}], TraditionalForm]], "InlineMath"],
 "."
}], "DetailNotes",
 CellID->1952407134],

Cell[TextData[{
 "In the course of the time interval ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    RowBox[{"0", ",", 
     SubscriptBox["T", "m"]}], ")"}], TraditionalForm]], "InlineMath"],
 " the radar frequency linearly varies between ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["f", "c"], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["f", "c"], "+", "B"}], TraditionalForm]], "InlineMath"],
 "."
}], "DetailNotes",
 CellID->1239921689],

Cell[TextData[{
 "In FMCW radar, the signal generated by the radar is split into two parts. A \
small portion is allowed to pass into the upper port of the radar mixer where \
it is used as a reference signal for detection of the echo signal. The other, \
larger portion of the generated signal passes out through the circulator and \
into the antenna. The circulator is a multiport, electronic device that only \
allows electrical signals to propagate in the clockwise direction. Use of a \
circulator allows the radar to share transmit and receive antennas. After \
exiting the antenna, this signal propagates into the air out to a target \
where it is reflected and returns to the antenna. It then goes back into the \
circulator, passes through in the preferred direction and into the horizontal \
port of the mixer. If the target is located at distance ",
 Cell[BoxData[
  FormBox["r", TraditionalForm]], "InlineMath"],
 " from the radar, then the echo signal that comes into the mixer can be \
written in the form"
}], "DetailNotes",
 CellID->535996836],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["s", "2"], "(", "t", ")"}], "=", 
    RowBox[{
     SubscriptBox["a", "2"], "cos", " ", 
     RowBox[{"\[Phi]", "(", 
      RowBox[{"t", "-", "\[Tau]"}], ")"}]}]}], TraditionalForm]], 
  "InlineMath"],
 ","
}], "DetailNotes",
 CellID->556574978],

Cell[TextData[{
 "where ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Tau]", "=", 
    RowBox[{"2", 
     RowBox[{"r", "/", "c"}]}]}], TraditionalForm]], "InlineMath"],
 " is the propagation delay of the echo, ",
 Cell[BoxData[
  FormBox["c", TraditionalForm]], "InlineMath"],
 " is the speed of light, and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["a", "2"], TraditionalForm]], "InlineMath"],
 " is a factor that accounts for propagation losses, target reflectivity, and \
a variety of radar performance parameters.",
 " "
}], "DetailNotes",
 CellID->948755829],

Cell["\<\
The mixer multiplies the echo signal and the reference signal that came into \
the upper port of the mixer. This process produces sum and difference \
frequencies. The sum frequencies are on the order of twice the radar carrier \
frequency. The radar electronic circuits cannot respond to signals at this \
frequency. Thus only the difference frequencies pass out of the mixer. The \
signal that comes out of the mixer can be written \
\>", "DetailNotes",
 CellID->105575525],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["s", "3"], "(", "t", ")"}], "=", 
    RowBox[{
     RowBox[{
      SubscriptBox["a", "3"], 
      RowBox[{"cos", "[", 
       RowBox[{
        RowBox[{"\[Phi]", "(", "t", ")"}], "-", 
        RowBox[{"\[Phi]", "(", 
         RowBox[{"t", "-", "\[Tau]"}], ")"}]}], "]"}]}], "=", 
     RowBox[{
      SubscriptBox["a", "3"], 
      RowBox[{"cos", "(", 
       RowBox[{
        RowBox[{"2", "\[Pi]", " ", 
         SubscriptBox["f", "beat"], "t"}], "+", 
        RowBox[{"2", "\[Pi]", " ", 
         SubscriptBox["f", "c"], "\[Tau]"}], "-", 
        RowBox[{
         FractionBox[
          RowBox[{"\[Pi]", " ", "B"}], 
          SubscriptBox["T", "m"]], 
         SuperscriptBox["\[Tau]", "2"]}]}], ")"}]}]}]}], TraditionalForm]], 
  "InlineMath"],
 ","
}], "DetailNotes",
 CellID->1838161926],

Cell["where", "DetailNotes",
 CellID->287744540],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   SubscriptBox["f", "beat"], "=", 
   RowBox[{
    FractionBox[
     RowBox[{"B", " ", "\[Tau]"}], 
     SubscriptBox["T", "m"]], "=", 
    RowBox[{
     SubscriptBox["f", "m"], 
     FractionBox["r", 
      RowBox[{"d", "\[InvisibleSpace]", "r"}]]}]}]}], 
  TraditionalForm]], "InlineMath"]], "DetailNotes",
 CellID->2136794383],

Cell[TextData[{
 "is the beat frequency of the target echo,",
 " ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["f", "m"], "=", 
    SubsuperscriptBox["T", "m", 
     RowBox[{"-", "1"}]]}], TraditionalForm]], "InlineMath"],
 " is the radar pulse modulation frequency, and ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"d", "\[InvisibleSpace]", "r"}], "=", 
    RowBox[{"c", "/", 
     RowBox[{"(", 
      RowBox[{"2", "B"}], ")"}]}]}], TraditionalForm]], "InlineMath"],
 " is the range resolution of the radar pulse."
}], "DetailNotes",
 CellID->734166300],

Cell[TextData[{
 "In FMCW radar applications, targets are found by performing Fourier \
analysis on the mixer output signal. Using a Fourier transform with a \
rectangular window of integration extending over a time interval of length ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["T", "m"], TraditionalForm]], "InlineMath"],
 " leads to the following representation of the power in the mixer output \
signal:"
}], "DetailNotes",
 CellID->1839452085],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["S", "echo"], "(", 
     RowBox[{"f", ",", 
      SubscriptBox["f", "beat"], ",", "B", ",", 
      SubscriptBox["T", "m"]}], ")"}], "=", 
    SuperscriptBox[
     RowBox[{
      SubscriptBox["P", "r"], "(", 
      FractionBox[
       RowBox[{"sin", "[", 
        RowBox[{"\[Pi]", " ", 
         RowBox[{"(", 
          RowBox[{"f", "-", 
           SubscriptBox["f", "beat"]}], ")"}], 
         SubscriptBox["T", "m"]}], "]"}], 
       RowBox[{"\[Pi]", " ", 
        RowBox[{"(", 
         RowBox[{"f", "-", 
          SubscriptBox["f", "beat"]}], ")"}], 
        SubscriptBox["T", "m"]}]], ")"}], "2"]}], TraditionalForm]], 
  "InlineMath"],
 "."
}], "DetailNotes",
 CellID->496610383],

Cell[TextData[{
 "Here ",
 Cell[BoxData[
  FormBox["f", TraditionalForm]], "InlineMath"],
 " is the analysis frequency and ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["P", "r"], "=", 
    SubsuperscriptBox["a", "3", "2"]}], TraditionalForm]], "InlineMath"],
 " is the power of the target echo. In terms of the standard radar equation, \
the target echo power ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["P", "r"], TraditionalForm]], "InlineMath"],
 " can be written"
}], "DetailNotes",
 CellID->1409254839],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["P", "r"], "=", 
    RowBox[{
     SubscriptBox["P", "t"], 
     FractionBox[
      RowBox[{"\[Sigma]", " ", 
       SubscriptBox["G", "t"], 
       SubscriptBox["G", "r"], 
       SuperscriptBox["\[Lambda]", "2"]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"4", "\[Pi]"}], ")"}], "3"], 
       SuperscriptBox["r", "4"]}]]}]}], TraditionalForm]], "InlineMath"],
 ","
}], "DetailNotes",
 CellID->748936310],

Cell[TextData[{
 "where ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["P", "t"], TraditionalForm]], "InlineMath"],
 " is the radar transmission power (W), ",
 Cell[BoxData[
  FormBox["\[Sigma]", TraditionalForm]], "InlineMath"],
 " is the radar cross section ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    SuperscriptBox[
     StyleBox["m",
      FontSlant->"Plain"], "2"]}], TraditionalForm]], "InlineMath"],
 "), ",
 Cell[BoxData[
  FormBox["\[Lambda]", TraditionalForm]], "InlineMath"],
 " is the wavelength of the radar (m), ",
 Cell[BoxData[
  FormBox["r", TraditionalForm]], "InlineMath"],
 " is the distance from the radar to the target (m), and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["G", "t"], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["G", "r"], TraditionalForm]], "InlineMath"],
 " are the gains on a power scale of the transmit and receive antennas. For a \
single antenna system these gains are equal (",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["G", "t"], "=", 
    RowBox[{
     SubscriptBox["G", "r"], "=", "G"}]}], TraditionalForm]], "InlineMath"],
 "). Gain is usually expressed on a decibel scale. The target echo path is \
illustrated by the solid blue line ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "3"], TraditionalForm]], "InlineMath"],
 " in the circuit diagram."
}], "DetailNotes",
 CellID->970907103],

Cell["\<\
It is more effective to perform the Fourier analysis of the mixer output \
signal with a nonuniform window of integration in order to reduce side lobes \
in the spectral response. For the case of a Hamming window of integration\
\>", "DetailNotes",
 CellID->1763318763],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["S", "echo"], "(", 
     RowBox[{"f", ",", 
      SubscriptBox["f", "beat"], ",", "B", ",", 
      SubscriptBox["T", "m"]}], ")"}], "=", 
    SuperscriptBox[
     RowBox[{
      SubscriptBox["P", "r"], "(", 
      RowBox[{
       FractionBox[
        RowBox[{"sin", "[", 
         RowBox[{"\[Pi]", " ", 
          RowBox[{"(", 
           RowBox[{"f", "-", 
            SubscriptBox["f", "beat"]}], ")"}], 
          SubscriptBox["T", "m"]}], "]"}], 
        RowBox[{"\[Pi]", " ", 
         RowBox[{"(", 
          RowBox[{"f", "-", 
           SubscriptBox["f", "beat"]}], ")"}], 
         SubscriptBox["T", "m"]}]], "+", 
       RowBox[{
        FractionBox["0.92", "1.08"], 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{"f", "-", 
            SubscriptBox["f", "beat"]}], ")"}], 
          SubscriptBox["T", "m"], 
          RowBox[{"sin", "[", 
           RowBox[{"\[Pi]", " ", 
            RowBox[{"(", 
             RowBox[{"f", "-", 
              SubscriptBox["f", "beat"]}], ")"}], 
            SubscriptBox["T", "m"]}], "]"}]}], 
         RowBox[{"\[Pi]", "[", 
          RowBox[{"1", "-", 
           RowBox[{
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"f", "-", 
               SubscriptBox["f", "beat"]}], ")"}], "2"], 
            SubsuperscriptBox["T", "m", "2"]}]}], "]"}]]}]}], ")"}], "2"]}], 
   TraditionalForm]], "InlineMath"],
 "."
}], "DetailNotes",
 CellID->824121141],

Cell["\<\
Use of the Hamming window of integration allows the detection of targets that \
are close together in range with large differences in echo level. Otherwise \
the 13 dB down side lobes (rectangular window) from the target with the \
larger echo will obscure the return from the target with the weaker echo.\
\>", "DetailNotes",
 CellID->1881977284],

Cell[TextData[{
 "The detection performance of the radar is limited by phase noise that \
propagates via the parasitic paths ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "1"], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "2"], TraditionalForm]], "InlineMath"],
 " illustrated as the dashed and solid red lines in the circuit diagram. The \
power spectrum of the phase noise of the signal generator is modeled via"
}], "DetailNotes",
 CellID->175724252],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["S", "gen"], "(", "f", ")"}], "=", 
    RowBox[{
     SuperscriptBox["10", "7.85"], 
     SuperscriptBox["f", 
      RowBox[{"-", "3.05"}]]}]}], TraditionalForm]], "InlineMath"],
 "."
}], "DetailNotes",
 CellID->607488739],

Cell["\<\
It is measured in units of W/Hz. If the phase noise effects are included in \
the representation for the FMCW beat signal out of the mixer, then \
\>", "DetailNotes",
 CellID->925978535],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["s", "3"], "(", "t", ")"}], "=", 
    RowBox[{
     SubscriptBox["a", "3"], 
     RowBox[{"cos", "[", 
      RowBox[{
       RowBox[{"\[Phi]", "(", "t", ")"}], "-", 
       RowBox[{"\[Phi]", "(", 
        RowBox[{"t", "-", 
         SubscriptBox["T", "p"]}], ")"}], "+", 
       RowBox[{"\[Delta]\[Phi]", "(", "t", ")"}], "-", 
       RowBox[{"\[Delta]\[Phi]", "(", 
        RowBox[{"t", "-", 
         SubscriptBox["T", "p"]}], ")"}]}], "]"}]}]}], TraditionalForm]], 
  "InlineMath"],
 ","
}], "DetailNotes",
 CellID->107247936],

Cell[TextData[{
 "where ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Delta]\[Phi]", "(", "t", ")"}], TraditionalForm]], "InlineMath"],
 " is the phase of the phase noise at time ",
 Cell[BoxData[
  FormBox["t", TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["T", "p"], TraditionalForm]], "InlineMath"],
 " is the difference in travel time on the parasitic path (",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "1"], TraditionalForm]], "InlineMath"],
 " or ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "2"], TraditionalForm]], "InlineMath"],
 ") and the travel time on the reference path that goes directly from the \
signal generator to the mixer. If the parasitic path and the reference path \
have the same travel times then the phase noise cancels."
}], "DetailNotes",
 CellID->777280519],

Cell["\<\
The frequency transfer function that models the process of obtaining a \
difference between two noise processes where one is a time-delayed version of \
the other is\
\>", "DetailNotes",
 CellID->610658132],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"H", "(", 
     RowBox[{"f", ",", 
      SubscriptBox["T", "p"]}], ")"}], "=", 
    RowBox[{"1", "-", 
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{"-", "j"}], " ", "2", "\[Pi]", " ", "f", " ", 
       SubscriptBox["T", "p"]}]]}]}], TraditionalForm]], "InlineMath"],
 "."
}], "DetailNotes",
 CellID->506875511],

Cell["The phase noise that limits radar performance is ", "DetailNotes",
 CellID->2136569375],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["S", "phase"], "(", 
     RowBox[{"f", ",", 
      RowBox[{
       SubscriptBox["T", "p"], 
       SubscriptBox["L", "p"]}]}], ")"}], "=", 
    RowBox[{
     RowBox[{
      SubscriptBox["P", "t"], 
      SuperscriptBox["10", 
       RowBox[{
        RowBox[{"-", 
         SubscriptBox["L", "p"]}], "/", "10"}]], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"|", 
         RowBox[{"H", "(", 
          RowBox[{"f", ",", 
           SubscriptBox["T", "p"]}], ")"}], "|"}], ")"}], "2"], 
      RowBox[{
       SubscriptBox["S", "gen"], "(", "f", ")"}]}], " ", "=", 
     RowBox[{
      SubscriptBox["P", "t"], 
      SuperscriptBox["10", 
       RowBox[{
        RowBox[{"-", 
         SubscriptBox["L", "p"]}], "/", "10"}]], 
      RowBox[{"2", "[", 
       RowBox[{"1", "-", 
        RowBox[{"cos", "(", 
         RowBox[{"2", " ", "\[Pi]", " ", "f", " ", 
          SubscriptBox["T", "p"]}], ")"}]}], "]"}], 
      RowBox[{
       SubscriptBox["S", "gen"], "(", "f", ")"}]}]}]}], TraditionalForm]], 
  "InlineMath"],
 ","
}], "DetailNotes",
 CellID->1247424138],

Cell[TextData[{
 "where ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["L", "p"], TraditionalForm]], "InlineMath"],
 " is the loss in signal power measured in decibels on the parasitic path \
relative to the reference path."
}], "DetailNotes",
 CellID->1722125352],

Cell[TextData[{
 "Radar performance is ultimately limited by thermal noise. If ",
 Cell[BoxData[
  FormBox["T", TraditionalForm]], "InlineMath"],
 " is the temperature of the radar electronics in degrees Kelvin (about 290 K \
at room temperature) and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["k", "B"], TraditionalForm]], "InlineMath"],
 " is Boltzmann's constant ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    RowBox[{"1.38", "\[Times]", 
     SuperscriptBox["10", 
      RowBox[{"-", "23"}]], " ", 
     StyleBox["J",
      FontSlant->"Plain"], " ", 
     SuperscriptBox[
      StyleBox["K",
       FontSlant->"Plain"], 
      RowBox[{"-", "1"}]]}]}], TraditionalForm]], "InlineMath"],
 "), then the power spectrum of thermal noise that limits radar performance is"
}], "DetailNotes",
 CellID->2123680445],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["S", "thermal"], "=", 
    RowBox[{
     SuperscriptBox["10", 
      RowBox[{"NF", "/", "10"}]], 
     SubscriptBox["k", 
      RowBox[{"B", " "}]], "T"}]}], TraditionalForm]], "InlineMath"],
 ","
}], "DetailNotes",
 CellID->1519025975],

Cell[TextData[{
 "where the noise factor ",
 Cell[BoxData[
  FormBox["NF", TraditionalForm]], "InlineMath"],
 " represents the decibel level increase of the radar thermal noise floor \
above the theoretical lower limit. An ideal (unobtainable) radar system would \
have a noise factor of 0 dB."
}], "DetailNotes",
 CellID->983966289],

Cell["\<\
The signal-to-noise ratio for the radar echo is the ratio of signal power to \
the sum of all limiting noise effects. For a FMCW radar with two parasitic \
phase noise paths as shown, it can be written in the form\
\>", "DetailNotes",
 CellID->1897337323],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{"SNR", "=", 
    FractionBox[
     RowBox[{
      RowBox[{
       SubscriptBox["S", "echo"], "(", 
       RowBox[{"f", ",", 
        SubscriptBox["f", "beat"], ",", "B", ",", 
        SubscriptBox["T", "m"]}], ")"}], " ", 
      SubscriptBox["T", "m"]}], 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        UnderoverscriptBox["\[Sum]", 
         RowBox[{"p", "=", "1"}], "2"], 
        RowBox[{
         SubscriptBox["S", "phase"], "(", 
         RowBox[{"f", ",", 
          SubscriptBox["T", 
           RowBox[{"p", " "}]], ",", 
          SubscriptBox["L", "p"]}], ")"}]}], " ", "+", " ", 
       SubscriptBox["S", "thermal"]}], ")"}]]}], TraditionalForm]], 
  "InlineMath"],
 "."
}], "DetailNotes",
 CellID->563713358],

Cell[TextData[{
 "Reliable detection of a radar echo requires signal to noise ratios in \
excess of 10. ",
 Cell[BoxData[
  FormBox["SNR", TraditionalForm]], "InlineMath"],
 " is usually expressed on a decibel scale. "
}], "DetailNotes",
 CellID->1780040989],

Cell["References", "DetailNotes",
 CellID->372397350],

Cell[TextData[{
 "[1] G. M. Brooker, \"Understanding Millimeter Wave FMCW Radars,\" in ",
 StyleBox["First International Conference on Sensing Technology",
  FontSlant->"Italic"],
 ", November 21\[Dash]23, 2005, Palmerston North, New Zealand (G. Sen Gupta, \
S. C. Mukhopadhyay, and C. H. Messom, eds.), pp. 152\[Dash]157."
}], "DetailNotes",
 CellID->292332230],

Cell[TextData[{
 "[2] I. V. Komarov and S. M. Smolskiy, ",
 StyleBox["Fundamentals of Short-Range FM Radar",
  FontSlant->"Italic"],
 ", Boston: Artech House, 2003."
}], "DetailNotes",
 CellID->1835456162],

Cell["RELATED LINKS", "RelatedLinksSection"],

Cell[TextData[{
 ButtonBox["High-Frequency Sonar Performance",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/HighFrequencySonarPerformance/"], 
    None},
  ButtonNote->
   "http://demonstrations.wolfram.com/HighFrequencySonarPerformance/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->121094435],

Cell[TextData[{
 ButtonBox["Bayesian Range Weighting for Sonar",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/BayesianRangeWeightingForSonar/"], 
    None},
  ButtonNote->
   "http://demonstrations.wolfram.com/BayesianRangeWeightingForSonar/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->149973362],

Cell[TextData[{
 ButtonBox["Uncertainty in Sonar Performance Prediction",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
UncertaintyInSonarPerformancePrediction/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/UncertaintyInSonarPerformancePrediction/\
"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->422655122],

Cell[BoxData[
 ButtonBox[
  PaneSelectorBox[{False->Cell[BoxData[
    GraphicsBox[RasterBox[CompressedData["
1:eJztmjlPZFcQhZH8H5z7LzmYFGkSyAwZkEEGZEDEkrBEbAEgAjCLxS6xCbEP
Bnu8SAYaRoIEhPDnPqJcvm/p17THyJp3pGnVPbdu1bl1T7dGo/nm/Xffvv+q
qqrqa/78wJ+/4ufn56enp4eHh7u7u9vb25ubm0KhcFNEoQiCT0Wwa4wA88kh
WCYhKS39eKAtCUFCSUmW4AN/96T6sY30qa1gUCk6NdiUK8Q+ij9riFZOR5YZ
xu5yFsNgG8zzXMTV1dX5+fmHHDnKBLbBPHLR2dnZW8vJ8X8F5pGLbJkjRxbI
LfZpv0U5crwauYuEpaWlmZkZPt9ayN/Y39+fKeKthZSGXPRj+WhoaHjnwHJi
YsJ2mUBra2t1dTVbzc3NvI5tQZKsWLu+Znd3t+LZ2Vl2fZBSFlIyCIaHhwnI
hEdSTU0Ny46ODl/cgyLKEYitbFI7JOkIu6ps12HLKtNOaXV1dTYcApaQlOWs
dEbBlkkik0t5wYgRjwCrAK9H4dOrKtmRg/39/cr0j5gdctG5A+R5BiCyra3t
+xcQo5PAdhkgd5+cnCRG/8HBATzL6iKISdYDaUunenp6FMe6yMpyWZVlJkxA
cyZTw9cAdZCnJNDYoy7iOGfZ3dzcZMkn70KLlHbwkLwL+lGrL4U0+wloi7Mw
UrW1tUUsQxKwRQJzi87WzhIzHJacYnRaqiNLElCi7uJVWfniM3bkOpxCIQl2
hRQEJom6KCP8iwvIQwwBYvw8dUHNpK0IlsvLywMDA3omzSeoqev7IKksdShi
VqS+BmiBQHIgGHBWlvZQl5RbeN538TySKO6Ho++al8QQ7O4GGvk6wWzlBLus
hoPUgMc2sUOI7Wh12GVEfpgZIRddFMHyIjMaGxvp6JmVlRWUbG9vS1JsMgqn
pqZYjoyMcFNI+Spa04pEg6Ds4eGhfrEp3viCqMKoYIBaZHtGXSBTbqFXFgmj
dgHvY19BRTyCHOaDH6I64YN2vlHA2zJLR4HnYIwEdO/s7IzNSYJ3UVmIfRTd
SPPncY3XD6ZsBi/z6MfTDy2Li6JlFVOcUvoq2QD9NHxyFBznp4MjtJCrU9pl
dJHePRiat7rMH+REfRLs6q0F5JmLPM/FbQglO14UXcor8NXWcwTfrJKQi34q
H0jq7e0NSBTOzc0RyM8iEQa/uroK09LSAkOO/tqpBJLJUU34o6Mj4sHBQSWQ
7DOjZXd2duDVF2iAqmDVVCQqGFDBfsrwj9VJauevCaipduItn5saTzKl0Ikk
ekkSoJ0G4uELxqqVP+2yVDZe8qgPaUMo2VFqAWkMgSNJ3ZMgF/1cPuQizzAl
LsIn8ejoKHFtba0eqL29HZIlvJIhqaC4vQjVJJ80/SaInJ+fJ1ZmbFlVUEHx
6nJ8fMxS8+STU4FguwhYW1uLbiW1I0aVYj268WRaQTX1d1E7SWKLINqXpvAp
k9dlNSi7rB8CxyUjY0cYLMS4KIUtU1onQS76WD5Q1dfX55muri502nJ3d3do
aIgcRIpZWFg4OTlRDEmCZSpHJGmcGhsbs1MMJ6WsMD093VcE+UbSjjqQnIoK
FijujxCTmd7OX8TEG28aiHUWbVGpbFkRD8hA0sfItOmoCjZDX1kD9LdI71g5
KnER5l94gb4Iwbj+FQQuejWSXATPN1HPwWd9fT3LyttVAr6P/GjoeyRbRn2V
jsBFnxty0S/lo6mp6Z0Dw8dCr6hTEnJR5XUQzGyj/Pr6OuLtIqSdnp5W3q5C
IFX/mKZ/LRwfHy/3OBf5TNqikIt+/eKxsbGxuLi4t7f31kL+gcUiMib/Z54B
QevcRTkqh1z0W44cFcBc9HuOHK+Cuejq6kqmSsnMnZYjgFxh/2P28fHx/v6+
UChcXl7+EQf46+vrpN0cXyAwA4bBNpgHC/0JwypFcA==
      "], {{0, 0}, {194, 22}}, {0, 255},
      ColorFunction->RGBColor],
     ImageSize->{194, 22},
     PlotRange->{{0, 194}, {0, 22}}]]], True->Cell[BoxData[
    GraphicsBox[RasterBox[CompressedData["
1:eJztmjtPLEcQhZH8H5z7LzlwinQjiCwIIYQQyICMRwRkPCIQCRgIeCa8At6S
vetrW2aXlTYAhD/PEeW63TOzu4wxsj1HYlVdXV11qvrMLEJ88+n7bz991dXV
9TU/m/z8ab+8vDw/Pz8+PjabzUaj8fDwUK/XHxLUE2A0XmEeofHPwnPLQhBQ
pFBqLT+crELaCgaVw9OOZGVOvRR/1hBnbtlpyxmm7nIWwSAbxPOSoFarVSqV
H0uU6BDIBvFIRR/NpcS/G6WKShRHqaISxVGqSNjd3d3Y2ODzo4n8hYuLi40E
H02kNaSinzrHwMDAdw4sV1dXbffy8nJkZKS7u5utoaGhvb0928JJsGzt+pxT
U1OyNzc32fVGTlqcooGxuLiIQSR+KPX09LAcHx/3yT1IohgB29JmlYOSjrCr
zNYOW5aZcgrr6+uz4WCwxElazopnDLaMEpE05QlDRn4IWAb8uhQ+PauWFTk4
NzenSH+J7UMqqjjgrLQBSI6Ojv7wCmx4YtguA6T3tbU1bPhfXV3hZ9mdAJtg
XZC2dGp6elp2qoosLc0qLTNhApozkRq+BqiDXCWGxh6riOOcZff4+Jgln9wL
JXLK4cfJvcAftnooxNlPQFucxTMxMcHWyckJtgSJwRYBzC2erZ3FZjgsOcXo
tFRFlgTARNXlV2bFy99mRdrhFAwJsBZyEIgkVlGb8DcuQA8yGJDx81SDmslo
Apb7+/vcvq5J8wlyqn1vZKUlD0lMiuTXAM0QCA4IAykwcKpKThfe76t4P5RI
7oejZ81TYgjWu4FCPk8wWynBmtVwoBr4kU3qEFIrWh52GZEfZpuQiqoJWFbb
xuDgIBW95+DgACanp6eilBoszbBcWloaHh7GKV3FOS1JbARpr6+v9cYm+eAr
YoYxYQBbaHuPquDM6UK3LCcelQv83vYZlMQjiNHrOuaJPyjnCwV+W7ZTUeA6
GCMG1Xl5psZkwauoI6ReijrS/Llc8+uFKZnhl3j08vRDa0dFcVrZJCeVHiUb
oJ+GD47B8fn5eY5QQqrOKdeminTvwdC81CX+ICbWSbCruxagZyryfhq3IbSs
WE1Uyi3waOs6gierJd5DRdUv9QwxPd14EHw1uSD92qkAgjVwtayL407jV1Bq
Wk3Ars8GSAbLpiSpKiKDvcr0vWNbqeWquSqyeDo1v34DhCeUqGXKpJwG4pGv
Ij2Jpk/K6RmUX/TIj9OG0LKi2ALCGAJHsqpnQSr6uXNAcmZmxnvOzs5ohE/s
5eVl3Ut/f7+ebpws8SsYJxlk60tNOYnXs2Ontra2sBWZmlYZlFB+Vbm5uWGp
efLJqYCwNQKYf7yVVQ4bVrLJaY3gJ9ISqqjvReVEiS099XFR/DmTV7MalDXr
h8Bx0WizIh4kxLhINTk5mVM6C1LR584hFXkPBHp7e22JnBYWFog5PDyUZ3t7
+/b2VjZOAixSMXISxik6slMMJyetsL6+PpOAeHNSjjw4ORUTFkjuj2ATmV/O
N2LkzW8csHUWbjFVtiyJB86A0udo2lRUBpuhz6wB+i7yKxZHERWNjY1tvwKb
3oNx/S0IVPRmZKkIP0+iroNPnmKWxcsVAc8jLw09R5JlrKt8BCp6b0hFv3QO
vagNDB8JvSFPS+zs7JC/eB4Iz87Oxv6joyN9YQmE3d3dFS9XEFDVH9P018KV
lZVOj9PIO3GL8WYV/ceAlpDr+fn5RxP5AjsJPppFa5QqKlEcUtGvJUoUgKno
txIl3gRTUa1Wk6hyIkullQggVdh/zD49PTWbzXq9fn9//3sa8BOctVvifwjE
gGCQDeJBQn8AEYN5ZQ==
      "], {{0, 0}, {194, 22}}, {0, 255},
      ColorFunction->RGBColor],
     ImageSize->{194, 22},
     PlotRange->{{0, 194}, {0, 22}}]]]}, Dynamic[
    CurrentValue["MouseOver"]]],
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/versions/source.jsp?id=\
FrequencyModulatedContinuousWaveFMCWRadar&version=0007"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/\
FrequencyModulatedContinuousWaveFMCWRadar-source.nb"]], \
"DemoSourceNotebookSection",
 CellFrame->{{0, 0}, {1, 1}},
 ShowCellBracket->False,
 CellMargins->{{48, 48}, {28, 28}},
 CellGroupingRules->{"SectionGrouping", 25},
 CellFrameMargins->{{48, 48}, {6, 8}},
 CellFrameColor->RGBColor[0.87, 0.87, 0.87]],

Cell["PERMANENT CITATION", "CitationSection"],

Cell[TextData[{
 "\"",
 ButtonBox["Frequency-Modulated Continuous-Wave (FMCW) Radar",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/"],
 "\"",
 " from ",
 ButtonBox["the Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 "\[ParagraphSeparator]\[NonBreakingSpace]",
 ButtonBox["http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
FrequencyModulatedContinuousWaveFMCWRadar/"]
}], "Citations"],

Cell[TextData[{
 "Contributed by: ",
 ButtonBox["Marshall Bradley",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/author.html?author=Marshall+\
Bradley"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/author.html?author=Marshall+Bradley"]
}], "Author",
 FontColor->GrayLevel[0.6]],

Cell[TextData[{
 "\[Copyright] ",
 StyleBox[ButtonBox["Wolfram Demonstrations Project & Contributors",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
  FontColor->GrayLevel[0.6]],
 "\[ThickSpace]\[ThickSpace]\[ThickSpace]|\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
 StyleBox[ButtonBox["Terms of Use",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/termsofuse.html"], None},
  ButtonNote->"http://demonstrations.wolfram.com/termsofuse.html"],
  FontColor->GrayLevel[0.6]]
}], "Text",
 CellFrame->{{0, 0}, {0, 0.5}},
 CellMargins->{{48, 48}, {20, 50}},
 CellFrameColor->GrayLevel[0.45098],
 FontFamily->"Verdana",
 FontSize->9,
 FontColor->GrayLevel[0.6],
 CellTags->"Copyright"]
},
Editable->False,
Saveable->False,
ScreenStyleEnvironment->"Working",
CellInsertionPointCell->None,
CellGrouping->Manual,
WindowSize->{750, 650},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowElements->{
 "StatusArea", "MemoryMonitor", "MagnificationPopUp", "VerticalScrollBar", 
  "MenuBar"},
WindowTitle->"Frequency-Modulated Continuous-Wave (FMCW) Radar",
DockedCells->{},
CellContext->Notebook,
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (February 23, 2011)",
StyleDefinitions->Notebook[{
   Cell[
    CellGroupData[{
      Cell[
      "Demonstration Styles", "Title", 
       CellChangeTimes -> {
        3.3509184553711*^9, {3.36928902713192*^9, 3.36928902738193*^9}, {
         3.3754479092466917`*^9, 3.3754479095123196`*^9}, {
         3.375558447161495*^9, 3.375558447395873*^9}, {3.37572892702972*^9, 
         3.375728927639103*^9}}], 
      Cell[
       StyleData[StyleDefinitions -> "Default.nb"]], 
      Cell[
       CellGroupData[{
         Cell[
         "Style Environment Names", "Section", 
          CellChangeTimes -> {{3.369277974278112*^9, 3.369277974396138*^9}}], 
         Cell[
          StyleData[All, "Working"], ShowCellBracket -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Notebook Options", "Section", 
          CellChangeTimes -> {{3.374865264950812*^9, 3.374865265419568*^9}}], 
         Cell[
         "  The options defined for the style below will be used at the \
Notebook level.  ", "Text"], 
         Cell[
          StyleData["Notebook"], Editable -> True, 
          PageHeaders -> {{None, None, None}, {None, None, None}}, 
          PageFooters -> {{None, None, None}, {None, None, None}}, 
          PageHeaderLines -> {False, False}, 
          PageFooterLines -> {False, False}, 
          PrintingOptions -> {
           "FacingPages" -> False, "FirstPageFooter" -> False, 
            "RestPagesFooter" -> False}, CellFrameLabelMargins -> 6, 
          DefaultNewInlineCellStyle -> "InlineMath", DefaultInlineFormatType -> 
          "DefaultTextInlineFormatType", ShowStringCharacters -> True, 
          CacheGraphics -> False, StyleMenuListing -> None, 
          DemonstrationSite`Private`CreateCellID -> True, 
          DemonstrationSite`Private`TrackCellChangeTimes -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Input/Output", "Section", 
          CellChangeTimes -> {{3.3756313297791014`*^9, 
           3.3756313299509783`*^9}}], 
         Cell[
         "The cells in this section define styles used for input and output \
to the kernel.  Be careful when modifying, renaming, or removing these \
styles, because the front end associates special meanings with these style \
names.    ", "Text"], 
         Cell[
          StyleData["Input"], CellMargins -> {{48, 4}, {6, 4}}], 
         Cell[
          StyleData["Output"], CellMargins -> {{48, 4}, {6, 4}}], 
         Cell[
          StyleData["DemonstrationHeader"], Deletable -> False, 
          CellFrame -> {{0, 0}, {0, 0}}, ShowCellBracket -> False, 
          CellMargins -> {{0, 0}, {30, 0}}, 
          CellGroupingRules -> {"SectionGrouping", 20}, 
          CellHorizontalScrolling -> True, 
          CellFrameMargins -> {{0, 0}, {0, 0}}, CellFrameColor -> 
          RGBColor[0, 0, 0], StyleMenuListing -> None, Background -> 
          RGBColor[0, 0, 0]], 
         Cell[
          StyleData["ShowSource"], CellFrame -> {{0, 0}, {0, 1}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
          CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
          RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
          "Helvetica", FontSize -> 10, FontWeight -> "Bold", FontSlant -> 
          "Plain", FontColor -> RGBColor[1, 0.42, 0]]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Basic Styles", "Section", 
          CellChangeTimes -> {{3.34971724802035*^9, 3.34971724966638*^9}, {
           3.35091840608065*^9, 3.35091840781999*^9}, {3.35091845122987*^9, 
           3.35091845356607*^9}, {3.35686681885432*^9, 3.35686681945788*^9}, {
           3.375657418186455*^9, 3.375657418452083*^9}}], 
         Cell[
          StyleData["Hyperlink"], StyleMenuListing -> None, FontColor -> 
          GrayLevel[0]], 
         Cell[
          StyleData["SiteLink"], StyleMenuListing -> None, 
          ButtonStyleMenuListing -> Automatic, FontColor -> 
          GrayLevel[0.45098], 
          ButtonBoxOptions -> {
           Active -> True, Appearance -> {Automatic, None}, 
            ButtonFunction :> (FrontEndExecute[{
               NotebookLocate[#2]}]& ), ButtonNote -> ButtonData}], 
         Cell[
          StyleData["Link"], FontColor -> GrayLevel[0.45098]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoNotes"], CellFrame -> True, 
             CellMargins -> {{0, 0}, {0, 0}}, 
             CellFrameMargins -> {{48, 48}, {4, 4}}, CellFrameColor -> 
             GrayLevel[0.99], StyleMenuListing -> None, FontFamily -> 
             "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.45098], 
             DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
            Cell[
             StyleData["DemoNotes", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, FontSize -> 9]}, Open]], 
         Cell[
          StyleData["SnapshotsSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, ShowGroupOpener -> True, 
          CellMargins -> {{48, 48}, {10, 30}}, 
          PrivateCellOptions -> {"DefaultCellGroupOpen" -> False}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], DefaultNewCellStyle -> 
          "SnapshotCaption", StyleMenuListing -> None, FontFamily -> 
          "Verdana", FontSize -> 12, FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData[
          "SnapshotCaption", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["SnapshotOutput"], ShowCellBracket -> False, 
             CellMargins -> {{48, 10}, {5, 7}}, Evaluatable -> True, 
             CellGroupingRules -> "InputGrouping", PageBreakWithin -> False, 
             GroupPageBreakWithin -> False, DefaultFormatType -> 
             DefaultInputFormatType, ShowAutoStyles -> True, 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, 
             AutoItalicWords -> {}, LanguageCategory -> "Mathematica", 
             FormatType -> InputForm, NumberMarks -> True, 
             LinebreakAdjustments -> {0.85, 2, 10, 0, 1}, CounterIncrements -> 
             "Input", MenuSortingValue -> 1500, MenuCommandKey -> "9", 
             DemonstrationSite`Private`StripStyleOnPaste -> True], 
            Cell[
             StyleData["SnapshotOuput", "Printout"], 
             CellMargins -> {{39, 0}, {4, 6}}, 
             LinebreakAdjustments -> {0.85, 2, 10, 1, 1}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoTitle"], Deletable -> False, ShowCellBracket -> 
             False, CellMargins -> {{48, 48}, {22, 10}}, 
             CellGroupingRules -> {"SectionGrouping", 20}, StyleMenuListing -> 
             None, FontFamily -> "Verdana", FontSize -> 20, FontWeight -> 
             "Bold", FontColor -> RGBColor[0.597406, 0, 0.0527047], 
             Background -> GrayLevel[1]], 
            Cell[
             StyleData["DemoName", "Printout"], 
             CellMargins -> {{24, 8}, {8, 27}}, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, FontSize -> 
             16]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DetailsSection"], CellFrame -> {{0, 0}, {1, 0}}, 
             ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
             CellGroupingRules -> {"SectionGrouping", 25}, 
             CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
             RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
             "Helvetica", FontSize -> 12, FontWeight -> "Bold", FontColor -> 
             RGBColor[0.597406, 0, 0.0527047]], 
            Cell[
             StyleData["DetailsSection", "Printout"], 
             CellMargins -> {{12, 0}, {0, 16}}, PageBreakBelow -> False, 
             FontSize -> 12]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoSection"], CellFrame -> {{0, 0}, {1, 0}}, 
             ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
             CellGroupingRules -> {"SectionGrouping", 30}, 
             CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
             RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
             "Helvetica", FontSize -> 12, FontWeight -> "Bold", FontColor -> 
             RGBColor[0.597406, 0, 0.0527047]], 
            Cell[
             StyleData["DemoSection", "Printout"], 
             CellMargins -> {{12, 0}, {0, 16}}, PageBreakBelow -> False, 
             FontSize -> 12]}, Open]], 
         Cell[
          StyleData["ManipulateSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12], 
         Cell[
          StyleData["ManipulateCaptionSection"], 
          CellFrame -> {{0, 0}, {0, 2}}, ShowCellBracket -> False, 
          CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], DefaultNewCellStyle -> 
          "ManipulateCaption", StyleMenuListing -> None, FontFamily -> 
          "Verdana", FontSize -> 12, FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData["ManipulateCaption"], ShowCellBracket -> False, 
          CellMargins -> {{48, 48}, {10, 16}}, StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12, FontColor -> GrayLevel[0], 
          DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
         Cell[
          StyleData[
          "SeeAlsoSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, DefaultNewCellStyle -> "SeeAlso"], 
         Cell[
          StyleData["SeeAlso", StyleDefinitions -> StyleData["DemoNotes"]], 
          CellDingbat -> 
          Cell["\[FilledSmallSquare]", FontColor -> 
            RGBColor[0.928786, 0.43122, 0.104662]], ShowCellBracket -> False, 
          FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData[
          "RelatedLinksSection", StyleDefinitions -> 
           StyleData["DemoSection"]], ShowCellBracket -> False, 
          DefaultNewCellStyle -> "RelatedLinks"], 
         Cell[
          StyleData[
          "RelatedLinks", StyleDefinitions -> StyleData["DemoNotes"], 
           FontColor -> RGBColor[0.928786, 0.43122, 0.104662]], 
          ShowCellBracket -> False, FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData[
          "CategoriesSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, DefaultNewCellStyle -> "Categories"], 
         Cell[
          StyleData["Categories", StyleDefinitions -> StyleData["DemoNotes"]],
           ShowCellBracket -> False], 
         Cell[
          StyleData[
          "AuthorSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {4, 18}}, 
          CellElementSpacings -> {"CellMinHeight" -> 3}, 
          CellFrameMargins -> {{48, 48}, {6, 3}}, DefaultNewCellStyle -> 
          "Author", FontSize -> 1, FontColor -> GrayLevel[1]], 
         Cell[
          StyleData[
          "Author", StyleDefinitions -> StyleData["DemoNotes"], FontColor -> 
           GrayLevel[0.64]], ShowCellBracket -> False], 
         Cell[
          StyleData[
          "DetailNotes", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False, FontColor -> GrayLevel[0]], 
         Cell[
          StyleData[
          "CitationSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 14}}, 
          DefaultNewCellStyle -> "Categories"], 
         Cell[
          StyleData["Citations", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False, ParagraphSpacing -> {0, 6}], 
         Cell[
          StyleData[
          "RevisionSection", StyleDefinitions -> StyleData["DemoSection"]], 
          DefaultNewCellStyle -> "RevisionNotes"], 
         Cell[
          StyleData[
          "RevisionNotes", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Specific Styles", "Section", 
          CellChangeTimes -> {{3.34971724802035*^9, 3.34971724966638*^9}, {
           3.35091840608065*^9, 3.35091840781999*^9}, {3.35091845122987*^9, 
           3.35091845356607*^9}, {3.36230868322317*^9, 3.36230868335672*^9}, {
           3.36928857618576*^9, 3.36928857640452*^9}, {3.3737586217185173`*^9,
            3.373758622077897*^9}}], 
         Cell[
          StyleData["InitializationSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12, FontColor -> 
          GrayLevel[0.45098]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["AnchorBar"], ShowCellBracket -> False, 
             CellMargins -> {{48, 44}, {3, 6}}, StyleMenuListing -> None, 
             FontFamily -> "Verdana", FontSize -> 9, FontColor -> 
             GrayLevel[0.5]], 
            Cell[
             StyleData["AnchorBar", "Presentation"], FontSize -> 18], 
            Cell[
             StyleData["AnchorBar", "SlideShow"], StyleMenuListing -> None], 
            Cell[
             StyleData["AnchorBar", "Printout"], FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["AnchorLink"], StyleMenuListing -> None, 
             ButtonStyleMenuListing -> Automatic, FontColor -> 
             RGBColor[0.5, 0.5, 0.5], 
             ButtonBoxOptions -> {
              Active -> True, ButtonFunction :> (FrontEndExecute[{
                  FrontEnd`NotebookLocate[#2]}]& ), ButtonNote -> 
               ButtonData}], 
            Cell[
             StyleData["AnchorLink", "Printout"], 
             FontVariations -> {"Underline" -> False}, FontColor -> 
             GrayLevel[0]]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["GamePadStatus"], ShowCellBracket -> False, 
             CellMargins -> {{48, 48}, {5, 5}}, StyleMenuListing -> None, 
             FontFamily -> "Verdana", FontSize -> 10], 
            Cell[
             StyleData["GamePadStatus", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoInstruction"], CellMargins -> {{48, 48}, {5, 5}}, 
             CellFrameLabelMargins -> 2, MenuSortingValue -> 800, 
             MenuCommandKey -> "8", StyleMenuListing -> None, FontFamily -> 
             "Verdana", FontSize -> 11, Background -> RGBColor[1, 0.85, 0.5], 
             DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
            Cell[
             StyleData["DemoInstruction", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, Hyphenation -> True, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, 
             LineSpacing -> {1., 2, 2.}, FontSize -> 9]}, Open]], 
         Cell[
          StyleData[
          "ImplementationSection", StyleDefinitions -> 
           StyleData["DemoSection"]], Deletable -> True, DefaultNewCellStyle -> 
          "ImplementationNotes"], 
         Cell[
          StyleData[
          "ImplementationNotes", StyleDefinitions -> StyleData["DemoNotes"]]], 
         Cell[
          StyleData[
          "StatusSection", StyleDefinitions -> StyleData["DemoSection"]], 
          DefaultNewCellStyle -> "StatusNotes"], 
         Cell[
          StyleData[
          "StatusNotes", StyleDefinitions -> StyleData["DemoNotes"]]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["SectionGloss"], StyleMenuListing -> None, FontSize -> 
             0.85 Inherited, FontWeight -> "Plain", FontColor -> 
             GrayLevel[0.6]], 
            Cell[
             StyleData["SectionGloss", "Printout"]]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineFormula"], 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, LanguageCategory -> 
             "Formula", AutoSpacing -> True, ScriptLevel -> 1, 
             AutoMultiplicationSymbol -> False, SingleLetterItalics -> False, 
             SpanMaxSize -> 1, StyleMenuListing -> None, FontFamily -> 
             "Courier", FontSize -> 1.05 Inherited, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             FractionBoxOptions -> {BaseStyle -> {SpanMaxSize -> Automatic}}, 
             GridBoxOptions -> {
              GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{1.}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["InlineFormula", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineOutput"], CellHorizontalScrolling -> True, 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, LanguageCategory -> 
             None, AutoMultiplicationSymbol -> False, StyleMenuListing -> 
             None, FontFamily -> "Courier", FontSize -> 1.05 Inherited], 
            Cell[
             StyleData["InlineOutput", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineMath"], DefaultFormatType -> 
             "DefaultTextFormatType", DefaultInlineFormatType -> 
             "TraditionalForm", LanguageCategory -> "Formula", AutoSpacing -> 
             True, ScriptLevel -> 1, AutoMultiplicationSymbol -> False, 
             SingleLetterItalics -> True, SpanMaxSize -> DirectedInfinity[1], 
             StyleMenuListing -> None, FontFamily -> "Times", FontSize -> 
             1.05 Inherited, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             GridBoxOptions -> {
              GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{1.}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["InlineMath", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["TableBase"], CellMargins -> {{48, 48}, {4, 4}}, 
             SpanMaxSize -> 1, StyleMenuListing -> None, FontFamily -> 
             "Courier", FontSize -> 11, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             GridBoxOptions -> {
              GridBoxAlignment -> {
                "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{Baseline}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["TableBase", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}, FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "1ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.04], {
                    Scaled[0.966]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.126], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "1ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.078], {
                    Scaled[0.922]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "2ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.05], 
                   Scaled[0.41], {
                    Scaled[0.565]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.14], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "2ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.079], 
                   Scaled[0.363], {
                    Scaled[0.558]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "3ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.04], 
                   Scaled[0.266], 
                   Scaled[0.26], {
                    Scaled[0.44]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
                 "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.14], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "3ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.08], 
                   Scaled[0.25], 
                   Scaled[0.25], {
                    Scaled[0.42]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
                 "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["TableText"], Deletable -> False, StyleMenuListing -> 
             None, FontFamily -> "Verdana", FontSize -> 0.952 Inherited], 
            Cell[
             StyleData["TableText", "Printout"], 
             CellMargins -> {{24, 0}, {0, 8}}, Hyphenation -> True, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, 
             LineSpacing -> {1., 2, 2.}]}, Open]], 
         Cell[
          StyleData["Continuation"], FontColor -> GrayLevel[1]]}, Closed]]}, 
     Open]]}, Visible -> False, FrontEndVersion -> 
  "8.0 for Microsoft Windows (32-bit) (February 23, 2011)", StyleDefinitions -> 
  "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ControlSuggestions"->{
  Cell[97229, 1770, 258, 7, 70, "ManipulateCaption",
   CellTags->"ControlSuggestions"],
  Cell[97490, 1779, 4440, 96, 70, "ManipulateCaption",
   CellTags->"ControlSuggestions"]},
 "Copyright"->{
  Cell[130556, 2778, 818, 23, 70, "Text",
   CellTags->"Copyright"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ControlSuggestions", 157258, 3347},
 {"Copyright", 157455, 3352}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[1297, 31, 34328, 567, 70, "DemonstrationHeader"],
Cell[35628, 600, 69, 0, 70, "DemoTitle"],
Cell[35700, 602, 58380, 1095, 70, "Output",
 CellID->238456132],
Cell[94083, 1699, 869, 12, 70, "ManipulateCaption"],
Cell[94955, 1713, 868, 22, 70, "ManipulateCaption",
 CellID->410781500],
Cell[95826, 1737, 1400, 31, 70, "ManipulateCaption",
 CellID->63149951],
Cell[97229, 1770, 258, 7, 70, "ManipulateCaption",
 CellTags->"ControlSuggestions"],
Cell[97490, 1779, 4440, 96, 70, "ManipulateCaption",
 CellTags->"ControlSuggestions"],
Cell[101933, 1877, 33, 0, 70, "DetailsSection"],
Cell[101969, 1879, 223, 5, 70, "DetailNotes",
 CellID->1996861395],
Cell[102195, 1886, 301, 12, 70, "DetailNotes",
 CellID->375030449],
Cell[102499, 1900, 49, 1, 70, "DetailNotes",
 CellID->2064372561],
Cell[102551, 1903, 371, 12, 70, "DetailNotes",
 CellID->1163831795],
Cell[102925, 1917, 560, 18, 70, "DetailNotes",
 CellID->272252398],
Cell[103488, 1937, 499, 19, 70, "DetailNotes",
 CellID->1952407134],
Cell[103990, 1958, 497, 18, 70, "DetailNotes",
 CellID->1239921689],
Cell[104490, 1978, 1059, 17, 70, "DetailNotes",
 CellID->535996836],
Cell[105552, 1997, 330, 13, 70, "DetailNotes",
 CellID->556574978],
Cell[105885, 2012, 562, 18, 70, "DetailNotes",
 CellID->948755829],
Cell[106450, 2032, 485, 8, 70, "DetailNotes",
 CellID->105575525],
Cell[106938, 2042, 877, 30, 70, "DetailNotes",
 CellID->1838161926],
Cell[107818, 2074, 48, 1, 70, "DetailNotes",
 CellID->287744540],
Cell[107869, 2077, 379, 13, 70, "DetailNotes",
 CellID->2136794383],
Cell[108251, 2092, 574, 19, 70, "DetailNotes",
 CellID->734166300],
Cell[108828, 2113, 449, 10, 70, "DetailNotes",
 CellID->1839452085],
Cell[109280, 2125, 771, 27, 70, "DetailNotes",
 CellID->496610383],
Cell[110054, 2154, 521, 17, 70, "DetailNotes",
 CellID->1409254839],
Cell[110578, 2173, 513, 19, 70, "DetailNotes",
 CellID->748936310],
Cell[111094, 2194, 1400, 44, 70, "DetailNotes",
 CellID->970907103],
Cell[112497, 2240, 279, 5, 70, "DetailNotes",
 CellID->1763318763],
Cell[112779, 2247, 1547, 50, 70, "DetailNotes",
 CellID->824121141],
Cell[114329, 2299, 357, 6, 70, "DetailNotes",
 CellID->1881977284],
Cell[114689, 2307, 503, 13, 70, "DetailNotes",
 CellID->175724252],
Cell[115195, 2322, 307, 12, 70, "DetailNotes",
 CellID->607488739],
Cell[115505, 2336, 196, 4, 70, "DetailNotes",
 CellID->925978535],
Cell[115704, 2342, 614, 21, 70, "DetailNotes",
 CellID->107247936],
Cell[116321, 2365, 835, 24, 70, "DetailNotes",
 CellID->777280519],
Cell[117159, 2391, 216, 5, 70, "DetailNotes",
 CellID->610658132],
Cell[117378, 2398, 387, 14, 70, "DetailNotes",
 CellID->506875511],
Cell[117768, 2414, 93, 1, 70, "DetailNotes",
 CellID->2136569375],
Cell[117864, 2417, 1162, 41, 70, "DetailNotes",
 CellID->1247424138],
Cell[119029, 2460, 262, 8, 70, "DetailNotes",
 CellID->1722125352],
Cell[119294, 2470, 814, 24, 70, "DetailNotes",
 CellID->2123680445],
Cell[120111, 2496, 307, 12, 70, "DetailNotes",
 CellID->1519025975],
Cell[120421, 2510, 333, 8, 70, "DetailNotes",
 CellID->983966289],
Cell[120757, 2520, 265, 5, 70, "DetailNotes",
 CellID->1897337323],
Cell[121025, 2527, 783, 27, 70, "DetailNotes",
 CellID->563713358],
Cell[121811, 2556, 258, 7, 70, "DetailNotes",
 CellID->1780040989],
Cell[122072, 2565, 53, 1, 70, "DetailNotes",
 CellID->372397350],
Cell[122128, 2568, 362, 7, 70, "DetailNotes",
 CellID->292332230],
Cell[122493, 2577, 205, 6, 70, "DetailNotes",
 CellID->1835456162],
Cell[122701, 2585, 44, 0, 70, "RelatedLinksSection"],
Cell[122748, 2587, 525, 16, 70, "RelatedLinks",
 CellID->121094435],
Cell[123276, 2605, 529, 16, 70, "RelatedLinks",
 CellID->149973362],
Cell[123808, 2623, 555, 17, 70, "RelatedLinks",
 CellID->422655122],
Cell[124366, 2642, 4866, 91, 70, "DemoSourceNotebookSection",
 CellGroupingRules->{"SectionGrouping", 25}],
Cell[129235, 2735, 45, 0, 70, "CitationSection"],
Cell[129283, 2737, 934, 27, 70, "Citations"],
Cell[130220, 2766, 333, 10, 70, "Author"],
Cell[130556, 2778, 818, 23, 70, "Text",
 CellTags->"Copyright"]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature hQE@u#FnYn#CqxmReSpKSDWM *)
