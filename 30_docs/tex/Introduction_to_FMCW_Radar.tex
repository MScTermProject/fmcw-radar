% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue
}

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!
\setlength{\parindent}{0pt} % Remove all para indents
%%% END Article customizations

%%% The "real" document content comes below...

\title{Introduction to FMCW Radar}
\author{Avinash Gokhale}
\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage

\lfoot{\ \\[-15pt]\hrule\ \\FMCW Radar \hfill Avinash Gokhale}

\section{Introduction}
Ordinary pulsed radar detects the range to a target by emitting a short pulse and observing the time of flight of the target echo. This requires the radar to have high instantaneous transmit power and often results in a radar with a large, expensive physical apparatus. Frequency-modulated continuous-wave (FMCW) radars achieve similar results using much smaller instantaneous transmit powers and physical size by continuously emitting periodic pulses whose frequency content varies with time. A very important type of FMCW radar pulse is the linear FM sweep. In this case, the range to the target is found by detecting the frequency difference between the received and emitted radar signals. The range to the target is proportional to this frequency difference, which is also referred to as the beat frequency.\\

This \href{http://demonstrations.wolfram.com/FrequencyModulatedContinuousWaveFMCWRadar}{demonstration} investigates the performance of a W-band (94 GHz) FMCW radar emitting an FM sweep with a 5 kHz pulse repetition frequency. You can select the range to the target $r$, the target radar cross section $\sigma$, the transmit power of the radar $P_t$, and the gain $G$ of the radar transmit-receive antenna. These four parameters, together with the radar wavelength $\lambda$ (= 3.2 mm at 94 GHz), control the target echo power.\\

Like all types of radar, FMCW radar is limited by the effects of thermal noise in the radar electronic circuits. The radar noise factor $NF$ controls the radar thermal noise floor relative to the theoretical lower limit set by black body radiation. Additionally, FMCW radars are limited by the effects of unwanted phase noise that propagates via parasitic paths within the radar electronics. Phase noise effects on radar performance in the Demonstration are controlled by the losses $L_1$ and $L_2$ on the two parasitic paths and the travel times $T_1$ and $T_2$ on these paths measured relative to the travel time of the reference signal from the signal generator to the radar mixer. The radar sweep width $B$ controls radar range resolution. Large sweep widths improve range resolution and increase the target beat frequency. This enables target detection at higher frequencies where phase noise is lower.

\section{Details}
In a frequency-modulated continuous-wave (FMCW) radar employing a linear upsweep in frequency, the radar signal generator produces a phase modulated signal of the form

\[s_1(t) = a_1 cos\phi(t)\]

where

\[\phi(t) =2\pi f_c t+ \pi \frac{B}{T_m}t^2\]

is the signal phase at time $t$,  $f_c$ is the radar carrier frequency, $B$ is the sweep width, and $T_m$ is the pulse repetition period. The instantaneous frequency of the transmitted signal is

\[F(t) = \frac{1}{2\pi} \frac{d}{dt} \phi(t) = f_c + \frac{B}{T_m} t\]

In the course of the time interval $(0, T_m)$  the radar frequency linearly varies between $f_c$ and $f_c + B$.\\

In FMCW radar, the signal generated by the radar is split into two parts. A small portion is allowed to pass into the upper port of the radar mixer where it is used as a reference signal for detection of the echo signal. The other, larger portion of the generated signal passes out through the circulator and into the antenna. The circulator is a multiport, electronic device that only allows electrical signals to propagate in the clockwise direction. Use of a circulator allows the radar to share transmit and receive antennas. After exiting the antenna, this signal propagates into the air out to a target where it is reflected and returns to the antenna. It then goes back into the circulator, passes through in the preferred direction and into the horizontal port of the mixer. If the target is located at distance $r$ from the radar, then the echo signal that comes into the mixer can be written in the form

\[r_2(t) = a_2 cos\phi(t - \tau)\]

where $\tau = \frac{2r}{c}$ is the propagation delay of the echo, $c$ is the speed of light, and $a_2$ is a factor that accounts for propagation losses, target reflectivity, and a variety of radar performance parameters.\\

The mixer multiplies the echo signal and the reference signal that came into the upper port of the mixer. This process produces sum and difference frequencies. The sum frequencies are on the order of twice the radar carrier frequency. The radar electronic circuits cannot respond to signals at this frequency. Thus only the difference frequencies pass out of the mixer. The signal that comes out of the mixer can be written

\[s_3(t) = a_3 cos[\phi(t) - \phi(t - \tau)] = a_3 cos\bigg(2\pi f_{beat} + 2\pi f_c \tau - \frac{\pi B}{T_m}\tau^2\bigg)\]

where

\[f_{beat} = \frac{B\tau}{T_m} = f_m \frac{r}{dr}\]

is the beat frequency of the target echo, $f_m = \frac{1}{T_m}$ is the radar pulse modulation frequency, and $dr = \frac{c}{2B}$ is the range resolution of the radar pulse.\\

In FMCW radar applications, targets are found by performing Fourier analysis on the mixer output signal. Using a Fourier transform with a rectangular window of integration extending over a time interval of length $T_m$ leads to the following representation of the power in the mixer output signal:

\[S_{echo}(f, f_{beat}, B, T_m) = P_r \bigg(\frac{sin[\pi (f - f_{beat} T_m)]}{\pi (f - f_{beat}T_m)}\bigg)^2\]

Here $f$ is the analysis frequency and $P_r = {a_3}^2$ is the power of the target echo. In terms of the standard radar equation, the target echo power $P_r$ can be written

\[P_r = P_t \frac{\sigma G_t G_\tau \lambda^2}{4\pi^3 r^4}\]

where $P_t$ is the radar transmission power (W), $\sigma$ is the radar cross section ($m^2$), $\lambda$ is the wavelength of the radar (m), $r$ is the distance from the radar to the target (m), and $G_t$ and $G_\tau$ are the gains on a power scale of the transmit and receive antennas. For a single antenna system these gains are equal ($G_t = G_\tau = G$). Gain is usually expressed on a decibel scale. The target echo path is illustrated by the solid blue line $L_3$ in the circuit diagram.\\

It is more effective to perform the Fourier analysis of the mixer output signal with a nonuniform window of integration in order to reduce side lobes in the spectral response. For the case of a Hamming window of integration

\[S_{echo}(f, f_{beat}, B, T_m) = P_r \bigg(\frac{sin[\pi (f - f_{beat} T_m)]}{\pi (f - f_{beat}T_m)} + \frac{0.92}{1.08}\frac{(f - f_{beat})sin[\pi (f - f_{beat} T_m)]}{\pi [1 - (f - f_{beat})^2{T_m}^2]}\bigg)^2\]

Use of the Hamming window of integration allows the detection of targets that are close together in range with large differences in echo level. Otherwise the 13 dB down side lobes (rectangular window) from the target with the larger echo will obscure the return from the target with the weaker echo.\\

The detection performance of the radar is limited by phase noise that propagates via the parasitic paths $L_1$ and $L_2$ illustrated as the dashed and solid red lines in the circuit diagram. The power spectrum of the phase noise of the signal generator is modeled via

\[S_{gen}(f) = 10^{7.85} f^{-3.05}\]

It is measured in units of W/Hz. If the phase noise effects are included in the representation for the FMCW beat signal out of the mixer, then

\[s_3(t) = a_3 cos[\phi(t) - \phi(t-T_p) + \delta \phi(t) - \delta \phi(t - T_p)]\]

where $\delta \phi(t)$ is the phase of the phase noise at time $t$ and $T_p$ is the difference in travel time on the parasitic path ($L_1$ or $L_2$) and the travel time on the reference path that goes directly from the signal generator to the mixer. If the parasitic path and the reference path have the same travel times then the phase noise cancels.\\

The frequency transfer function that models the process of obtaining a difference between two noise processes where one is a time-delayed version of the other is

\[H(f, T_p) = 1 - e^{-j2\pi f T_p}\]

The phase noise that limits radar performance is

\[S_{phase}(f, T_p, L_p) = P_t 10^{-L_p/10} |H(f, T_p)|^2 S_{gen}(f)\]
i.e.
\[S_{phase}(f, T_p, L_p) = P_t 10^{-L_p/10}2[1 - cos(2\pi f T_p)] S_{gen}(f)\]

where $L_p$ is the loss in signal power measured in decibels on the parasitic path relative to the reference path.\\

Radar performance is ultimately limited by thermal noise. If $T$ is the temperature of the radar electronics in degrees Kelvin (about 290 K at room temperature) and $k_B$ is Boltzmann's constant $1.38e^{-23}JK^{-1}$), then the power spectrum of thermal noise that limits radar performance is

\[S_{thermal} = 10^{NF/10}k_B T\]

where the noise factor $NF$ represents the decibel level increase of the radar thermal noise floor above the theoretical lower limit. An ideal (unobtainable) radar system would have a noise factor of 0 dB.\\

The signal-to-noise ratio for the radar echo is the ratio of signal power to the sum of all limiting noise effects. For a FMCW radar with two parasitic phase noise paths as shown, it can be written in the form

\[SNR = \frac{S_{echo}(f, f_{beat}, B, T_m) T_m}{\sum_{p=1}^{2} S_{phase}(f, T_p, L_p) + S_{thermal}}\]

Reliable detection of a radar echo requires signal to noise ratios in excess of 10. $SNR$ is usually expressed on a decibel scale.

\section{Original Attribution}
Marshal Bradley\footnote{\href{http://demonstrations.wolfram.com/FrequencyModulatedContinuousWaveFMCWRadar}{\texttt{Wolfram Demo}}}

\section{References}
[1] G. M. Brooker, "Understanding Millimeter Wave FMCW Radars," in First International Conference on Sensing Technology, November 21–23, 2005, Palmerston North, New Zealand (G. Sen Gupta, S. C. Mukhopadhyay, and C. H. Messom, eds.), pp. 152–157.

[2] I. V. Komarov and S. M. Smolskiy, Fundamentals of Short-Range FM Radar, Boston: Artech House, 2003.


\end{document}
