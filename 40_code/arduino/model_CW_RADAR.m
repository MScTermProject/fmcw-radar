% file   : simpleFFT.m
% author : Avinash Gokhale
% date   : May 3, 2016

close all; fclose all; clearvars;

% Sampling period of Arduino ADC
T = 1e-3;
fs = 1/T;

% fft size
N = 256;

% open file with sensor values
fileID = fopen('out_slow.log','r');
%fileID = fopen('D:\Dropbox\projects\RadarAltimeter\hterm\fmcw_dist_01.dat','r');
formatSpec = '%d';
x = fscanf(fileID,formatSpec);

% normalize samples
x = x./1024;
% DBG: get max
[val, idx] = max(x);

% run fft with n = logN/log2 radix
X = fft(x, N);
% get the psd
X = X.*conj(X)/N;
% findout the maxima in psd (excluding DC component)
[VAL, IDX] = max(X(2:end))
%X = fftshift(X);
% frequency bin (fft resolution)
freq_bin = fs/N

% calculations : doppler frequency
dopplerFreq = (IDX - 1)*fs/N

% calculations : Velocity
v_metersPerSecond = (dopplerFreq * (12.5e-3)/2)
v_kmph = v_metersPerSecond * 3.6

% plot : Sensor data
subplot(211);
plot(x);
title('Sensor Data'); xlabel('Time, t'); ylabel('Normalized Amplitude, V');

% plot : Power Spectral Density (PSD)
subplot(212);
n = -N/2+1:1:N/2-1;
stem(n*fs/N, X(2:end))
title('PSD of Sensor Data'); xlabel('FFT index, n'); ylabel('Normalized Power, W');