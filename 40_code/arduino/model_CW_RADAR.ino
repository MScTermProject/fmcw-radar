// CW Radar Sampling : Used for calculating velocity
// Prototype code | agokhale

// ADC Channel
int inputPin = 0;

// ADC Init
void setup() {
  analogReference(DEFAULT);
  Serial.begin(115200);
}

// ADC Sampling
void loop() {
  Serial.println(0x3ff & analogRead(inputPin));
  delay(1); // 1ms
}
