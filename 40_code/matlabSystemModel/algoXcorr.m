%% Algorithm using cross-correlation and FFT
% On the sampled dataset perform cross-correlation with generated ramp signal to pick one period
% of sampled data for recovery of signal of interest. Perform FFT to obtain beat frequency $\Delta f$

%% algoXcorr

% Parameters
% [in] Fs, Sampling frequency
% [in] Framp, Ramp frequency
% [in] sim, simulate RADAR sensor
% [in] adcSamples, file holding ADC samples
% [out] fd, Beat frequency

function fd = algoXcorr(Fs, Framp, sim, adcSamples)
	
	if sim
		% simulate sensor output with ramp coupling
		x = simulateRadarSensor(Fs, 1.0, Framp);
	else
		% read adc samples
		x = readSamples(adcSamples);
		x = a;
	end
	
	% For fine smoothening of sampled data
	movingAvgEn = 1;
	
	if movingAvgEn
		x = movingAvg(x, 5);
	end
	
	figure('Name', 'ADC Samples', 'NumberTitle', 'off');
	plot(x);
	title('ADC samples');
	xlabel('Time, t');
	ylabel('Amp');

	y = delayAdjust(x, Fs, Framp);
	figure('Name', 'Delay adjusted ADC samples in one period of Framp', 'NumberTitle', 'off');
	plot(y);
	title('ADC samples in one period of Framp');
	xlabel('Time, t');
	ylabel('Amp');
	
	soi = slopeAdjust(y);
	%soi = normalize(slopeAdjust(y));
	figure('Name', 'Slope adjusted ADC samples in one period of Framp', 'NumberTitle', 'off');
	plot(soi);
	title('Slope adjusted ADC samples in one period of Framp');
	xlabel('Time, t');
	ylabel('Amp');

	% Get the beat frequency
	fd = extractFreq(soi, Fs);
end

%% Note
% The ramp frequency must be sawtooth for this scheme to work!

%% Matfile info

% Filename : algoXCorr.m
% Author   : Avinash Gokhale