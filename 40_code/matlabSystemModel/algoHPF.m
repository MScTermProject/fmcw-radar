%% Algorithm using high pass filtering and FFT
% On the sampled dataset perform windowed moving average to avoid SAR adc sampling errors
% then perform high pass filtering to obtain SOI. Perform fft on SOI to get beat frequency $\Delta f$

%% algoHPF

% Parameters
% [in] Fs, Sampling frequency
% [in] Framp, Ramp frequency
% [in] sim, simulate RADAR sensor
% [in] adcSamples, file holding ADC samples
% [out] fd, Beat frequency

function fd = algoHPF(Fs, Framp, sim, adcSamples)

	if sim
		% simulate sensor output with triangular wave coupling
		x = simulateRadarSensor(Fs, 0.5, Framp, 20e3, 1);
	else
		% read adc samples
		x = readSamples(adcSamples);
	end
	
	% For fine smoothening of sampled data
	movingAvgEn = 1;
	
	if movingAvgEn
		x = movingAvg(x, 5);
    end
	
    % low pass filter
    FcLPF = 60e3;
    x = lpf(x, FcLPF, Fs);
    
	% high pass filtering
	soi = hpf(x, Framp + 2e3, Fs);
	plot(soi);
	
	% Get the beat frequency
	fd = extractFreq(soi, Fs)
end

%% Note
% The ramp frequency must be triangular for this scheme to work!

%% Matfile info

% Filename : algoHPF.m
% Author   : Avinash Gokhale