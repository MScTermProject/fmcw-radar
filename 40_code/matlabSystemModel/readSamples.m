%% Read ADC samples from ADC datafile

%% readSamples

% Parameters
% [in] fileName, ADC data file
% [out] samples, An array containing ADC samples
function samples = readSamples(fileName)
	fid = fopen(fileName, 'r');
	formatspec = '%x';
	samples = fscanf(fid, formatspec);
	fclose(fid);

    % Optional moving average filter to smoothen SAR ADC sampling errors
    mAvgEn = 0;
    if mAvgEn
        WindowSize = 2;
        b = (1/WindowSize)*ones(1,WindowSize);
        a = 1;
        samples = filter(b, a, samples);
    end
	
	% normalize
	samples = normalize(samples);
end

%% Matfile info

% Filename : readSamples.m
% Author   : Avinash Gokhale