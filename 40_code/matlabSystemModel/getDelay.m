%% Cross correlation
% This module performs cross corelation of two similar vectors x and y and
% returns correlator lag $\tau$

%% getDelay

% parameters
% [in] x, y - input vectors
% [in] dispFig, display correlation plot
% [out] tau, lag
function tau = getDelay(x, y, dispFig)
	[r, lags] = xcorr(x, y);
	
	if nargin < 3
		dispFig = 0;
	end

	[~, idx] = max(abs(r));
	tau = abs(lags(idx));
	indic = 10;

	if dispFig == 1
		figure('Name', 'xcorr', 'NumberTitle', 'off')
		set(gcf,'color','w');
		stem(lags,r)
		title('xcorr(x, y)'); xlabel('lags'); ylabel('xcorr |r|');
		line(lags(idx),r(idx),...
			'marker','o',...
			'markersize',10,...
			'markeredgecolor',[0,0,0],...
			'markerfacecolor',[.1,.1,.1],...
			'linestyle','none');
	end
end

%% Matfile info

% Filename : getDelay.m
% Author   : Avinash Gokhale