%% Single sided spectrum
% This module performs FFT and returns single sided spectrum

%% getSingleSidedSpectrum

% Parameters
% [in] x, input vector
% [in] Fs, Sampling frequency
% [out] f, pwr - Row vectors holding frequency and power

function [f, pwr] = getSingleSidedSpectrum(x, Fs)
	x = normalize(x);
    L = length(x);
    X = fft(x);
    P2 = abs(X/L);
    pwr = P2(1:L/2+1);
    f = Fs*(0:(L/2))/L;
end

%% Matfile info

% Filename : getSingleSidedSpectrum.m
% Author   : Avinash Gokhale