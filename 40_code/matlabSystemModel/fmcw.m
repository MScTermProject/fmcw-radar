%% System model for FMCW radar signal processing
% Top level script for FMCW sensor modeling and ADC data processing

%%
clearvars; close all; fclose all;

%% Sampling frequency, Fs
Fs = 796e3;

%% Ramp frequency, Framp
Framp = 2e3;
Tramp = 1/Framp;

%% Mode select for chosing data samples

% sim = 1 - simulated sensor modeling
% sim = 0 - use adc samples from micro-controller
sim = 1;

%% ADC samples
adcSamples = 'D:\Dropbox\TeamProject\20_06_16\4KHz\0p4m.txt';

%% Algorithms
%fd = algoHPF(Fs, Framp, sim, adcSamples);
fd = algoXcorr(Fs, Framp, sim, adcSamples)

%% Distance calculation
c = 3e8;
delta_f = 125e6;
R = (c/2)*Tramp*(fd/delta_f)

%% Matfile info

% Filename : fmcw.m
% Author   : Avinash Gokhale