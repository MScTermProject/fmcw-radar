%% Extracting frequency of input vector
% Extracts major frequency component of signal 'x' by performing fft

%% extractFreq

% Parameters
% [in] x, input vector
% [in] Fs, Sampling frequency
% [out] freq - Major freq component

function freq = extractFreq(x, Fs)

	% get single sided spectra
	[f, pwr] = getSingleSidedSpectrum(x, Fs);

	% find maxima (exclude DC component)
	[~, idx] = max(pwr(2:end));
	freq = f(idx+1);
	
	figure('Name', 'Spectrum', 'NumberTitle', 'off');
	plot(f(2:end), pwr(2:end));
	title('Single sided spectrum');
	xlabel('freq, f');
	ylabel('Power')
	line(f(idx+1),pwr(idx+1),...
		'marker','o',...
		'markersize',10,...
		'markeredgecolor',[0,0,0],...
		'markerfacecolor',[0,1,1],...
		'linestyle','none');
end

%% Matfile info

% Filename : extractFreq.m
% Author   : Avinash Gokhale