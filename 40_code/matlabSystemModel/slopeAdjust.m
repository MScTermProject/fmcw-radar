%% Reduce slope for a sinusoid riding ramp wave

%% slopeAdjust

% Parameters
% [in] x, input vector
% [out] y, slope adjusted vector x
function y = slopeAdjust(x)
	
	area = 1;
	z = x;

	% figure('Name', 'Slope adjustment', 'NumberTitle', 'off');

	% find acceptable minima
	% while (abs(area) > 1e-3)
		area = 0;

		for i = 1:1:length(z)
			area = area + z(i);
		end
		
		% [~, minIdx] = min(z);
		% [~, maxIdx] = max(z);
		
		% [area minIdx maxIdx]
		
		b = length(z);
		
		% Area of triangle = 0.5 * Base (b) * Height (h)
		h = 2 * area / b;
		
		% if minIdx > maxIdx
		% 	h = -1*h;
		% end
		
		for i = 1:1:length(z)
			z(i) = z(i) - i*(h/length(z));
		end
		z = -z;
		z = normalize(z);
		%plot(z); hold on;
	% end
	
	y = z;
end

%% Matfile info

% Filename : slopeAdjust.m
% Author   : Avinash Gokhale