%% Moving average filter
% This module performs moving average filtering of discrete time domain
% samples

%% movingAvg

% Parameter
% [in] x, input vector
% [out] y, filtered vector
function y = movingAvg(x, windowSize)
    b = (1/windowSize)*ones(1,windowSize);
    a = 1;
    y = normalize(filter(b, a, x));
end

%% Matfile info

% Filename : movingAvg.m
% Author   : Avinash Gokhale