%% Adjust delay and return one period wave

%% delayAdjust

% Parameters
% [in] x, Input vector
% [in] Fs, Sampling frequency
% [in] Framp, Ramp frequency
% [out] z, delay adjusted vector x

function z = delayAdjust(x, Fs, Framp)
	
	dt = 1/Fs;
	t = 0:dt:5/Framp-dt;
	y = 0.5+0.5*sawtooth(2*pi*Framp*t, 1);
	figure('Name', 'Ramp', 'NumberTitle', 'off');
	plot(y);
	title('Internally generated ramp, freq = Framp');
	xlabel('Time, t');
	ylabel('Amp')
	
	% get lag between our ramp and ADC dataset
	tau = getDelay(y, x, 1);
	
	% number of ADC samples in one period of Framp
	numSamples = Fs/Framp;
	
	% head room to avoid sudden phase changes
	headRoom = 3;
	
	% start and end indices
	startIdx = numSamples - tau + headRoom;
	endIdx = startIdx + numSamples - 2*headRoom;
	
	% Delay adjusted ADC samples in one period of Framp
	z = x(startIdx:endIdx);
end

%% Matfile info

% Filename : delayAdjust.m
% Author   : Avinash Gokhale