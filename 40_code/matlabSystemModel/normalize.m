%% Normalize vector
% Normalizes input vector x

%% normalize

% Parameters
% [in] x, input vector to be normalized
% [out] y, output
function y = normalize(x)
    minVal = min(x);
    maxVal = max(x);
    y = (x - minVal)/(maxVal - minVal);
end

%% Matfile info

% Filename : normalize.m
% Author   : Avinash Gokhale