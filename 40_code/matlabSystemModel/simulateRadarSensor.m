%% Simulation of radar sensor module
% Modeling received signal of radar sensor module which has high coupling
% between transmiter and receiver paths. The signal of interest, $SOI$ has
% the characteristic beat frequency, $\Delta f$ which essentially captures
% the delay between transmitted and recieved waves
%
% $$ \Delta f = f_{transmitted} - f_{received} $$
%
% However what we see is the $SOI$ riding on the leakage $V_{tune}$ ramp we
% use to generate FMCW signal that couples with the received path signal.

%% simulateRadarSensor

% Parameters
% [in] Fs, sampling frequency
% [in] sym, wave symmetry (0.5 - triangular, 1.0 - ramp)
% [in] Ftri, sawtooth frequency
% [in] Fsin, riding sine wave frequency
% [out] z, simulated radar sensor data (normalized)
function z = simulateRadarSensor(Fs, sym, Ftri, Fsin, dispPlot)
	
	if nargin < 3
		Ftri = 2e3;
	end
	Ttri = 1/Ftri;

	% assuming Fsin is ~15 times that of Ftri
    if nargin < 4
        Fsin = 15*Ftri;
    end
    Tsin = 1/Fsin;
	
	if nargin < 5
		dispPlot = 0;
	end

	dt = 1/Fs;
	t = 0:dt:5*Ttri-dt;

	x = 0.5+0.5*sawtooth(2*pi*Ftri*t + pi/2, sym);
	y = 0.05*sin(2*pi*Fsin*t);
	
	z = x + y;

	% normalize
	z = normalize(z);
	if dispPlot
		figure();
		plot(z);
		title('Simulated RADAR Sensor IF signal');
		xlabel('Time, t');
		ylabel('Amp');
	end
end

%% Matfile info

% Filename : simulateRadarSensor.m
% Author   : Avinash Gokhale