%% High pass filter
% This module performs high pass filtering of discrete time domain samples
% obtained with sampling frequency, $F_s$.

%% hpf

% Parameters
% [in] x, input vector
% [in] Fc, Cutoff frequency
% [in] Fs, Sampling frequency
% [out] y, filtered output

function y = hpf(x, Fc, Fs)
	hpf_filterCoeff = designfilt('highpassiir','FilterOrder', 16, ...
         'PassbandFrequency', Fc, 'PassbandRipple', 1e-5, ...
         'SampleRate', Fs);
	
	m = fvtool(hpf_filterCoeff);
	m.Analysis = 'magnitude';
	p = fvtool(hpf_filterCoeff);
	p.Analysis = 'phase';

	y = normalize(filter(hpf_filterCoeff, x));
end

%% Matfile info

% Filename : hpf.m
% Author   : Avinash Gokhale