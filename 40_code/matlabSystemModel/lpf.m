%% Low pass filter
% This module performs low pass filtering of discrete time domain samples
% obtained with sampling frequency, $F_s$.

%% lpf

% Parameters
% [in] x, input vector
% [in] Fc, Cutoff frequency
% [in] Fs, Sampling frequency
% [out] y, filtered signal

function y = lpf(x, Fc, Fs)
	lpf_filterCoeff = designfilt('lowpassiir','FilterOrder', 8, ...
			 'PassbandFrequency',Fc,'PassbandRipple',1e-4, ...
			 'SampleRate',Fs);
			 
	m = fvtool(lpf_filterCoeff);
	m.Analysis = 'magnitude';
    p = fvtool(lpf_filterCoeff);
    p.Analysis = 'phase';
	
	y = filter(lpf_filterCoeff, x);
	
	% normalize
	y = normalize(y);
end

%% Matfile info

% Filename : lpf.m
% Author   : Avinash Gokhale