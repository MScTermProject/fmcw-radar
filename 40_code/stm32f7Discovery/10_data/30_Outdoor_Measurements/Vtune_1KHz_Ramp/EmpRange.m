function Range = EmpRange(Freq)
    M = 1.0222e+04;
    Range = Freq/M - 0.18;
end