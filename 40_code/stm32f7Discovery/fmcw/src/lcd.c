/**
 *  ******************************************************************************
 * @file    lcd.c
 * @author  Pratheek Rai
 * @version 1.0
 * @date    01.08.2016
 * @brief   LCD file for radar altimeter.
 */

#include <lcd.h>

/** @addtogroup LCD
  * @{
  * @brief LCD module for radar altimeter.
  */

/**
 * Configures the LCD of the board.
 */
void LCD_Config(void)
{
  /* LCD Initialization */
  BSP_LCD_Init();

  /* LCD Initialization */
  BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
  BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS+(BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4));

  /* Enable the LCD */
  BSP_LCD_DisplayOn();

  /* Select the LCD Background Layer  */
  BSP_LCD_SelectLayer(0);
  BSP_LCD_SetFont(&Font16);
  /* Clear the Background Layer */
  BSP_LCD_Clear(LCD_COLOR_BLACK);

  /* Select the LCD Foreground Layer  */
  BSP_LCD_SelectLayer(1);
  BSP_LCD_SetFont(&Font16);
  /* Clear the Foreground Layer */
  BSP_LCD_Clear(LCD_COLOR_BLACK);

  /* Configure the transparency for foreground and background :
     Increase the transparency */
  BSP_LCD_SetTransparency(0, 0);
  BSP_LCD_SetTransparency(1, 100);
}

/**
 * Draws sampled signal on the LCD.
 * @param - uint32_t from - the drawing starts from this index.
 * @param - uint32_t to  - the drawing stops from this index.
 */
void drawWave(uint32_t* uhADCxConvertedValue ,uint32_t from, uint32_t to) {

	uint16_t value = 0;
	BSP_LCD_Clear(LCD_COLOR_WHITE);

	static char addString[128];
	sprintf(addString, "           FMCW RADAR ALTIMETER"); // header string.
	BSP_LCD_DisplayStringAtLine(0, addString);

	for (int i = from; i < to; i++) {

		value = uhADCxConvertedValue[i] % 4096;

		value = value / 16;

		BSP_LCD_DrawPixel((i - from) % 480, (270 - value), LCD_COLOR_RED);
		BSP_LCD_DrawPixel((i - from) % 480, (270 - value) + 1, LCD_COLOR_RED);

		for (int j = 0; j < 999999 / 10; j++)
			;

		if (((i - from) % 480) == 0) {
			BSP_LCD_Clear(LCD_COLOR_WHITE);
			BSP_LCD_DisplayStringAtLine(0, addString);
		}
	}
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/


