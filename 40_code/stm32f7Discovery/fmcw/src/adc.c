/**
 *  ******************************************************************************
 * @file    adc.c
 * @author  Pratheek Rai
 * @version 1.0
 * @date    01.08.2016
 * @brief   ADC file for radar altimeter.
 *  ADC file for FMCW RADAR.
 *  Performs Configuration of ADC.
 *  Reads ADC data through DMA.
 */

/** @addtogroup ADC
  * @{
  * @brief   ADC Module for radar altimeter.
  */

#include "../../fmcw/inc/adc.h"

/// ADC notifier.
extern uint32_t ADCstatus;

/// ADC handle.
ADC_HandleTypeDef AdcHandle;

/**
 * Initializes the ADC module.
 * @param adc_config_t config - configuration data for the ADC.
 * @return 0 if there is no error. Otherwise error code.
 */
uint32_t adc_initialize(adc_config_t config) {

	ADC_ChannelConfTypeDef sConfig;

	AdcHandle.Instance = ADCx;

	AdcHandle.Init.ClockPrescaler = config.CLK_PRESCALAR; // ADC clock frequency divided by 4.
	AdcHandle.Init.Resolution = config.RESOLUTION;       // resolution 12 bits.
	AdcHandle.Init.ScanConvMode = DISABLE; /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
	AdcHandle.Init.ContinuousConvMode = ENABLE; /* Continuous mode disabled to have only 1 conversion at each conversion trig */
	AdcHandle.Init.DiscontinuousConvMode = DISABLE; /* Parameter discarded because sequencer is disabled */
	AdcHandle.Init.NbrOfDiscConversion = 0;
	AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE; /* Conversion start trigged at each external event */
	AdcHandle.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;
	AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	AdcHandle.Init.NbrOfConversion = 1;
	AdcHandle.Init.DMAContinuousRequests = ENABLE;
	AdcHandle.Init.EOCSelection = DISABLE;

	if (HAL_ADC_Init(&AdcHandle) != HAL_OK) {
		return 1;
	}

	/*##-2- Configure ADC regular channel ######################################*/
	sConfig.Channel = config.CHANNEL;
	sConfig.Rank = 1;
	sConfig.SamplingTime = config.SAMPLING_CYCLES;
	sConfig.Offset = 0;

	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK) {
		return 1;
	}
	return 0;
}

/**
 * Starts ADC conversion.
 * @param uint32_t* uhADCxConvertedValue - Buffer for samples.
 * @param uint32_t len - size of the buffer.
 * @return uint32_t - error status. returns 0 if successfully.
 */
uint32_t adc_start_conv(uint32_t* uhADCxConvertedValue, uint32_t len)
{

	/// start conversion.
		if (HAL_ADC_Start_DMA(&AdcHandle, (uint32_t*) uhADCxConvertedValue, len)
				!= HAL_OK) {
			return 1;
		}
	return 0;
}

/**
 * @brief  Conversion complete callback in non blocking mode
 * @param  AdcHandle : AdcHandle handle
 * @note   This example shows a simple way to report end of conversion, and
 *         you can add your own implementation.
 * @retval None
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle) {

	BSP_LCD_DisplayStringAtLine(3, " Conversion done ! Buffer filled .");

	if (HAL_ADC_DeInit(AdcHandle) != HAL_OK) {
		ADCstatus = 2;
	}
	if (HAL_ADC_Stop_DMA(AdcHandle) != HAL_OK) {
		BSP_LCD_DisplayStringAtLine(6, " Error !!!!");
	}

	if (ADCstatus == 0) {
		ADCstatus = 1;
	}

}
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc) {
	ADCstatus = 2;
}

/**
  * @brief ADC MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  * @param hadc: ADC handle pointer
  * @retval None
  */
void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
  GPIO_InitTypeDef          GPIO_InitStruct;
  static DMA_HandleTypeDef  hdma_adc;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* ADC3 Periph clock enable */
  ADCx_CLK_ENABLE();
  /* Enable GPIO clock ****************************************/
  ADCx_CHANNEL_GPIO_CLK_ENABLE();
  /* Enable DMA2 clock */
  DMAx_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* ADC Channel GPIO pin configuration */
  GPIO_InitStruct.Pin = ADCx_CHANNEL_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ADCx_CHANNEL_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the DMA streams ##########################################*/
  /* Set the parameters to be configured */
  hdma_adc.Instance = ADCx_DMA_STREAM;

  hdma_adc.Init.Channel  = ADCx_DMA_CHANNEL;
  hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
  hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
  hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_adc.Init.Mode = DMA_CIRCULAR;
  hdma_adc.Init.Priority = DMA_PRIORITY_HIGH;
  hdma_adc.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  hdma_adc.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
  hdma_adc.Init.MemBurst = DMA_MBURST_SINGLE;
  hdma_adc.Init.PeriphBurst = DMA_PBURST_SINGLE;

  HAL_DMA_Init(&hdma_adc);

  /* Associate the initialized DMA handle to the ADC handle */
  __HAL_LINKDMA(hadc, DMA_Handle, hdma_adc);

  /*##-4- Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt */
  HAL_NVIC_SetPriority(ADCx_DMA_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(ADCx_DMA_IRQn);
}

/**
  * @brief ADC MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO to their default state
  * @param hadc: ADC handle pointer
  * @retval None
  */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef *hadc)
{

  /*##-1- Reset peripherals ##################################################*/
  ADCx_FORCE_RESET();
  ADCx_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  /* De-initialize the ADC Channel GPIO pin */
  HAL_GPIO_DeInit(ADCx_CHANNEL_GPIO_PORT, ADCx_CHANNEL_PIN);
}
