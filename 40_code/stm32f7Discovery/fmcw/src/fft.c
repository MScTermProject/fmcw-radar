/**
 *  ******************************************************************************
 * @file    fft.c
 * @author  Pratheek Rai
 * @version 1.0
 * @date    01.08.2016
 * @brief   FFT file for radar altimeter.
 *  FFT file for FMCW RADAR.
 *  Performs Configuration and executes FFT on the samples passed.
 */

#include "fft.h"

/** @addtogroup FFT
  * @{
  * @brief   FFT Module for FMCW RADAR.
  */

/// FFT handle.
TM_FFT_F32_t FFT;

/// FFT config handle.
fft_init_t * initVal;

/// FFT results buffer.
float32_t * fftRes;
/**
 * Initialises the FFT module.
 * @param - float32_t * input - input sample buffer.
 * @param - float32_t * output - output sample buffer.
 */
void initFFT(fft_init_t fft_init)
{
	TM_FFT_SetBuffers_F32(&FFT, fft_init.inputBuf, fft_init.outputBuf);
	initVal = &fft_init;
	fftRes = fft_init.outputBuf;
	// initialize FFT.
		TM_FFT_Init_F32(&FFT, fft_init.size, 0);

}

/**
 * Performs FFT on the sampled ADC signal.
 * @param - float32_t* samples - Normalized sample buffer.
 * @param - fft_val_t* fftout - single sided FFT output.
 */
void doFFT(float32_t* samples , fft_val_t* fftout , uint32_t findNoOfPeaks ){
 uint32_t counter = 0;
 float32_t sample_val = 0.0;
	do {
			sample_val = samples[counter % ONERAMPDATA];
			counter++;
		} while ((!TM_FFT_AddToBuffer(&FFT, sample_val)));

		TM_FFT_Process_F32(&FFT);

			for (int i = 0; i < initVal->size / 2; i++) {

				for (int j = 0; j < findNoOfPeaks; j++) {

					if (fftout[j].value < fftRes[i]) {
						for (int k = findNoOfPeaks - 1; k > j; k--) {
							fftout[k].value = fftout[k - 1].value;
							fftout[k].index = fftout[k - 1].index;
						}
						fftout[j].value = fftRes[i];
						fftout[j].index = i;
						break;
					}
				}
			}
}
