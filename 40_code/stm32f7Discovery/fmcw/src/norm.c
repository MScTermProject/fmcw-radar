/**
 *  ******************************************************************************
 * @file    norm.c
 * @author  Vikas Patil
 * @version 1.0
 * @date    01.08.2016
 * @brief   Normalizes the signal from the sensor.
 */
#include "../../fmcw/inc/norm.h"

/** @addtogroup NORMALIZER
  * @{
  * @brief  Normalizes the signal from the sensor.
  */

/**
 *  Normalizes the input samples.
 *  @param float32_t* data - Buffer for normalized data.
 *  @param len  - length of the buffer.
 */
void norm(float32_t* data, uint32_t len)
{
	float32_t min_data =data[0];
	float32_t max_data =0;
	uint32_t max_idx =0;

	float32_t area = 0;
	float32_t height = 0;

	uint32_t loop_cnt = 0;

	arm_max_f32(data, len, &max_data, &max_idx);

	for(loop_cnt= 0; loop_cnt<len; loop_cnt++)
	{
		data[loop_cnt] = ((float32_t)data[loop_cnt] - min_data)/(max_data-min_data);
	}

	for(loop_cnt= 0; loop_cnt<len; loop_cnt++)
	{
		area += data[loop_cnt];
	}

	height = (area * 2)/ len;

	for(loop_cnt= 0;loop_cnt<len;loop_cnt++)
	{
		data[loop_cnt] = data[loop_cnt] - (float32_t)((height / len) * loop_cnt);
	}

	return;
}
