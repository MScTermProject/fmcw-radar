/**
 *  ******************************************************************************
 * @file    filter.c
 * @author  Pratheek Rai
 * @version 1.0
 * @date    01.08.2016
 * @brief   Performs filtering actions on the samples from the sensor.
 *  Filter file for FMCW RADAR.
 */

#include <filter.h>
#include "arm_math.h"
#include"defines.h"

/** @addtogroup FILTER
  * @{
  * @brief   Performs filtering actions on the samples from the sensor.
  */

/**
 *  Analyses the sampled ADC samples and the returns the starting index of the ramp.
 *  This method takes the "minimum" of one time period and looks for a peak at the end of the time period. If a peak
 *  is found then the minimum is the start of ramp.
 *  @param - uint32_t index -  start index.
 *  @param - uint32_t* samples - input samples.
 *  @param - uint32_t len - length of buffer.
 *  @param - float32_t average - average value of the samples.
 *  @return - Index of the start of ramp.
 */
int32_t checkIfPeak(uint32_t index, uint32_t* samples, uint32_t len,
		float32_t average) {
	int32_t valaboveAverage = 0;
	for (int i = (index - 20); i < (index); i++) {
		if (samples[i] > average) {
			valaboveAverage++;
		}
	}
	if (valaboveAverage > 0) {
		for (int i = index; i < (index + 20); i++) {
			if (samples[i] < average) {
				valaboveAverage--;
			}
		}

		if ((valaboveAverage > -10) && (valaboveAverage < 10)) {
			return 1; // peak !!
		}

	} else {
		return 0;
	}

}

/**
 *  Analyses the sampled ADC samples and the returns the starting index of the ramp.
 *  This method takes the "minimum" of one time period and looks for a peak at the end of the time period. If a peak
 *  is found then the minimum is the start of ramp.
 *  @param - uint32_t index -  start index.
 *  @param - uint32_t* samples - input samples.
 *  @param - uint32_t len - length of buffer.
 *  @param - float32_t average - average value of the samples.
 *  @param - uint32_t timePeriod - time period of ramp in terms of no of samples.
 *  @return - Index of the start of ramp.
 */
int32_t checkIfPeak2(uint32_t index, uint32_t* samples, uint32_t len,
		float32_t average, uint32_t timePeriod) {
	uint32_t sweepRange = 400;
	uint32_t previousVal = samples[index - ((sweepRange / 2))];
	int32_t fallMag = average - samples[index - timePeriod];
	for (int i = (index - (sweepRange / 2)); i < (index + (sweepRange / 2));
			i++) {
		int32_t fall = (previousVal - samples[i]);
		if ( (fall > fallMag/4) && (fall < 2.2*fallMag)) {
			return 1; // peak !!
		}
		previousVal = samples[i];
	}
	return 0;
}

/**
 *  Returns the starting index of the ramp.
 *  @param - uint32_t* samples - input samples.
 *  @param - uint32_t len - length of buffer.
 *  uint32_t timePeriod - time period of ramp in terms of no of samples.
 *  @return - Index of the start of ramp.
 */
int32_t getRamp(uint32_t* samples, uint32_t len, uint32_t timePeriod) {
	float32_t average = 0.0;
	float32_t min = 0;
	uint32_t start = 5;
	for (int i = start; i < len; i++) {
		average = (samples[i]) + average;
	}
	average = average / (len - start);
	min = average;
	uint32_t minIndex[50] = { 0 };
	uint32_t minCount = 0;

	// accumulates the minimum for 2 cycles.
	for (int i = start; i < 2 * timePeriod; i++) {
		if (min > samples[i]) {
			min = samples[i];
		}
	}
    // stores the minimum indices.
	for (int i = start; i < 2 * timePeriod; i++) {
		if (min == samples[i]) {
			minIndex[minCount++] = i;
		}

	}
	// loops through all the minimums until one valid minimum is found.
	for (int i = 0; i < minCount; i++) {
		if (checkIfPeak2(minIndex[i] + timePeriod, samples, len, average,
				timePeriod) == 1) {
			return minIndex[i];
		}
	}

	return minIndex[0];

}

/**
 * Removes ADC abnormalities. There are possibilities that some of the data
 * could be corrupted. This method removes those abnormalities.
 */
void removeADCAnomalies(uint32_t *uhADCxConvertedValue) {
	uint32_t recentValue = 0;
	for (int i = 0; i < LENGTH; i++) {

		if (uhADCxConvertedValue[i] < 10) {
			uhADCxConvertedValue[i] = recentValue;
		} else {
			recentValue = uhADCxConvertedValue[i];
		}

	}
}

/**
 * Moving average filter.(LPF). Takes an average of data on idx to idx + MOVING_POINT.
 * @param uint32_t idx index where MP average is performed.
 */

uint32_t movingAvg(uint32_t idx , uint32_t* uhADCxConvertedValue) {
	uint32_t count = 0;
	float32_t delayLine[MOVING_POINT] = { 0.0 };
	int index = idx;
	do {
		count++;
		delayLine[MOVING_POINT - count] = uhADCxConvertedValue[index--];
	} while ((index >= 0) && (MOVING_POINT > count));
	float32_t retVal = 0.0;
	for (int i = 0; i < MOVING_POINT; i++) {
		retVal = delayLine[i] + retVal;
	}

	return (retVal / MOVING_POINT);
}
