/**
 *  This is the main method for FMCW RADAR ALTIMETER module.
 */
#include "../../fmcw/inc/adc.h"
#include "../../fmcw/inc/defines.h"
#include "../../fmcw/inc/fft.h"
#include "../../fmcw/inc/filter.h"
#include "../../fmcw/inc/lcd.h"
#include "../../fmcw/inc/norm.h"
#include "../../fmcw/inc/stm32F7.h"

///< Input buffer is always 2 * FFT_SIZE
static float32_t Input[2 * FFT_SIZE];

///< Output buffer is always FFT_SIZE
static float32_t fftRes[FFT_SIZE];

/// ADC status flag.
__IO uint32_t ADCstatus = 0;

/// Buffer for the ADC sampled data.
__IO static uint32_t uhADCxConvertedValue[LENGTH] = { 0 };

/// Buffer for the normalized ADC data.
static float32_t normalizedData[ONERAMPDATA] = { 0.0 };

/* Private function prototypes -----------------------------------------------*/
static void Error_Handler(void);
void performFreqCal();

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {
	/* Configure the MPU attributes as Write Through */
	MPU_Config();

	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	HAL_Init();

	/* Configure the system clock to 216 MHz */
	SystemClock_Config();

	LCD_Config();

	BSP_LCD_DisplayStringAtLine(0, "----------  FMCW ------------");

	/* Configure LED1 */
	BSP_LED_Init(LED1);

	adc_config_t config;
	config.CHANNEL = ADC_CHANNEL_8;
	config.CLK_PRESCALAR = ADC_CLK_PRESCALAR;
	config.RESOLUTION = ADC_RESOLUTION_12B;
	config.SAMPLING_CYCLES = ADC_SAMPLING_CYCLES;
	/// Initialize ADC.
	if (adc_initialize(config)){
		Error_Handler();
	}
    /// start ADC conversion
	if(adc_start_conv(uhADCxConvertedValue, LENGTH))
	{
		Error_Handler();
	}

	/* Infinite loop */
	while (1) {

		if (ADCstatus == 1) {

			//  drawWave(0 , LENGTH);
			performFreqCal();
			ADCstatus = 0;
		}else if(ADCstatus == 2){
			Error_Handler();
			break;
		}
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void) {
	/* Turn LED1 on */
	BSP_LED_On(LED1);
	while (1) {
		/* Error if LED1 is slowly blinking (1 sec. period) */
		BSP_LED_Toggle(LED1);
		HAL_Delay(1000);
	}
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif



/**
 * Performs FFT on the sampled ADC signal.
 */
void performFreqCal() {

	char string[100];

	/*.........FFT test ........... */
#ifdef FFT_TEST

	float32_t sin_val = 0;
	uint32_t i = 0, freq = 15000;   // sine freq.

	do {
		/* Calculate sinus value */
		sin_val = 500;  							// Minimum amplitude.
		sin_val += 2500 * 0.1
		* (float) sin(
				2 * (float) 3.14159265359 * (float) 1 * (float) freq
				* (float) i / SAMPLING_FREQUENCY);
		sin_val = sin_val + (((i + (RAMP_LEN / 4)) % RAMP_LEN) * 2000 / RAMP_LEN);
		//*******************************************************
		// Noise introduction.

		if ((i % (RAMP_LEN / 3)) == 0 || (i % (RAMP_LEN / 3)) == 1
				|| (i % (RAMP_LEN / 3)) == 2)
		sin_val = 0;
		//*******************************************************

		uhADCxConvertedValue[i] = sin_val;
		i++;
	}while (i < LENGTH);
#endif

    /// remove noise from the sampled data.
	removeADCAnomalies(uhADCxConvertedValue);

	uint32_t counter = 0;
	float32_t sample_val;
	uint32_t inputSample[LENGTH] = { 0.0 };
	do {
		sample_val = movingAvg(counter ,uhADCxConvertedValue);
		inputSample[counter] = sample_val;
		counter++;
	} while (counter < LENGTH);

	//drawWave(0, 0, LENGTH);

	/// get the starting index of the ramp.
	uint32_t startOfRamp = getRamp(inputSample, LENGTH, RAMP_LEN);


	if (startOfRamp != 999999) {
		drawWave(uhADCxConvertedValue,startOfRamp, RAMP_LEN);
	} else {
		sprintf(string, "could not locate start of ramp !!");
		BSP_LCD_DisplayStringAtLine(4, string);
		return;
	}

	counter = startOfRamp;
	sample_val = 0.0;

	for (int i = startOfRamp; i < (ONERAMPDATA + startOfRamp); i++) {
		normalizedData[i - startOfRamp] = inputSample[i];
	}
	// normalize the data.
	norm(normalizedData, ONERAMPDATA);

	fft_val_t peaks[3] = { 0.0 };
	int findNoOfPeaks = 3;

	fft_init_t fft_init = {Input ,fftRes , FFT_SIZE};

	initFFT(fft_init);

	doFFT(normalizedData,peaks ,findNoOfPeaks);



	for (int i = 0; i < findNoOfPeaks; i++) {
		sprintf(string, "freq=%f ,%f & dist = %f",
				((float) peaks[i].index * SAMPLING_FREQUENCY / FFT_SIZE),peaks[i].value,
				(float)((float)peaks[i].index/(4 * 8)));
		BSP_LCD_DisplayStringAtLine(12 + i, string);
	}
}
