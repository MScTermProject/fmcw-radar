/*
 * Filter.h
 * This file performs all the required filtering actions on the samples from the ADC.
 *  Created on: 29-Jul-2016
 *      Author: Pratheek
 */

#ifndef FILTER_H_
#define FILTER_H_
#include "arm_math.h"
/**
 * Performs auto-correlation on the samples and returns the time period  in terms of number of samples.
 */
uint32_t autoCorrelate(float32_t* samples, uint32_t len, float32_t* ramp,
		uint32_t ramplen);

/**
 * Selects one Ramp from the samples from ADC. This Ramp is used to calculate the frequency (delta F).
 * @param uint32_t* samples - ADC samples.
 * @param uint32_t len - length of the input buffer.
 * @param uint32_t timePeriod - time period of the varactor ramp in terms of samples.
 */
int32_t getRamp(uint32_t* samples, uint32_t len, uint32_t timePeriod);

/**
 *  Removes ADC irregularities from the sampled signal.
 *  @param uint32_t *uhADCxConvertedValue - ADC samples.
 */
void removeADCAnomalies(uint32_t *uhADCxConvertedValue);

/**
 * Moving average filter around the index passed. ** This is called from a for loop where filtering action is
 * performed around all the samples in the buffer **
 * @param uint32_t idx - index where filtering is performed.
 * @param uint32_t* uhADCxConvertedValue - samples.
 */
uint32_t movingAvg(uint32_t idx, uint32_t* uhADCxConvertedValue);

#endif /* FILTER_H_ */
