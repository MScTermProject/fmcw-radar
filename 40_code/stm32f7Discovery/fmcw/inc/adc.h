/*
 * adc.h
 *
 *  Created on: 03-Aug-2016
 *      Author: Pratheek
 */

#ifndef ADC_H_
#define ADC_H_
/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "stm32f7xx_hal_adc.h"

#include "../../fmcw/inc/defines.h"

/**
 * ADC configuration data structure.
 */
typedef struct
{
	uint32_t CLK_PRESCALAR;
	uint32_t RESOLUTION;
	uint32_t CHANNEL;
	uint32_t SAMPLING_CYCLES;

} adc_config_t;

/**
 * Initializes the ADC module.
 * @param adc_config_t config - configuration data for the ADC.
 * @return 0 if there is no error. Otherwise error code.
 */
uint32_t adc_initialize(adc_config_t config);

/**
 * Starts ADC conversion for the number of samples specified in len through DMA.
 * @param  uint32_t* uhADCxConvertedValue - buffer for the samples.
 * @param  uint32_t len - length of the buffer.
 * @return 0 if there is no error. Otherwise error code.
 */
uint32_t adc_start_conv(uint32_t* uhADCxConvertedValue, uint32_t len);


#endif /* ADC_H_ */
