/*
 * norm.h
 *
 *  Created on: 02-Aug-2016
 *      Author: Vikas Patil
 */

#ifndef NORM_H_
#define NORM_H_

#include "stm32f7xx.h"
#include "stm32746g_discovery.h"
#include "arm_math.h"

/**
 *  Normalizes the input samples.
 *  @param float32_t* data - Buffer for normalized data.
 *  @param len  - length of the buffer.
 */
void norm(float32_t* data, uint32_t len);


#endif /* NORM_H_ */
