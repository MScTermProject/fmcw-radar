/*
 * stm32F7.h
 *
 *  Created on: 07-Aug-2016
 *      Author: Pratheek
 */

#ifndef STM32F7_H_
#define STM32F7_H_

/// system clock configuration.
void SystemClock_Config(void);

/// MPU configuration
void MPU_Config(void);

/// CPU cache enable.
void CPU_CACHE_Enable(void);

#endif /* STM32F7_H_ */
