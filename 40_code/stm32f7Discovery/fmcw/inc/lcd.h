/*
 * lcd_config.h
 *
 *  Created on: 30.05.2016
 *      Author: Pratheek Rai
 */

#ifndef LCD_H_
#define LCD_H_


#include "stm32746g_discovery_lcd.h"

#define LCD_COLOR_RED           ((uint32_t)0xFFFF0000)
#define LCD_COLOR_WHITE         ((uint32_t)0xFFFFFFFF)
/**
 * Configures the LCD of the board.
 */
void LCD_Config(void);

/**
 * Draws sampled signal on the LCD.
 * @param - uint32_t from - the drawing starts from this index.
 * @param - uint32_t to  - the drawing stops from this index.
 */
void drawWave(uint32_t* uhADCxConvertedValue ,uint32_t from, uint32_t to);

#endif /* LCD_H_ */
