/**
 * global definitions.
 */
#ifndef TM_DEFINES_H
#define TM_DEFINES_H

/* Put your global defines for all libraries here used in your project */

#define APB_CLK_DIV              RCC_HCLK_DIV2  				/// clock division for ADC input clock.

#define LENGTH 					 4096             				/// Length of the buffer.
#define MAX_ADC_AMPL             4096                           /// Max ADC amplitude.

#define FFT_SIZE                (LENGTH)          				/// Define the no of freq bins.
#define MOVING_POINT             4               				/// No of points for moving average filter.
#define ONERAMPDATA              512                            /// No of good data on ramp based on ADC sampling frequency.

/* DEMO RAMP SETTINGS */
#define RAMP_FREQ                1000                           /// Frequency of the varactor ramp input.
#define RAMP_LEN                 (SAMPLING_FREQUENCY/RAMP_FREQ) /// Ramp frequency for DEMO mode.


/// ADC settings.
#define ADC_CLK_PRESCALAR        ADC_CLOCKPRESCALER_PCLK_DIV2  	/// Prescalar on APB2 clock.  216M / (2 * 4) = 27Mhz.
#define ADC_SAMPLING_CYCLES      ADC_SAMPLETIME_56CYCLES   		/// Number of cycles.
#define SAMPLING_FREQUENCY       (216000000/2/2/(56 + 12))		/// sampling frequency for FFT. *** Check this  when the ADC settings are changed !!!! ***

#define ADCx                            ADC3
#define ADCx_CLK_ENABLE()               __HAL_RCC_ADC3_CLK_ENABLE()
#define DMAx_CLK_ENABLE()               __HAL_RCC_DMA2_CLK_ENABLE()
#define ADCx_CHANNEL_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOF_CLK_ENABLE()

#define ADCx_FORCE_RESET()              __HAL_RCC_ADC_FORCE_RESET()
#define ADCx_RELEASE_RESET()            __HAL_RCC_ADC_RELEASE_RESET()
/* Definition for ADCx Channel Pin */
#define ADCx_CHANNEL_PIN                GPIO_PIN_10
#define ADCx_CHANNEL_GPIO_PORT          GPIOF

/* Definition for ADCx's Channel */
#define ADCx_CHANNEL                    ADC_CHANNEL_8

/* Definition for ADCx's DMA */
#define ADCx_DMA_CHANNEL                DMA_CHANNEL_2
#define ADCx_DMA_STREAM                 DMA2_Stream0

/* Definition for ADCx's NVIC */
#define ADCx_DMA_IRQn                   DMA2_Stream0_IRQn
#define ADCx_DMA_IRQHandler             DMA2_Stream0_IRQHandler

#define DMAx_CLK_ENABLE()                __HAL_RCC_DMA2_CLK_ENABLE()


//#define FFT_TEST

#endif
