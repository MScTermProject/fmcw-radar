/*
 * fft.h
 *
 *  Created on: 30-Jul-2016
 *      Author: Pratheek
 */

#ifndef FFT_H_
#define FFT_H_
#include <tm_stm32_fft.h>
#include "arm_math.h"
#include "defines.h"

/// FFT init configuration.
typedef struct
{
	float32_t * inputBuf;
	float32_t * outputBuf;
	uint32_t size;
}fft_init_t;

/// FFT value structure.
typedef struct fft_val{
	float32_t value;
	uint32_t index;
} fft_val_t;

/**
 * Initialises FFT module.
 * @param fft_init_t fft_init - FFT config data structure.
 */
void initFFT(fft_init_t fft_init);

/**
 * Performs FFT on the sampled ADC signal.
 * @param float32_t* samples - Normalized samples.
 * @param fft_val_t* peaks - returns the number of top peaks mentioned in len.
 * @param uint32_t findNoOfPeaks - number of top peaks.Also the length of peaks buffer.
 */
void doFFT(float32_t* samples , fft_val_t* peaks , uint32_t findNoOfPeaks);



#endif /* FFT_H_ */
