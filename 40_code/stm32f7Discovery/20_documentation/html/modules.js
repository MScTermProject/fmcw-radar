var modules =
[
    [ "ADC", "group___a_d_c.html", "group___a_d_c" ],
    [ "FFT", "group___f_f_t.html", "group___f_f_t" ],
    [ "FILTER", "group___f_i_l_t_e_r.html", "group___f_i_l_t_e_r" ],
    [ "LCD", "group___l_c_d.html", "group___l_c_d" ],
    [ "NORMALIZER", "group___n_o_r_m_a_l_i_z_e_r.html", "group___n_o_r_m_a_l_i_z_e_r" ]
];