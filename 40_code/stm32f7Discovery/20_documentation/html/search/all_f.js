var searchData=
[
  ['ramp_5ffreq',['RAMP_FREQ',['../defines_8h.html#a85cfc0c6e65b64a878767e39272c68c8',1,'defines.h']]],
  ['ramp_5flen',['RAMP_LEN',['../defines_8h.html#a01d3e40bd502719f157cc668941ad9f8',1,'defines.h']]],
  ['removeadcanomalies',['removeADCAnomalies',['../group___f_i_l_t_e_r.html#ga8e05a9f9ffc086e6790bbd67791d8c34',1,'removeADCAnomalies(uint32_t *uhADCxConvertedValue):&#160;filter.c'],['../group___f_i_l_t_e_r.html#ga8e05a9f9ffc086e6790bbd67791d8c34',1,'removeADCAnomalies(uint32_t *uhADCxConvertedValue):&#160;filter.c']]],
  ['resolution',['RESOLUTION',['../structadc__config__t.html#a0a893bc33aae967d375478425e200db3',1,'adc_config_t']]]
];
