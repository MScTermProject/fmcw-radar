var searchData=
[
  ['debugmon_5fhandler',['DebugMon_Handler',['../stm32f7xx__it_8c.html#adbdfb05858cc36fc520974df37ec3cb0',1,'stm32f7xx_it.c']]],
  ['dofft',['doFFT',['../group___f_f_t.html#gaf5a63078c68d106b151abdf208798415',1,'doFFT(float32_t *samples, fft_val_t *peaks, uint32_t findNoOfPeaks):&#160;fft.c'],['../group___f_f_t.html#gaf5a63078c68d106b151abdf208798415',1,'doFFT(float32_t *samples, fft_val_t *fftout, uint32_t findNoOfPeaks):&#160;fft.c']]],
  ['drawwave',['drawWave',['../group___l_c_d.html#gad1699af9a53d77a0afbb81d92e07172f',1,'drawWave(uint32_t *uhADCxConvertedValue, uint32_t from, uint32_t to):&#160;lcd.c'],['../group___l_c_d.html#gad1699af9a53d77a0afbb81d92e07172f',1,'drawWave(uint32_t *uhADCxConvertedValue, uint32_t from, uint32_t to):&#160;lcd.c']]]
];
