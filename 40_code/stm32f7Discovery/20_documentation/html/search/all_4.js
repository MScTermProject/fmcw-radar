var searchData=
[
  ['debugmon_5fhandler',['DebugMon_Handler',['../stm32f7xx__it_8c.html#adbdfb05858cc36fc520974df37ec3cb0',1,'stm32f7xx_it.c']]],
  ['defines_2eh',['defines.h',['../defines_8h.html',1,'']]],
  ['dmax_5fclk_5fenable',['DMAx_CLK_ENABLE',['../defines_8h.html#a927580de237da5fc37448af795c5ae3d',1,'DMAx_CLK_ENABLE():&#160;defines.h'],['../defines_8h.html#a00a8f08ba4ca57e785f2e4424641f3dc',1,'DMAx_CLK_ENABLE():&#160;defines.h']]],
  ['dofft',['doFFT',['../group___f_f_t.html#gaf5a63078c68d106b151abdf208798415',1,'doFFT(float32_t *samples, fft_val_t *peaks, uint32_t findNoOfPeaks):&#160;fft.c'],['../group___f_f_t.html#gaf5a63078c68d106b151abdf208798415',1,'doFFT(float32_t *samples, fft_val_t *fftout, uint32_t findNoOfPeaks):&#160;fft.c']]],
  ['drawwave',['drawWave',['../group___l_c_d.html#gad1699af9a53d77a0afbb81d92e07172f',1,'drawWave(uint32_t *uhADCxConvertedValue, uint32_t from, uint32_t to):&#160;lcd.c'],['../group___l_c_d.html#gad1699af9a53d77a0afbb81d92e07172f',1,'drawWave(uint32_t *uhADCxConvertedValue, uint32_t from, uint32_t to):&#160;lcd.c']]]
];
