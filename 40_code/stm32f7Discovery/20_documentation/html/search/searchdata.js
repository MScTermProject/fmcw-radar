var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuv",
  1: "af",
  2: "adflmns",
  3: "_abcdghilmnprsu",
  4: "acefiorsv",
  5: "f",
  6: "adefhlmoprstuv",
  7: "afln"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Modules"
};

