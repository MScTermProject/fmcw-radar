var adc_8c =
[
    [ "adc_initialize", "group___a_d_c.html#ga063aa74adfc7ba18fbd58c2b31c51407", null ],
    [ "adc_start_conv", "group___a_d_c.html#ga37a890dd5e611a261853d039c2746a4d", null ],
    [ "HAL_ADC_ConvCpltCallback", "group___a_d_c.html#ga9297a848610654bf5ca8e6e68032bd07", null ],
    [ "HAL_ADC_ErrorCallback", "group___a_d_c.html#gadea1a55c5199d5cb4cfc1fdcd32be1b2", null ],
    [ "HAL_ADC_MspDeInit", "group___a_d_c.html#ga39b0f8e80268ab3e660ead921ad4b22f", null ],
    [ "HAL_ADC_MspInit", "group___a_d_c.html#gaa30863492d5c3103e3e8ce8a63dadd07", null ],
    [ "AdcHandle", "group___a_d_c.html#ga35c38ca7a8210322754a21a7009e1021", null ],
    [ "ADCstatus", "group___a_d_c.html#ga09bfc755ab75315633bb59ed5aeb40c4", null ]
];