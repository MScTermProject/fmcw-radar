#**FMCW** (**F**requency **M**odulated **C**ontinuous **W**ave) **Radar**
---


### Background ###

Ordinary pulsed radar detects the range to a target by emitting a short pulse and observing the time of flight of the target echo. This requires the radar to have high instantaneous transmit power and often results in a radar with a large, expensive physical apparatus. Frequency-modulated continuous-wave (FMCW) radars achieve similar results using much smaller instantaneous transmit powers and physical size by continuously emitting periodic pulses whose frequency content varies with time. A very important type of FMCW radar pulse is the linear FM sweep. In this case, the range to the target is found by detecting the frequency difference between the received and emitted radar signals. The range to the target is proportional to this frequency difference, which is also referred to as the beat frequency.

**Source:** *[Wolfram Demonstrations Project](http://demonstrations.wolfram.com/FrequencyModulatedContinuousWaveFMCWRadar/)*

TeX formated pdf in ```docs/tex/Introduction_to_FMCW_Radar.pdf```

### Sandbox ###
```
$ git clone https://MScTermProject@bitbucket.org/MScTermProject/fmcw-radar.git
```
---

If you want to edit this file refer to *[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)* page on Bitbucket

---

### Repo Info ###
```
>$ tree
.
+---10_concept
+---20_planning
+---30_docs
|   +---applicationNotes
|   +---datasheets
|   +---manuals
|   +---opamp
|   +---tex
+---40_code
|   +---arduino
|   +---matlabSystemModel
|   |   \---html
|   \---stm32f7Discovery
|       +---10_data
|       +---archive
|       +---CMSIS
|       +---HAL_Driver
|       +---inc
|       +---lib
|       +---src
|       +---startup
+---50_results
|   +---Results_IVS_162
|   +---Results_IVS_465
|   \---Results_IVS_565
\---60_presentation


Description
                  docs : documents dir
                  code : C/C++/Matlab/Python source
code/matlabSystemModel : Matlab system model with simulation environment with html documentation
 code/stm32f7Discovery : ARM F7 prototype with doxygen documentation

```